DECLARE @Client VARCHAR(1000) = 'Lake Forest College';
DECLARE @UserName VARCHAR(500) = 'pfenning@lakeforest.edu';
DECLARE @FullName VARCHAR(1000) = 'Stephanie Pfenning';
DECLARE @Email VARCHAR(500) = 'pfenning@lakeforest.edu';
DECLARE @EM_Access VARCHAR(3) = '0';
DECLARE @FM_Access VARCHAR(3) = '1';
DECLARE @InternalUser CHAR(1) = '0';

INSERT INTO sec_tableau_users
SELECT @Client, @UserName, @FullName, @Email, @EM_Access, @FM_Access, GETDATE(), NULL, @InternalUser

SELECT * FROM sec_tableau_users WHERE user_name = @UserName
