DECLARE @Client VARCHAR(1000) = 'Saint Leo University';
DECLARE @UserName VARCHAR(500) = 'Jen.McMahon@saintleo.RNL';
DECLARE @FullName VARCHAR(1000) = 'Jen McMahon';
DECLARE @Email VARCHAR(500) = 'Jen.McMahon@saintleo.RNL';
DECLARE @EM_Access VARCHAR(3) = '0';
DECLARE @FM_Access VARCHAR(3) = '1';
DECLARE @InternalUser CHAR(1) = '0';

INSERT INTO sec_tableau_users
SELECT @Client, @UserName, @FullName, @Email, @EM_Access, @FM_Access, GETDATE(), NULL, @InternalUser

SELECT * FROM sec_tableau_users WHERE user_name = @UserName
