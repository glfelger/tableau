insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management','True Cost Calculator Performance', 'https://reporting.ruffalonl.com/#/views/TCC/CalculatorSummary','Y','Dashboard','AFAS Reporting', 'Class Optimizer')
;
commit;