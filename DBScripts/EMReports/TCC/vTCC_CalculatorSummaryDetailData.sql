


/*
	TCC Calculator Summary Data
	This view [TCC_DataMart].[dbo].vTCC_CalculatorSummaryDetailData is called by the custom SQL query in Tableau Calculator Summary dashboard.
	Rob C. 05/03/2019 Updated
*/

SET NOCOUNT ON;
SET XACT_ABORT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


USE TCC_DataMart
GO

IF object_id(N'vTCC_CalculatorSummaryDetailData', 'V') IS NOT NULL
	DROP VIEW vTCC_CalculatorSummaryDetailData
GO

CREATE VIEW [dbo].vTCC_CalculatorSummaryDetailData AS

SELECT
	S.StudentKey,
	S.FirstName,  
	S.Middle, 
	S.LastName,  
	S.Address1, 
	S.Address2 ,
	S.City,
	S.StudentStateAbbrev,
	S.[StudentStateName],
	S.ZipCode ,
	S.Phone, 
	S.Email ,
	S.DateOfBirth ,
	S.GraduationYear, 
	I.InstitutionKey,
	I.InstitutionName,
	I.InstitutionType,
	I.InstitutionStateName,
	I.RegionName,
	I.Active as Institution_Active,
	SV.SurveyKey,
	SV.SurveyName,
	SV.IsActive AS Survey_IsActive, 
	S.PopulationDecisionID,
	PD.PopulationName,
	PD.Description AS Pop_Description,
	PD.Formula as Pop_Formula,
	PD.Active as Pop_Active,
	S.ContactMe,
	S.AcademicLevel,
	S.AcademicLevelName,
	S.RoomBoardTypeID,
	S.RoomBoardTypeName,
	S.ContactMeType,
	S.IsEFC,
	S.[PopulationFlexGridTypeId],
	S.CalcDate,
	S.LastUpdateDate,
	S.Age,
	S.IncludeRoomAndBoardOffCampus,
	S.IncludeRoomAndBoardOnCampus,
	S.IncludeRoomAndBoardWithParent,
	F.[Need],
	F.[CostOther], 
	F.[CostTotal], 
	F.[EFC], 
	F.[ScholarshipTotal], 
	F.[Gift], 
	F.[OtherAid], 
	F.[TrueCost], 
	F.[TotalInvestment], 
	F.[CostDirectTotal], 
	F.[Pell], 
	F.[OtherGiftAid], 
	F.[Loan], 
	F.[OtherLoan], 
	F.[TrueDirectCost], 
	F.[TotalDirectInvestment], 
	F.[StateGrant], 
	F.[ParentContribution], 
	F.[StudentContribution], 
	F.[FlexAmount], 
	F.[TotalFlexGrantAmount], 
	F.[TotalFlexLoanAmount], 
	F.[Over_All_Responded], 
	F.[Scholarship_Complete], 
	F.[Over_All_Complete], 
	F.[Email_plus_contact_me], 
	F.[Email_plus_contact_me_Scholarship], 
	F.[Email_but_no_contact_me], 
	F.[Email_but_no_contact_me_Scholarship],
	F.[NeedMin], 
	F.[EFMax], 
	F.[ACTCOMP], 
	F.[HSGPA],
	F.[CGPA],
	F.[SATCOMP1600],
	I.Users
FROM [dbo].[Fact_TCC_StudentFinancial] F 
JOIN [dbo].[Dim_TCC_Student] S ON F.StudentKey = S.StudentKey
JOIN [dbo].[Dim_TCC_Survey] SV ON F.SurveyKey = SV.SurveyKey
JOIN [dbo].[Dim_TCC_Institution] I ON F.InstitutionKey = I.InstitutionKey
LEFT JOIN [dbo].[Dim_TCC_PopulationDecision] PD ON S.PopulationDecisionID = PD.PopulationDecisionKey
WHERE SV.IsActive = 1 AND I.Active = 1


