DROP VIEW IF EXISTS edw.financial_aid_matrix_student_view
;
commit
;
CREATE OR REPLACE VIEW edw.financial_aid_matrix_student_view
(
  client,
  academicyearid,
  studentid,
  l_name,
  f_name,
  submitted_date,
  sub_population,
  student_status,
  city,
  state,
  academictiersequence,
  financialtiersequence,
  gender,
  housing,
  major1_admit,
  efc_9_month,
  need,
  gap,
  gift,
  i__g
)
AS 
 SELECT i.institution_name AS client, a.academic_year_id AS academicyearid, s.studentid, s.last_name AS l_name, s.first_name AS f_name, dds.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.student_status, s.scity AS city, s.state, "at".display_sequence AS academictiersequence, ft.display_sequence AS financialtiersequence, s.gender, s.housing, s.major1_admit, fsd.efc_9_month, fsd.need, fsd.gap, fsd.gift, fsd.i__g
   FROM (( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'ADMITTED'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'YIELD RATE'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_admitted = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar AND (i.institution_name::text = 'Augustana College'::character varying::text OR i.institution_name::text = 'Mercer University'::character varying::text OR i.institution_name::text = 'Marquette University'::character varying::text OR i.institution_name::text = 'Bowling Green State University'::character varying::text OR i.institution_name::text = 'Carroll University'::character varying::text OR i.institution_name::text = 'Roberts Wesleyan College'::character varying::text);

  ;
commit
;

GRANT SELECT ON edw.financial_aid_matrix_student_view TO reportinguser;

commit;

DROP VIEW IF EXISTS edw.financial_aid_matrix_view
;
commit
;
CREATE OR REPLACE VIEW edw.financial_aid_matrix_view
(
  client,
  academic_year,
  academicyearid,
  year,
  submitted_date,
  sub_population,
  population_order,
  institution_key,
  academictiersequence,
  actual_academictiersequence,
  financialtiersequence,
  actual_financialtiersequence,
  a_tier_name,
  fin_tier_name,
  student_status,
  category,
  need,
  need_wgift,
  gift,
  gift_w_need,
  i__g,
  tuition,
  fees,
  student_count,
  admitted_count,
  net_confirmed_count,
  pending_count
)
AS 
 SELECT i.institution_name AS client, a.academic_start_year AS academic_year, a.academic_year_id AS academicyearid, a.academic_year_name AS "year", dds.transmit_date AS submitted_date, p.population_name AS sub_population, p.population_order, fsd.institution_key, "at".display_sequence AS academictiersequence, at2.display_sequence AS actual_academictiersequence, ft.display_sequence AS financialtiersequence, ft2.display_sequence AS actual_financialtiersequence, "at".name AS a_tier_name, ft.name AS fin_tier_name, fsd.student_status, fsd.category, sum(fsd.need) AS need, sum(fsd.need_wgift) AS need_wgift, sum(fsd.gift) AS gift, sum(fsd.gift_w_need) AS gift_w_need, sum(fsd.i__g) AS i__g, sum(fsd.tuition) AS tuition, sum(fsd.fees) AS fees, sum(fsd.student_count) AS student_count, sum(fsd.admitted_count) AS admitted_count, sum(fsd.net_confirmed_count) AS net_confirmed_count, sum(fsd.pending_count) AS pending_count
   FROM (((((( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, 
                CASE
                    WHEN COALESCE(fact_osds_student_detail.need, 0) = 0 THEN 0
                    ELSE fact_osds_student_detail.gift
                END AS gift_w_need, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'ADMITTED'::character varying AS student_status, 1 AS student_count, 1 AS admitted_count, 0 AS net_confirmed_count, 0 AS pending_count, 'NONE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, 
                CASE
                    WHEN COALESCE(fact_osds_student_detail.need, 0) = 0 THEN 0
                    ELSE fact_osds_student_detail.gift
                END AS gift_w_need, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'NONE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, 
                CASE
                    WHEN COALESCE(fact_osds_student_detail.need, 0) = 0 THEN 0
                    ELSE fact_osds_student_detail.gift
                END AS gift_w_need, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'AVERAGE INSTITUTIONAL GIFT'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, 
                CASE
                    WHEN COALESCE(fact_osds_student_detail.need, 0) = 0 THEN 0
                    ELSE fact_osds_student_detail.gift
                END AS gift_w_need, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'AVERAGE NET TUITION AND FEE REVENUE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, 
                CASE
                    WHEN COALESCE(fact_osds_student_detail.need, 0) = 0 THEN 0
                    ELSE fact_osds_student_detail.gift
                END AS gift_w_need, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'AVERAGE NEEED'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, 
                CASE
                    WHEN COALESCE(fact_osds_student_detail.need, 0) = 0 THEN 0
                    ELSE fact_osds_student_detail.gift
                END AS gift_w_need, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'MET WITH GIFT'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, 
                CASE
                    WHEN COALESCE(fact_osds_student_detail.need, 0) = 0 THEN 0
                    ELSE fact_osds_student_detail.gift
                END AS gift_w_need, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'YIELD RATE'::character varying AS student_status, 1 AS student_count, fact_osds_student_detail.flag_admitted AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, 0 AS pending_count, 'NONE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_admitted = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN ((( SELECT sp.student_detail_key, sp.population_key, sp.financial_tier_key, sp.financial_tier_key AS actual_f_tier_key, sp.academic_tier_key, sp.academic_tier_key AS actual_a_tier_key
   FROM edw.dim_osds_student_population sp
UNION ALL 
 SELECT sp.student_detail_key, sp.population_key, -99 AS financial_tier_key, sp.financial_tier_key AS actual_f_tier_key, sp.academic_tier_key, sp.academic_tier_key AS actual_a_tier_key
   FROM edw.dim_osds_student_population sp)
UNION ALL 
 SELECT sp.student_detail_key, sp.population_key, sp.financial_tier_key, sp.financial_tier_key AS actual_f_tier_key, -99 AS academic_tier_key, sp.academic_tier_key AS actual_a_tier_key
   FROM edw.dim_osds_student_population sp)
UNION ALL 
 SELECT sp.student_detail_key, sp.population_key, -99 AS financial_tier_key, sp.financial_tier_key AS actual_f_tier_key, -99 AS academic_tier_key, sp.academic_tier_key AS actual_a_tier_key
   FROM edw.dim_osds_student_population sp) sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN ( SELECT dim_osds_financial_tier.financial_tier_key, dim_osds_financial_tier.financial_tier_id, dim_osds_financial_tier.name, dim_osds_financial_tier.display_sequence
   FROM edw.dim_osds_financial_tier
UNION ALL 
 SELECT -99 AS financial_tier_key, 100 AS financial_tier_id, 'Total'::character varying AS name, 100 AS display_sequence) ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_financial_tier ft2 ON ft2.financial_tier_key = sp.actual_f_tier_key
   JOIN ( SELECT dim_osds_academic_tier.academic_tier_key, dim_osds_academic_tier.academic_tier_id, dim_osds_academic_tier.name, dim_osds_academic_tier.display_sequence
   FROM edw.dim_osds_academic_tier
UNION ALL 
 SELECT -99 AS academic_tier_key, 100 AS academic_tier_id, 'Total'::character varying AS name, 100 AS display_sequence) "at" ON "at".academic_tier_key = sp.academic_tier_key
   JOIN edw.dim_osds_academic_tier at2 ON at2.academic_tier_key = sp.actual_a_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar AND (i.institution_name::text = 'Augustana College'::character varying::text OR i.institution_name::text = 'Mercer University'::character varying::text OR i.institution_name::text = 'Marquette University'::character varying::text OR i.institution_name::text = 'Bowling Green State University'::character varying::text OR i.institution_name::text = 'Carroll University'::character varying::text OR i.institution_name::text = 'Roberts Wesleyan College'::character varying::text)
  GROUP BY i.institution_name, a.academic_start_year, a.academic_year_id, a.academic_year_name, dds.transmit_date, p.population_name, p.population_order, fsd.institution_key, "at".display_sequence, at2.display_sequence, ft.display_sequence, ft2.display_sequence, "at".name, ft.name, fsd.student_status, fsd.category;

  ;
commit
;

GRANT SELECT ON edw.financial_aid_matrix_view TO reportinguser;


commit;


CREATE OR REPLACE VIEW edw.financial_aid_matrix_goals
(
  client,
  category,
  year,
  sub_population,
  institution_key,
  academic_year_key,
  financial_tier_key,
  population_key,
  academic_tier_key,
  cellnumber,
  goal_student_count,
  goal_admitted_count,
  goal_net_confirmed,
  total_need,
  averageneed,
  percentneedmetwithgift,
  averageinstitutionalaid,
  averagenettuitionrevenue,
  avg_net_tuit_rev,
  nettuitionfeerevenue,
  netrevenue,
  total_aid,
  avg_inst_aid,
  total_gifts,
  a_tier_name,
  f_tier_name,
  f_tier_display_sequence,
  a_tier_display_sequence,
  student_status
)
AS 
 SELECT i.institution_name AS client, fgd.category, a.academic_year_name AS "year", p.population_name AS sub_population, fgd.institution_key, fgd.academic_year_key, fgd.financial_tier_key, fgd.population_key, fgd.academic_tier_key, fgd.cellnumber, fgd.goal_student_count, fgd.goal_admitted_count, fgd.goal_net_confirmed, fgd.goal_student_count * fgd.averageneed AS total_need, fgd.averageneed, fgd.percentneedmetwithgift, fgd.institutionalgiftaid AS averageinstitutionalaid, fgd.averagenetrevenue AS averagenettuitionrevenue, fgd.averagenettuitionrevenue AS avg_net_tuit_rev, fgd.nettuitionfeerevenue, fgd.netrevenue, fgd.total_aid, fgd.averageinstitutionalaid AS avg_inst_aid, fgd.total_gifts * fgd.goal_student_count AS total_gifts, "at".name AS a_tier_name, ft.name AS f_tier_name, ft.display_sequence AS f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence, fgd.student_status
   FROM (((((( SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, 0 AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'NONE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 'NET CONFIRMED'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.admittedcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, 0 AS goal_net_confirmed, 'NONE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 'ADMITTED'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.admittedcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'NONE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 'YIELD RATE'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'AVERAGE INSTITUTIONAL GIFT'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 'NET CONFIRMED'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'AVERAGE NET TUITION AND FEE REVENUE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 'NET CONFIRMED'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'AVERAGE NEEED'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 'NET CONFIRMED'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'MET WITH GIFT'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 'NET CONFIRMED'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue) fgd
   JOIN dim_osds_institution i ON fgd.institution_key = i.institution_key
   JOIN dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN dim_osds_population p ON fgd.population_key = p.population_key
   JOIN ( SELECT dim_osds_financial_tier.financial_tier_key, dim_osds_financial_tier.financial_tier_id, dim_osds_financial_tier.name, dim_osds_financial_tier.display_sequence
   FROM dim_osds_financial_tier
UNION ALL 
 SELECT dim_osds_financial_tier.financial_tier_key, 100 AS financial_tier_id, 'Total'::character varying AS name, 100 AS display_sequence
   FROM dim_osds_financial_tier) ft ON ft.financial_tier_key = fgd.financial_tier_key
   LEFT JOIN dim_osds_financial_tier ft2 ON fgd.financial_tier_key = ft2.financial_tier_key
   JOIN ( SELECT dim_osds_academic_tier.academic_tier_key, dim_osds_academic_tier.academic_tier_id, dim_osds_academic_tier.name, dim_osds_academic_tier.display_sequence
   FROM dim_osds_academic_tier
UNION ALL 
 SELECT dim_osds_academic_tier.academic_tier_key, 100 AS academic_tier_id, 'Total'::character varying AS name, 100 AS display_sequence
   FROM dim_osds_academic_tier) "at" ON "at".academic_tier_key = fgd.academic_tier_key
   LEFT JOIN dim_osds_academic_tier at2 ON fgd.academic_tier_key = at2.academic_tier_key
  WHERE p.flag_active::character varying::text = 1::character varying::text AND fgd.cellnumber < 100
 ;
commit
;
GRANT SELECT ON edw.financial_aid_matrix_goals TO reportinguser;
commit
;