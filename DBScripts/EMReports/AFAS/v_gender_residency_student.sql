DROP VIEW IF EXISTS edw.v_gender_residency_student;

COMMIT;
CREATE OR REPLACE VIEW edw.v_gender_residency_student
(
  institution_key,
  client,
  submitted_date,
  sub_population,
  student_status,
  student_id,
  last_name,
  first_name,
  szip_code,
  gender,
  state,
  housing,
  counselor,
  flag_athlete,
  sport1,
  gender_name,
  housing_name,
  i__g,
  in_g,
  ip_g,
  im_g,
  academictiersequence,
  financialtiersequence
)
AS 
 SELECT fsd.institution_key, i.institution_name AS client, d.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.student_status, sd.studentid AS student_id, sd.last_name, sd.first_name, sd.szip_code, sd.gender, sd.state, sd.housing, sd.counselor, sd.flag_athlete, sd.sport1, 
        CASE
            WHEN lower("left"(sd.gender::text, 1)) = 'f'::character varying::text THEN 'Female'::character varying
            WHEN lower("left"(sd.gender::text, 1)) = 'm'::character varying::text THEN 'Male'::character varying
            ELSE 'Other'::character varying
        END AS gender_name, 
        CASE
            WHEN lower(sd.housing::text) = 'r'::character varying::text THEN 'In-Hall Residence'::character varying
            WHEN lower(sd.housing::text) = 'c'::character varying::text THEN 'Off Campus (not living with parents)'::character varying
            WHEN lower(sd.housing::text) = 'p'::character varying::text THEN 'Living with Parents/Relatives'::character varying
            ELSE 'Unknown'::character varying
        END AS housing_name, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, "at".display_sequence AS academictiersequence, ft.display_sequence AS financialtiersequence
   FROM ( SELECT fsd.student_detail_key, fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.academic_year_key, 'Admitted'::character varying AS student_status, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.in_g, 0) AS in_g, COALESCE(fsd.ip_g, 0) AS ip_g, COALESCE(fsd.im_g, 0) AS im_g
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_admitted = 1
UNION ALL 
         SELECT fsd.student_detail_key, fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.academic_year_key, 
                CASE
                    WHEN fsd.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fsd.flag_confirmed_cancelled = 1 THEN 'Confirmed Cancelled'::character varying
                    WHEN fsd.flag_canc = 1 AND fsd.flag_con = 0 THEN 'Cancelled'::character varying
                    ELSE 'Pending'::character varying
                END AS student_status, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.in_g, 0) AS in_g, COALESCE(fsd.ip_g, 0) AS ip_g, COALESCE(fsd.im_g, 0) AS im_g
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_net_confirmed = 1 OR fsd.flag_confirmed_cancelled = 1 OR fsd.flag_pending = 1 OR fsd.flag_canc = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset d ON fsd.dataset_key = d.dataset_key AND fsd.institution_key = d.institution_key
   JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON a.academic_year_key = fsd.academic_year_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE d.flag_active = '1'::bpchar AND p.flag_active::character varying::text = 1::character varying::text 
  AND d.process_type_id = 2 AND d.dataset_status_id = 4 
  ;
commit;
GRANT SELECT ON edw.v_gender_residency_student TO reportinguser;
commit;