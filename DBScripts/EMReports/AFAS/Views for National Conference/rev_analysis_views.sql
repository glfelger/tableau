DROP VIEW IF EXISTS edw.agg_afas_rev_analysis_report_summary
;
commit
;
DROP VIEW IF EXISTS edw.agg_afas_rev_analysis
;
commit
;
DROP VIEW IF EXISTS edw.agg_afas_rev_analysis_goals1
;
commit
;
CREATE OR REPLACE VIEW edw.agg_afas_rev_analysis_goals1
(
  client,
  year,
  fiscal_year,
  sub_population,
  fin_tier_name,
  fin_tier_display_sequence,
  a_tier_display_sequence,
  student_status,
  student_count,
  sum_tuitionrevenue,
  sum_requiredfeerevenue,
  sum_roomboardrevenue,
  sum_totalrevenue,
  sum_unfundedinstitutionalgiftaid,
  sum_fundedinstitutionalgiftaid,
  sum_tuitionexchangewaivers,
  sum_premieracademic,
  sum_roomboard,
  sum_campusbasedwork,
  sum_nettuitionfeerevenue,
  sum_overallincludingfees,
  sum_insneed,
  sum_insmerit,
  sum_talentgift,
  sum_otheraid,
  sum_totalaid,
  sum_unfundedtuitiondiscount,
  sum_tuitiondiscountrate,
  sum_overallunfundeddiscount,
  sum_unfundedroomboarddiscount,
  tuitionrevenue,
  requiredfeerevenue,
  roomboardrevenue,
  totalrevenue,
  nettuitionfeerevenue,
  netrevenue,
  overallincludingfees,
  unfundedinstitutionalgiftaid,
  roomboard,
  fundedinstitutionalgiftaid,
  unfundedtuitiondiscount,
  unfundedroomboarddiscount,
  overallunfundeddiscount,
  tuitiondiscountrate,
  institutionalgiftaid
)
AS 
 SELECT derived_table1.client, derived_table1."year", derived_table1.fiscal_year, derived_table1.sub_population, derived_table1.fin_tier_name, derived_table1.fin_tier_display_sequence, derived_table1.a_tier_display_sequence, derived_table1.student_status, derived_table1.student_count, derived_table1.sum_tuitionrevenue, derived_table1.sum_requiredfeerevenue, derived_table1.sum_roomboardrevenue, derived_table1.sum_totalrevenue, derived_table1.sum_unfundedinstitutionalgiftaid, derived_table1.sum_fundedinstitutionalgiftaid, derived_table1.sum_tuitionexchangewaivers, derived_table1.sum_premieracademic, derived_table1.sum_roomboard, derived_table1.sum_campusbasedwork, derived_table1.sum_nettuitionfeerevenue, derived_table1.sum_overallincludingfees, derived_table1.sum_insneed, derived_table1.sum_insmerit, derived_table1.sum_talentgift, derived_table1.sum_otheraid, derived_table1.sum_totalaid, derived_table1.sum_unfundedtuitiondiscount, derived_table1.sum_tuitiondiscountrate, derived_table1.sum_overallunfundeddiscount, derived_table1.sum_unfundedroomboarddiscount, derived_table1.tuitionrevenue, derived_table1.requiredfeerevenue, derived_table1.roomboardrevenue, derived_table1.totalrevenue, derived_table1.nettuitionfeerevenue, derived_table1.netrevenue, derived_table1.overallincludingfees, derived_table1.unfundedinstitutionalgiftaid, derived_table1.roomboard, derived_table1.fundedinstitutionalgiftaid, derived_table1.unfundedtuitiondiscount, derived_table1.unfundedroomboarddiscount, derived_table1.overallunfundeddiscount, derived_table1.tuitiondiscountrate, derived_table1.institutionalgiftaid
   FROM ( SELECT i.institution_name AS client, a.academic_year_name AS "year", a.academic_start_year AS fiscal_year, p.population_name AS sub_population, ft.name AS fin_tier_name, 
                CASE
                    WHEN fgd.cellnumber >= 2001 AND fgd.cellnumber <= 2015 THEN fgd.cellnumber
                    ELSE 99
                END AS fin_tier_display_sequence, 
                CASE
                    WHEN fgd.cellnumber >= 1001 AND fgd.cellnumber <= 1007 THEN fgd.cellnumber
                    ELSE 99
                END AS a_tier_display_sequence, 'Net Confirmed'::character varying AS student_status, fgd.enrolledcount AS student_count, 0 AS sum_tuitionrevenue, 0 AS sum_requiredfeerevenue, 0 AS sum_roomboardrevenue, 0 AS sum_totalrevenue, 0 AS sum_unfundedinstitutionalgiftaid, 0 AS sum_fundedinstitutionalgiftaid, 0 AS sum_tuitionexchangewaivers, 0 AS sum_premieracademic, 0 AS sum_roomboard, 0 AS sum_campusbasedwork, 0 AS sum_nettuitionfeerevenue, 0 AS sum_overallincludingfees, 0 AS sum_insneed, 0 AS sum_insmerit, 0 AS sum_talentgift, 0 AS sum_otheraid, 0 AS sum_totalaid, 0 AS sum_unfundedtuitiondiscount, 0 AS sum_tuitiondiscountrate, 0 AS sum_overallunfundeddiscount, 0 AS sum_unfundedroomboarddiscount, fgd.tuitionrevenue, fgd.feerevenue AS requiredfeerevenue, fgd.roomboardrevenue, fgd.totalrevenue, fgd.netrevenue AS nettuitionfeerevenue, fgd.nettuitionfeerevenue AS netrevenue, fgd.overallincludingfees, fgd.unfundedinstitutionalgiftaid, fgd.roomboard, fgd.fundedinstitutionalgiftaid, fgd.unfundedtuitiondiscount, fgd.unfundedroomboarddiscount, fgd.overallunfundeddiscount, fgd.tuitiondiscountrate, fgd.institutionalgiftaid
           FROM edw.fact_osds_goal_detail_revenue fgd
      JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fgd.institution_key = i.institution_key
   LEFT JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   LEFT JOIN edw.dim_osds_financial_tier ft ON fgd.financial_tier_key = ft.financial_tier_key
  WHERE p.flag_active::character varying::text = 1::character varying::text AND fgd.source_type::text = 'GoalDetail'::character varying::text AND fgd.cellnumber >= 1000 AND fgd.cellnumber <= 2050
UNION ALL 
         SELECT DISTINCT i.institution_name AS client, a.academic_year_name AS "year", a.academic_start_year AS fiscal_year, p.population_name AS sub_population, ''::character varying AS fin_tier_name, 99 AS fin_tier_display_sequence, 99 AS a_tier_display_sequence, 'Net Confirmed'::character varying AS student_status, 0 AS student_count, fgd.tuitionrevenue AS sum_tuitionrevenue, fgd.requiredfeerevenue AS sum_requiredfeerevenue, fgd.roomboardrevenue AS sum_roomboardrevenue, fgd.totalrevenue AS sum_totalrevenue, fgd.unfundedinstitutionalgiftaid AS sum_unfundedinstitutionalgiftaid, fgd.fundedinstitutionalgiftaid AS sum_fundedinstitutionalgiftaid, fgd.tuitionexchangewaivers AS sum_tuitionexchangewaivers, fgd.premieracademic AS sum_premieracademic, fgd.roomboard AS sum_roomboard, fgd.campusbasedwork AS sum_campusbasedwork, fgd.nettuitionfeerevenue AS sum_nettuitionfeerevenue, fgd.overallincludingfees AS sum_overallincludingfees, fgd.ins_need AS sum_insneed, fgd.ins_merit AS sum_insmerit, fgd.talent_gift AS sum_talentgift, fgd.other_aid AS sum_otheraid, fgd.total_aid AS sum_totalaid, fgd.unfundedtuitiondiscount AS sum_unfundedtuitiondiscount, fgd.tuitiondiscountrate AS sum_tuitiondiscountrate, fgd.overallunfundeddiscount AS sum_overallunfundeddiscount, fgd.unfundedroomboarddiscount AS sum_unfundedroomboarddiscount, 0 AS tuitionrevenue, 0 AS requiredfeerevenue, 0 AS roomboardrevenue, 0 AS totalrevenue, 0 AS nettuitionfeerevenue, 0 AS netrevenue, 0 AS overallincludingfees, 0 AS unfundedinstitutionalgiftaid, 0 AS roomboard, 0 AS fundedinstitutionalgiftaid, 0 AS unfundedtuitiondiscount, 0 AS unfundedroomboarddiscount, 0 AS overallunfundeddiscount, 0 AS tuitiondiscountrate, 0 AS institutionalgiftaid
           FROM edw.fact_osds_goal_detail_revenue fgd
      JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fgd.institution_key = i.institution_key
   LEFT JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
  WHERE p.flag_active::character varying::text = 1::character varying::text AND fgd.source_type::text = 'GoalSummary'::character varying::text) derived_table1;

commit
;

CREATE OR REPLACE VIEW edw.agg_afas_rev_analysis
(
  client,
  year,
  fiscal_year,
  submitted_date,
  sub_population,
  fin_tier_name,
  fin_tier_display_sequence,
  a_tier_display_sequence,
  student_status,
  studentid,
  last_name,
  first_name,
  scity,
  state,
  housing,
  room_revenue,
  board_revenue,
  i_rg,
  i_fg,
  student_count,
  total_tuition_revenue,
  total_fees_revenue,
  total_room_board_revenue,
  total_overall_revenue,
  net_tuition_fee_revenue,
  net_room_board_revenue,
  net_overall_revenue,
  inst_gift_unfunded_gift_tuition,
  inst_gift_unfunded_room_board,
  inst_gift_funded_inst_gift,
  i__g,
  total_tuition_fees_revenue,
  i_ug,
  disrate_tuition_fees,
  disrate_tuition,
  disrate_overall_numerator,
  disrate_overall_denominator,
  act,
  act_student_count,
  academic_index,
  academic_index_student_count,
  gpa,
  gpa_student_count,
  sat,
  sat_student_count
)
AS 
 SELECT i.institution_name AS client, a.academic_year_name AS "year", a.academic_start_year AS fiscal_year, d.transmit_date AS submitted_date, p.population_name AS sub_population, ft.name AS fin_tier_name, ft.display_sequence AS fin_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence, fsd.student_status, 
 case when i.institution_name = 'RNL University' then 'ABC' + left(st.studentid,4) else st.studentid end AS studentid, 
case when i.institution_name = 'RNL University' then 'Student' else st.last_name end AS last_name,
  case when i.institution_name = 'RNL University' then 'Demo' else st.first_name end AS first_name, 
 case when i.institution_name = 'RNL University' then 'City' else st.scity end AS scity, 
 case when i.institution_name = 'RNL University' then 'ST' else st.state end as state, st.housing, sum(fsd.room_revenue) AS room_revenue, sum(fsd.board_revenue) AS board_revenue, sum(fsd.i_rg) AS i_rg, sum(fsd.i_fg) AS i_fg, sum(fsd.student_count) AS student_count, sum(fsd.total_tuition_revenue) AS total_tuition_revenue, sum(fsd.total_fees_revenue) AS total_fees_revenue, sum(fsd.total_room_board_revenue) AS total_room_board_revenue, sum(fsd.total_overall_revenue) AS total_overall_revenue, sum(fsd.net_tuition_fee_revenue) AS net_tuition_fee_revenue, sum(fsd.total_room_board_revenue - fsd.inst_gift_unfunded_room_board) AS net_room_board_revenue, sum(fsd.total_overall_revenue - fsd.inst_gift_unfunded_gift_tuition) AS net_overall_revenue, sum(fsd.inst_gift_unfunded_gift_tuition) AS inst_gift_unfunded_gift_tuition, sum(fsd.inst_gift_unfunded_room_board) AS inst_gift_unfunded_room_board, sum(fsd.inst_gift_funded_inst_gift) AS inst_gift_funded_inst_gift, sum(fsd.i__g) AS i__g, sum(fsd.total_tuition_fees_revenue) AS total_tuition_fees_revenue, sum(fsd.i_ug) AS i_ug, sum(fsd.disrate_tuition_fees) AS disrate_tuition_fees, sum(fsd.disrate_tuition) AS disrate_tuition, sum(fsd.disrate_overall_numerator) AS disrate_overall_numerator, sum(fsd.disrate_overall_denominator) AS disrate_overall_denominator, sum(fsd.act) AS act, sum(fsd.act_student_count) AS act_student_count, sum(fsd.academic_index) AS academic_index, sum(fsd.academic_index_student_count) AS academic_index_student_count, sum(fsd.gpa) AS gpa, sum(fsd.gpa_student_count) AS gpa_student_count, sum(fsd.sat) AS sat, sum(fsd.sat_student_count) AS sat_student_count
   FROM (( SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, 'Admitted'::character varying AS student_status, fsd.flag_admitted, fsd.flag_gross_confirmed, fsd.flag_net_confirmed, fsd.flag_confirmed_cancelled, fsd.flag_pending, COALESCE(fsd.room, 0) AS room_revenue, COALESCE(fsd.board, 0) AS board_revenue, COALESCE(fsd.i_rg, 0) AS i_rg, COALESCE(fsd.i_fg, 0) AS i_fg, 1 AS student_count, COALESCE(fsd.tuition, 0) AS total_tuition_revenue, COALESCE(fsd.fees, 0) AS total_fees_revenue, COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) AS total_room_board_revenue, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) + COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) AS total_overall_revenue, COALESCE(fsd.net_tuition_fee_revenue, 0) AS net_tuition_fee_revenue, COALESCE(fsd.net_room_board_revenue, 0) AS net_room_board_revenue, COALESCE(fsd.net_overall_revenue, 0) AS net_overall_revenue, COALESCE(fsd.i_ug, 0) AS inst_gift_unfunded_gift_tuition, COALESCE(fsd.i_rg, 0) AS inst_gift_unfunded_room_board, COALESCE(fsd.i_fg, 0) AS inst_gift_funded_inst_gift, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) AS total_tuition_fees_revenue, COALESCE(fsd.i_ug, 0) AS i_ug, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) - COALESCE(fsd.empb, 0) AS disrate_tuition_fees, COALESCE(fsd.tuition, 0) - COALESCE(fsd.empb, 0) AS disrate_tuition, COALESCE(fsd.i_ug, 0) + COALESCE(fsd.i_rg, 0) AS disrate_overall_numerator, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) + COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) + COALESCE(fsd.other_chgs, 0) - COALESCE(fsd.empb, 0) AS disrate_overall_denominator, fsd.act_comp AS act, 
                CASE
                    WHEN fsd.act_comp > 0 THEN 1
                    ELSE 0
                END AS act_student_count, COALESCE(fsd.inst_rat, 0::double precision) AS academic_index, 
                CASE
                    WHEN fsd.inst_rat > 0::double precision THEN 1
                    ELSE 0
                END AS academic_index_student_count, COALESCE(fsd.hsgpa, 0::double precision) AS gpa, 
                CASE
                    WHEN fsd.hsgpa > 0::double precision THEN 1
                    ELSE 0
                END AS gpa_student_count, COALESCE(fsd.sat_comp, 0) AS sat, 
                CASE
                    WHEN fsd.sat_comp > 0 THEN 1
                    ELSE 0
                END AS sat_student_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_admitted = 1
UNION ALL 
         SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, 'Net Confirmed'::character varying AS student_status, fsd.flag_admitted, fsd.flag_gross_confirmed, fsd.flag_net_confirmed, fsd.flag_confirmed_cancelled, fsd.flag_pending, COALESCE(fsd.room, 0) AS room_revenue, COALESCE(fsd.board, 0) AS board_revenue, COALESCE(fsd.i_rg, 0) AS i_rg, COALESCE(fsd.i_fg, 0) AS i_fg, 1 AS student_count, COALESCE(fsd.tuition, 0) AS total_tuition_revenue, COALESCE(fsd.fees, 0) AS total_fees_revenue, COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) AS total_room_board_revenue, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) + COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) AS total_overall_revenue, COALESCE(fsd.net_tuition_fee_revenue, 0) AS net_tuition_fee_revenue, COALESCE(fsd.net_room_board_revenue, 0) AS net_room_board_revenue, COALESCE(fsd.net_overall_revenue, 0) AS net_overall_revenue, COALESCE(fsd.i_ug, 0) AS inst_gift_unfunded_gift_tuition, COALESCE(fsd.i_rg, 0) AS inst_gift_unfunded_room_board, COALESCE(fsd.i_fg, 0) AS inst_gift_funded_inst_gift, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) AS total_tuition_fees_revenue, COALESCE(fsd.i_ug, 0) AS i_ug, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) - COALESCE(fsd.empb, 0) AS disrate_tuition_fees, COALESCE(fsd.tuition, 0) - COALESCE(fsd.empb, 0) AS disrate_tuition, COALESCE(fsd.i_ug, 0) + COALESCE(fsd.i_rg, 0) AS disrate_overall_numerator, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) + COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) + COALESCE(fsd.other_chgs, 0) - COALESCE(fsd.empb, 0) AS disrate_overall_denominator, COALESCE(fsd.act_comp, 0) AS act, 
                CASE
                    WHEN fsd.act_comp > 0 THEN 1
                    ELSE 0
                END AS act_student_count, COALESCE(fsd.inst_rat, 0::double precision) AS academic_index, 
                CASE
                    WHEN fsd.inst_rat > 0::double precision THEN 1
                    ELSE 0
                END AS academic_index_student_count, COALESCE(fsd.hsgpa, 0::double precision) AS gpa, 
                CASE
                    WHEN fsd.hsgpa > 0::double precision THEN 1
                    ELSE 0
                END AS gpa_student_count, COALESCE(fsd.sat_comp, 0) AS sat, 
                CASE
                    WHEN fsd.sat_comp > 0 THEN 1
                    ELSE 0
                END AS sat_student_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_net_confirmed = 1)
UNION ALL 
         SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, 'Pending'::character varying AS student_status, fsd.flag_admitted, fsd.flag_gross_confirmed, fsd.flag_net_confirmed, fsd.flag_confirmed_cancelled, fsd.flag_pending, COALESCE(fsd.room, 0) AS room_revenue, COALESCE(fsd.board, 0) AS board_revenue, COALESCE(fsd.i_rg, 0) AS i_rg, COALESCE(fsd.i_fg, 0) AS i_fg, 1 AS student_count, COALESCE(fsd.tuition, 0) AS total_tuition_revenue, COALESCE(fsd.fees, 0) AS total_fees_revenue, COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) AS total_room_board_revenue, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) + COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) AS total_overall_revenue, COALESCE(fsd.net_tuition_fee_revenue, 0) AS net_tuition_fee_revenue, COALESCE(fsd.net_room_board_revenue, 0) AS net_room_board_revenue, COALESCE(fsd.net_overall_revenue, 0) AS net_overall_revenue, COALESCE(fsd.i_ug, 0) AS inst_gift_unfunded_gift_tuition, COALESCE(fsd.i_rg, 0) AS inst_gift_unfunded_room_board, COALESCE(fsd.i_fg, 0) AS inst_gift_funded_inst_gift, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) AS total_tuition_fees_revenue, COALESCE(fsd.i_ug, 0) AS i_ug, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) - COALESCE(fsd.empb, 0) AS disrate_tuition_fees, COALESCE(fsd.tuition, 0) - COALESCE(fsd.empb, 0) AS disrate_tuition, COALESCE(fsd.i_ug, 0) + COALESCE(fsd.i_rg, 0) AS disrate_overall_numerator, COALESCE(fsd.tuition, 0) + COALESCE(fsd.fees, 0) + COALESCE(fsd.room, 0) + COALESCE(fsd.board, 0) + COALESCE(fsd.other_chgs, 0) - COALESCE(fsd.empb, 0) AS disrate_overall_denominator, COALESCE(fsd.act_comp, 0) AS act, 
                CASE
                    WHEN fsd.act_comp > 0 THEN 1
                    ELSE 0
                END AS act_student_count, COALESCE(fsd.inst_rat, 0::double precision) AS academic_index, 
                CASE
                    WHEN fsd.inst_rat > 0::double precision THEN 1
                    ELSE 0
                END AS academic_index_student_count, COALESCE(fsd.hsgpa, 0::double precision) AS gpa, 
                CASE
                    WHEN fsd.hsgpa > 0::double precision THEN 1
                    ELSE 0
                END AS gpa_student_count, COALESCE(fsd.sat_comp, 0) AS sat, 
                CASE
                    WHEN fsd.sat_comp > 0 THEN 1
                    ELSE 0
                END AS sat_student_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_pending = 1) fsd
   JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset d ON fsd.dataset_key = d.dataset_key AND fsd.institution_key = d.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_student st ON fsd.student_key = st.student_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE d.flag_active = '1'::bpchar AND p.flag_active::text = 1::text AND d.process_type_id = 2 AND d.dataset_status_id = 4 AND "date_part"('year'::character varying::text, d.transmit_date) >= ("date_part"('year'::character varying::text, 'now'::character varying::date) - 2)
  GROUP BY i.institution_name, a.academic_year_name, a.academic_start_year, d.transmit_date, p.population_name, ft.name, ft.display_sequence, "at".display_sequence, fsd.student_status, st.studentid, st.last_name, st.first_name, st.scity, st.state, st.housing
;

commit;

CREATE OR REPLACE VIEW edw.agg_afas_rev_analysis_report_summary
(
  client,
  year,
  fiscal_year,
  submitted_date,
  ly_submitted_date,
  max_ly_submitted_date,
  sub_population,
  fin_tier_name,
  fin_tier_display_sequence,
  a_tier_display_sequence,
  student_status,
  goal_student_count,
  cy_student_count,
  ly_student_count,
  max_ly_student_count,
  goal_summary_tuition_revenue,
  goal_summary_total_fees_revenue,
  goal_summary_total_room_board_revenue,
  goal_summary_total_revenue,
  goal_summary_inst_gift_unfunded_tuition,
  goal_summary_inst_gift_funded_gift,
  goal_summary_tuition_exchange_waivers,
  goal_summary_premier_academic,
  goal_summary_room_board,
  goal_summary_campus_based_work,
  goal_summary_net_tuition_fee_revenue,
  goal_summary_overall_including_fees,
  goal_summary_total_inst_gift_aid,
  goal_summary_unfunded_tuition_discount,
  goal_summary_tuition_discount_rate,
  goal_summary_overall_unfunded_discount,
  goal_summary_unfunded_room_board_discount,
  goal_total_tuition_revenue,
  goal_total_fees_revenue,
  goal_toal_room_board_revenue,
  goal_total_overall_revenue,
  cy_total_tuition_revenue,
  cy_total_fees_revenue,
  cy_total_room_board_revenue,
  cy_total_overall_revenue,
  ly_total_tuition_revenue,
  ly_total_fees_revenue,
  ly_total_room_board_revenue,
  ly_total_overall_revenue,
  max_ly_total_tuition_revenue,
  max_ly_total_fees_revenue,
  max_ly_total_room_board_revenue,
  max_ly_total_overall_revenue,
  goal_net_tuition_fee,
  goal_net_revenue,
  goal_net_room_board,
  goal_net_overall_revenue,
  cy_net_tuition_fee_revenue,
  cy_net_room_board_revenue,
  cy_net_overall_revenue,
  ly_net_tuition_fee_revenue,
  ly_net_room_board_revenue,
  ly_net_overall_revenue,
  max_ly_net_tuition_fee_revenue,
  max_ly_net_room_board_revenue,
  max_ly_net_overall_revenue,
  goal_inst_gift_unfunded_tuition,
  goal_inst_gift_unfunded_room_board,
  goal_inst_gift_funded_gift,
  cy_inst_gift_unfunded_gift_tuition,
  cy_inst_gift_unfunded_room_board,
  cy_inst_gift_funded_inst_gift,
  ly_inst_gift_unfunded_gift_tuition,
  ly_inst_gift_unfunded_room_board,
  ly_inst_gift_funded_inst_gift,
  max_ly_inst_gift_unfunded_gift_tuition,
  max_ly_inst_gift_unfunded_room_board,
  max_ly_inst_gift_funded_inst_gift,
  goal_disrate_tuition_discount,
  goal_disrate_unfunded_tuition_discount,
  goal_disrate_unfunded_room_board,
  goal_disrate_overall_discount,
  cy_i__g,
  cy_total_tuition_fees_revenue,
  cy_i_ug,
  cy_disrate_tuition_fees, 
  cy_disrate_tuition, 
  cy_disrate_overall_numerator,
  cy_disrate_overall_denominator,
  ly_i__g,
  ly_total_tuition_fees_revenue,
  ly_i_ug,
  ly_disrate_tuition_fees, 
  ly_disrate_tuition, 
  ly_disrate_overall_numerator,
  ly_disrate_overall_denominator,
  max_ly_i__g,
  max_ly_total_tuition_fees_revenue,
  max_ly_i_ug,
  max_ly_disrate_tuition_fees, 
  max_ly_disrate_tuition, 
  max_ly_disrate_overall_numerator,
  max_ly_disrate_overall_denominator,
  cy_act,
  cy_academic_index,
  cy_gpa,
  cy_sat,
  cy_act_student_count,
  cy_academic_index_student_count,
  cy_gpa_student_count,
  cy_sat_student_count,
  ly_act,
  ly_academic_index,
  ly_gpa,
  ly_sat,
  ly_act_student_count,
  ly_academic_index_student_count,
  ly_gpa_student_count,
  ly_sat_student_count,
  max_ly_act,
  max_ly_academic_index,
  max_ly_gpa,
  max_ly_sat,
  max_ly_act_student_count,
  max_ly_academic_index_student_count,
  max_ly_gpa_student_count,
  max_ly_sat_student_count
)
AS 
(( SELECT gl.client, gl."year", gl.fiscal_year, cy.submitted_date, NULL::"unknown" AS ly_submitted_date, NULL::"unknown" AS max_ly_submitted_date, gl.sub_population, gl.fin_tier_name, "right"(gl.fin_tier_display_sequence::text, 2)::character varying AS fin_tier_display_sequence, (('Level'::character varying::text || ' '::character varying::text) || "right"(gl.a_tier_display_sequence::text, 1)::character varying::text)::character varying AS a_tier_display_sequence, gl.student_status, gl.student_count AS goal_student_count, NULL::"unknown" AS cy_student_count, NULL::"unknown" AS ly_student_count, NULL::"unknown" AS max_ly_student_count, gl.sum_tuitionrevenue AS goal_summary_tuition_revenue, gl.sum_requiredfeerevenue AS goal_summary_total_fees_revenue, gl.sum_roomboardrevenue AS goal_summary_total_room_board_revenue, gl.sum_totalrevenue AS goal_summary_total_revenue, gl.sum_unfundedinstitutionalgiftaid AS goal_summary_inst_gift_unfunded_tuition, gl.sum_fundedinstitutionalgiftaid AS goal_summary_inst_gift_funded_gift, gl.sum_tuitionexchangewaivers AS goal_summary_tuition_exchange_waivers, gl.sum_premieracademic AS goal_summary_premier_academic, gl.sum_roomboard AS goal_summary_room_board, gl.sum_campusbasedwork AS goal_summary_campus_based_work, gl.sum_nettuitionfeerevenue AS goal_summary_net_tuition_fee_revenue, gl.sum_overallincludingfees AS goal_summary_overall_including_fees, gl.sum_totalaid AS goal_summary_total_inst_gift_aid, gl.sum_unfundedtuitiondiscount AS goal_summary_unfunded_tuition_discount, gl.sum_tuitiondiscountrate AS goal_summary_tuition_discount_rate, gl.sum_overallunfundeddiscount AS goal_summary_overall_unfunded_discount, gl.sum_unfundedroomboarddiscount AS goal_summary_unfunded_room_board_discount, gl.tuitionrevenue AS goal_total_tuition_revenue, gl.requiredfeerevenue AS goal_total_fees_revenue, gl.roomboardrevenue AS goal_toal_room_board_revenue, gl.totalrevenue AS goal_total_overall_revenue, NULL::"unknown" AS cy_total_tuition_revenue, NULL::"unknown" AS cy_total_fees_revenue, NULL::"unknown" AS cy_total_room_board_revenue, NULL::"unknown" AS cy_total_overall_revenue, NULL::"unknown" AS ly_total_tuition_revenue, NULL::"unknown" AS ly_total_fees_revenue, NULL::"unknown" AS ly_total_room_board_revenue, NULL::"unknown" AS ly_total_overall_revenue, NULL::"unknown" AS max_ly_total_tuition_revenue, NULL::"unknown" AS max_ly_total_fees_revenue, NULL::"unknown" AS max_ly_total_room_board_revenue, NULL::"unknown" AS max_ly_total_overall_revenue, gl.nettuitionfeerevenue AS goal_net_tuition_fee, gl.netrevenue AS goal_net_revenue, gl.roomboard AS goal_net_room_board, gl.overallincludingfees AS goal_net_overall_revenue, NULL::"unknown" AS cy_net_tuition_fee_revenue, NULL::"unknown" AS cy_net_room_board_revenue, NULL::"unknown" AS cy_net_overall_revenue, NULL::"unknown" AS ly_net_tuition_fee_revenue, NULL::"unknown" AS ly_net_room_board_revenue, NULL::"unknown" AS ly_net_overall_revenue, NULL::"unknown" AS max_ly_net_tuition_fee_revenue, NULL::"unknown" AS max_ly_net_room_board_revenue, NULL::"unknown" AS max_ly_net_overall_revenue, gl.unfundedinstitutionalgiftaid AS goal_inst_gift_unfunded_tuition, gl.roomboardrevenue - gl.roomboard AS goal_inst_gift_unfunded_room_board, gl.fundedinstitutionalgiftaid AS goal_inst_gift_funded_gift, NULL::"unknown" AS cy_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS cy_inst_gift_unfunded_room_board, NULL::"unknown" AS cy_inst_gift_funded_inst_gift, NULL::"unknown" AS ly_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS ly_inst_gift_unfunded_room_board, NULL::"unknown" AS ly_inst_gift_funded_inst_gift, NULL::"unknown" AS max_ly_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS max_ly_inst_gift_unfunded_room_board, NULL::"unknown" AS max_ly_inst_gift_funded_inst_gift, gl.tuitiondiscountrate AS goal_disrate_tuition_discount, gl.unfundedtuitiondiscount AS goal_disrate_unfunded_tuition_discount, gl.unfundedroomboarddiscount AS goal_disrate_unfunded_room_board, gl.overallunfundeddiscount AS goal_disrate_overall_discount, NULL::"unknown" AS cy_i__g, NULL::"unknown" AS cy_total_tuition_fees_revenue, NULL::"unknown" AS cy_i_ug, NULL::"unknown" AS cy_disrate_tuition_fees, NULL::"unknown" AS cy_disrate_tuition, NULL::"unknown" AS cy_disrate_overall_numerator, NULL::"unknown" AS cy_disrate_overall_denominator, NULL::"unknown" AS ly_i__g, NULL::"unknown" AS ly_total_tuition_fees_revenue, NULL::"unknown" AS ly_i_ug, NULL::"unknown" AS ly_disrate_tuition_fees, NULL::"unknown" AS ly_disrate_tuition, NULL::"unknown" AS ly_disrate_overall_numerator, NULL::"unknown" AS ly_disrate_overall_denominator, NULL::"unknown" AS max_ly_i__g, NULL::"unknown" AS max_ly_total_tuition_fees_revenue, NULL::"unknown" AS max_ly_i_ug, NULL::"unknown" AS max_ly_disrate_tuition_fees, NULL::"unknown" AS max_ly_disrate_tuition, NULL::"unknown" AS max_ly_disrate_overall_numerator, NULL::"unknown" AS max_ly_disrate_overall_denominator, NULL::"unknown" AS cy_act, NULL::"unknown" AS cy_academic_index, NULL::"unknown" AS cy_gpa, NULL::"unknown" AS cy_sat, NULL::"unknown" AS cy_act_student_count, NULL::"unknown" AS cy_academic_index_student_count, NULL::"unknown" AS cy_gpa_student_count, NULL::"unknown" AS cy_sat_student_count, NULL::"unknown" AS ly_act, NULL::"unknown" AS ly_academic_index, NULL::"unknown" AS ly_gpa, NULL::"unknown" AS ly_sat, NULL::"unknown" AS ly_act_student_count, NULL::"unknown" AS ly_academic_index_student_count, NULL::"unknown" AS ly_gpa_student_count, NULL::"unknown" AS ly_sat_student_count, NULL::"unknown" AS max_ly_act, NULL::"unknown" AS max_ly_academic_index, NULL::"unknown" AS max_ly_gpa, NULL::"unknown" AS max_ly_sat, NULL::"unknown" AS max_ly_act_student_count, NULL::"unknown" AS max_ly_academic_index_student_count, NULL::"unknown" AS max_ly_gpa_student_count, NULL::"unknown" AS max_ly_sat_student_count
   FROM ( SELECT DISTINCT cy.client, cy."year", cy.submitted_date
           FROM edw.agg_afas_rev_analysis cy
      JOIN ( SELECT agg_afas_rev_analysis.client AS maxclient, "max"(agg_afas_rev_analysis."year"::text) AS max_year
                   FROM edw.agg_afas_rev_analysis
                  GROUP BY agg_afas_rev_analysis.client) my ON cy.client::text = my.maxclient::text AND cy."year"::text = my.max_year) cy
   JOIN edw.agg_afas_rev_analysis_goals1 gl ON cy.client::text = gl.client::text AND cy."year"::text = gl."year"::text
UNION ALL 
 SELECT cy.client, cy."year", cy.fiscal_year, cy.submitted_date, NULL::"unknown" AS ly_submitted_date, NULL::"unknown" AS max_ly_submitted_date, cy.sub_population, cy.fin_tier_name, lpad(cy.fin_tier_display_sequence::text, 2, 0::text)::character varying AS fin_tier_display_sequence, (('Level'::character varying::text || ' '::character varying::text) || "right"(cy.a_tier_display_sequence::text, 1)::character varying::text)::character varying AS a_tier_display_sequence, cy.student_status, NULL::"unknown" AS goal_student_count, cy.student_count AS cy_student_count, NULL::"unknown" AS ly_student_count, NULL::"unknown" AS max_ly_student_count, NULL::"unknown" AS goal_summary_tuition_revenue, NULL::"unknown" AS goal_summary_total_fees_revenue, NULL::"unknown" AS goal_summary_total_room_board_revenue, NULL::"unknown" AS goal_summary_total_revenue, NULL::"unknown" AS goal_summary_inst_gift_unfunded_tuition, NULL::"unknown" AS goal_summary_inst_gift_funded_gift, NULL::"unknown" AS goal_summary_tuition_exchange_waivers, NULL::"unknown" AS goal_summary_premier_academic, NULL::"unknown" AS goal_summary_room_board, NULL::"unknown" AS goal_summary_campus_based_work, NULL::"unknown" AS goal_summary_net_tuition_fee_revenue, NULL::"unknown" AS goal_summary_overall_including_fees, NULL::"unknown" AS goal_summary_total_inst_gift_aid, NULL::"unknown" AS goal_summary_unfunded_tuition_discount, NULL::"unknown" AS goal_summary_tuition_discount_rate, NULL::"unknown" AS goal_summary_overall_unfunded_discount, NULL::"unknown" AS goal_summary_unfunded_room_board_discount, NULL::"unknown" AS goal_total_tuition_revenue, NULL::"unknown" AS goal_total_fees_revenue, NULL::"unknown" AS goal_toal_room_board_revenue, NULL::"unknown" AS goal_total_overall_revenue, cy.total_tuition_revenue AS cy_total_tuition_revenue, cy.total_fees_revenue AS cy_total_fees_revenue, cy.total_room_board_revenue AS cy_total_room_board_revenue, cy.total_overall_revenue AS cy_total_overall_revenue, NULL::"unknown" AS ly_total_tuition_revenue, NULL::"unknown" AS ly_total_fees_revenue, NULL::"unknown" AS ly_total_room_board_revenue, NULL::"unknown" AS ly_total_overall_revenue, NULL::"unknown" AS max_ly_total_tuition_revenue, NULL::"unknown" AS max_ly_total_fees_revenue, NULL::"unknown" AS max_ly_total_room_board_revenue, NULL::"unknown" AS max_ly_total_overall_revenue, NULL::"unknown" AS goal_net_tuition_fee, NULL::"unknown" AS goal_net_revenue, NULL::"unknown" AS goal_net_room_board, NULL::"unknown" AS goal_net_overall_revenue, cy.net_tuition_fee_revenue AS cy_net_tuition_fee_revenue, cy.net_room_board_revenue AS cy_net_room_board_revenue, cy.net_overall_revenue AS cy_net_overall_revenue, NULL::"unknown" AS ly_net_tuition_fee_revenue, NULL::"unknown" AS ly_net_room_board_revenue, NULL::"unknown" AS ly_net_overall_revenue, NULL::"unknown" AS max_ly_net_tuition_fee_revenue, NULL::"unknown" AS max_ly_net_room_board_revenue, NULL::"unknown" AS max_ly_net_overall_revenue, NULL::"unknown" AS goal_inst_gift_unfunded_tuition, NULL::"unknown" AS goal_inst_gift_unfunded_room_board, NULL::"unknown" AS goal_inst_gift_funded_gift, cy.inst_gift_unfunded_gift_tuition AS cy_inst_gift_unfunded_gift_tuition, cy.inst_gift_unfunded_room_board AS cy_inst_gift_unfunded_room_board, cy.inst_gift_funded_inst_gift AS cy_inst_gift_funded_inst_gift, NULL::"unknown" AS ly_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS ly_inst_gift_unfunded_room_board, NULL::"unknown" AS ly_inst_gift_funded_inst_gift, NULL::"unknown" AS max_ly_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS max_ly_inst_gift_unfunded_room_board, NULL::"unknown" AS max_ly_inst_gift_funded_inst_gift, NULL::"unknown" AS goal_disrate_tuition_discount, NULL::"unknown" AS goal_disrate_unfunded_tuition_discount, NULL::"unknown" AS goal_disrate_unfunded_room_board, NULL::"unknown" AS goal_disrate_overall_discount, cy.i__g AS cy_i__g, cy.total_tuition_fees_revenue AS cy_total_tuition_fees_revenue, cy.i_ug AS cy_i_ug, cy.disrate_tuition_fees AS cy_disrate_tuition_fees, cy.disrate_tuition AS cy_disrate_tuition, cy.disrate_overall_numerator AS cy_disrate_overall_numerator, cy.disrate_overall_denominator AS cy_disrate_overall_denominator, NULL::"unknown" AS ly_i__g, NULL::"unknown" AS ly_total_tuition_fees_revenue, NULL::"unknown" AS ly_i_ug, NULL::"unknown" AS ly_disrate_tuition_fees, NULL::"unknown" AS ly_disrate_tuition, NULL::"unknown" AS ly_disrate_overall_numerator, NULL::"unknown" AS ly_disrate_overall_denominator, NULL::"unknown" AS max_ly_i__g, NULL::"unknown" AS max_ly_total_tuition_fees_revenue, NULL::"unknown" AS max_ly_i_ug, NULL::"unknown" AS max_ly_disrate_tuition_fees, NULL::"unknown" AS max_ly_disrate_tuition, NULL::"unknown" AS max_ly_disrate_overall_numerator, NULL::"unknown" AS max_ly_disrate_overall_denominator, cy.act AS cy_act, cy.academic_index AS cy_academic_index, cy.gpa AS cy_gpa, cy.sat AS cy_sat, cy.act_student_count AS cy_act_student_count, cy.academic_index_student_count AS cy_academic_index_student_count, cy.gpa_student_count AS cy_gpa_student_count, cy.sat_student_count AS cy_sat_student_count, NULL::"unknown" AS ly_act, NULL::"unknown" AS ly_academic_index, NULL::"unknown" AS ly_gpa, NULL::"unknown" AS ly_sat, NULL::"unknown" AS ly_act_student_count, NULL::"unknown" AS ly_academic_index_student_count, NULL::"unknown" AS ly_gpa_student_count, NULL::"unknown" AS ly_sat_student_count, NULL::"unknown" AS max_ly_act, NULL::"unknown" AS max_ly_academic_index, NULL::"unknown" AS max_ly_gpa, NULL::"unknown" AS max_ly_sat, NULL::"unknown" AS max_ly_act_student_count, NULL::"unknown" AS max_ly_academic_index_student_count, NULL::"unknown" AS max_ly_gpa_student_count, NULL::"unknown" AS max_ly_sat_student_count
   FROM edw.agg_afas_rev_analysis cy
   JOIN ( SELECT agg_afas_rev_analysis.client AS maxclient, "max"(agg_afas_rev_analysis."year"::text) AS max_year
           FROM edw.agg_afas_rev_analysis
          GROUP BY agg_afas_rev_analysis.client) my ON cy.client::text = my.maxclient::text AND cy."year"::text = my.max_year)
UNION ALL 
 SELECT ly.client, ly."year", ly.fiscal_year, cy.submitted_date, ly.submitted_date AS ly_submitted_date, NULL::"unknown" AS max_ly_submitted_date, ly.sub_population, cy.fin_tier_name, lpad(cy.fin_tier_display_sequence::text, 2, 0::text)::character varying AS fin_tier_display_sequence, (('Level'::character varying::text || ' '::character varying::text) || ly.a_tier_display_sequence::character varying::text)::character varying AS a_tier_display_sequence, ly.student_status, NULL::"unknown" AS goal_student_count, NULL::"unknown" AS cy_student_count, ly.student_count AS ly_student_count, NULL::"unknown" AS max_ly_student_count, NULL::"unknown" AS goal_summary_tuition_revenue, NULL::"unknown" AS goal_summary_total_fees_revenue, NULL::"unknown" AS goal_summary_total_room_board_revenue, NULL::"unknown" AS goal_summary_total_revenue, NULL::"unknown" AS goal_summary_inst_gift_unfunded_tuition, NULL::"unknown" AS goal_summary_inst_gift_funded_gift, NULL::"unknown" AS goal_summary_tuition_exchange_waivers, NULL::"unknown" AS goal_summary_premier_academic, NULL::"unknown" AS goal_summary_room_board, NULL::"unknown" AS goal_summary_campus_based_work, NULL::"unknown" AS goal_summary_net_tuition_fee_revenue, NULL::"unknown" AS goal_summary_overall_including_fees, NULL::"unknown" AS goal_summary_total_inst_gift_aid, NULL::"unknown" AS goal_summary_unfunded_tuition_discount, NULL::"unknown" AS goal_summary_tuition_discount_rate, NULL::"unknown" AS goal_summary_overall_unfunded_discount, NULL::"unknown" AS goal_summary_unfunded_room_board_discount, NULL::"unknown" AS goal_total_tuition_revenue, NULL::"unknown" AS goal_total_fees_revenue, NULL::"unknown" AS goal_toal_room_board_revenue, NULL::"unknown" AS goal_total_overall_revenue, NULL::"unknown" AS cy_total_tuition_revenue, NULL::"unknown" AS cy_total_fees_revenue, NULL::"unknown" AS cy_total_room_board_revenue, NULL::"unknown" AS cy_total_overall_revenue, ly.total_tuition_revenue AS ly_total_tuition_revenue, ly.total_fees_revenue AS ly_total_fees_revenue, ly.total_room_board_revenue AS ly_total_room_board_revenue, ly.total_overall_revenue AS ly_total_overall_revenue, NULL::"unknown" AS max_ly_total_tuition_revenue, NULL::"unknown" AS max_ly_total_fees_revenue, NULL::"unknown" AS max_ly_total_room_board_revenue, NULL::"unknown" AS max_ly_total_overall_revenue, NULL::"unknown" AS goal_net_tuition_fee, NULL::"unknown" AS goal_net_revenue, NULL::"unknown" AS goal_net_room_board, NULL::"unknown" AS goal_net_overall_revenue, NULL::"unknown" AS cy_net_tuition_fee_revenue, NULL::"unknown" AS cy_net_room_board_revenue, NULL::"unknown" AS cy_net_overall_revenue, ly.net_tuition_fee_revenue AS ly_net_tuition_fee_revenue, ly.net_room_board_revenue AS ly_net_room_board_revenue, ly.net_overall_revenue AS ly_net_overall_revenue, NULL::"unknown" AS max_ly_net_tuition_fee_revenue, NULL::"unknown" AS max_ly_net_room_board_revenue, NULL::"unknown" AS max_ly_net_overall_revenue, NULL::"unknown" AS goal_inst_gift_unfunded_tuition, NULL::"unknown" AS goal_inst_gift_unfunded_room_board, NULL::"unknown" AS goal_inst_gift_funded_gift, NULL::"unknown" AS cy_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS cy_inst_gift_unfunded_room_board, NULL::"unknown" AS cy_inst_gift_funded_inst_gift, ly.inst_gift_unfunded_gift_tuition AS ly_inst_gift_unfunded_gift_tuition, ly.inst_gift_unfunded_room_board AS ly_inst_gift_unfunded_room_board, ly.inst_gift_funded_inst_gift AS ly_inst_gift_funded_inst_gift, NULL::"unknown" AS max_ly_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS max_ly_inst_gift_unfunded_room_board, NULL::"unknown" AS max_ly_inst_gift_funded_inst_gift, NULL::"unknown" AS goal_disrate_tuition_discount, NULL::"unknown" AS goal_disrate_unfunded_tuition_discount, NULL::"unknown" AS goal_disrate_unfunded_room_board, NULL::"unknown" AS goal_disrate_overall_discount, NULL::"unknown" AS cy_i__g, NULL::"unknown" AS cy_total_tuition_fees_revenue, NULL::"unknown" AS cy_i_ug, NULL::"unknown" AS cy_disrate_tuition_fees, NULL::"unknown" AS cy_disrate_tuition, NULL::"unknown" AS cy_disrate_overall_numerator, NULL::"unknown" AS cy_disrate_overall_denominator, ly.i__g AS ly_i__g, ly.total_tuition_fees_revenue AS ly_total_tuition_fees_revenue, ly.i_ug AS ly_i_ug, ly.disrate_tuition_fees AS ly_disrate_tuition_fees, ly.disrate_tuition AS ly_disrate_tuition, ly.disrate_overall_numerator AS ly_disrate_overall_numerator, ly.disrate_overall_denominator AS ly_disrate_overall_denominator, NULL::"unknown" AS max_ly_i__g, NULL::"unknown" AS max_ly_total_tuition_fees_revenue, NULL::"unknown" AS max_ly_i_ug, NULL::"unknown" AS max_ly_disrate_tuition_fees, NULL::"unknown" AS max_ly_disrate_tuition, NULL::"unknown" AS max_ly_disrate_overall_numerator, NULL::"unknown" AS max_ly_disrate_overall_denominator, NULL::"unknown" AS cy_act, NULL::"unknown" AS cy_academic_index, NULL::"unknown" AS cy_gpa, NULL::"unknown" AS cy_sat, NULL::"unknown" AS cy_act_student_count, NULL::"unknown" AS cy_academic_index_student_count, NULL::"unknown" AS cy_gpa_student_count, NULL::"unknown" AS cy_sat_student_count, ly.act AS ly_act, ly.academic_index AS ly_academic_index, ly.gpa AS ly_gpa, ly.sat AS ly_sat, ly.act_student_count AS ly_act_student_count, ly.academic_index_student_count AS ly_academic_index_student_count, ly.gpa_student_count AS ly_gpa_student_count, ly.sat_student_count AS ly_sat_student_count, NULL::"unknown" AS max_ly_act, NULL::"unknown" AS max_ly_academic_index, NULL::"unknown" AS max_ly_gpa, NULL::"unknown" AS max_ly_sat, NULL::"unknown" AS max_ly_act_student_count, NULL::"unknown" AS max_ly_academic_index_student_count, NULL::"unknown" AS max_ly_gpa_student_count, NULL::"unknown" AS max_ly_sat_student_count
   FROM ( SELECT DISTINCT cy.client, cy."year", cy.submitted_date, cy.fin_tier_display_sequence, cy.fin_tier_name
           FROM edw.agg_afas_rev_analysis cy
      JOIN ( SELECT agg_afas_rev_analysis.client AS maxclient, "max"(agg_afas_rev_analysis."year"::text) AS max_year
                   FROM edw.agg_afas_rev_analysis
                  GROUP BY agg_afas_rev_analysis.client) my ON cy.client::text = my.maxclient::text AND cy."year"::text = my.max_year
     ORDER BY cy.client, cy.submitted_date) cy
   JOIN ( SELECT derived_table1.client, derived_table1.submitted_date, "max"(derived_table1.ly_submitted_date) AS ly_submitted_date
           FROM ( SELECT a.client, a.submitted_date, b.ly_submitted_date, abs(date_diff('Day'::character varying::text, a.sd1, b.ly_submitted_date)) AS abs, pg_catalog.rank()
                  OVER( 
                  PARTITION BY a.client, a.submitted_date
                  ORDER BY abs(date_diff('Day'::character varying::text, a.sd1, b.ly_submitted_date))) AS dt_rank
                   FROM ( SELECT DISTINCT agg_afas_rev_analysis.client, pgdate_part('y'::character varying::text, to_date("right"(agg_afas_rev_analysis."year"::text, 4), 'YYYY'::character varying::text)::timestamp without time zone) AS fiscal_year, agg_afas_rev_analysis.submitted_date, agg_afas_rev_analysis.submitted_date - 365::bigint AS sd1
                           FROM edw.agg_afas_rev_analysis) a
              LEFT JOIN ( SELECT DISTINCT agg_afas_rev_analysis.client, agg_afas_rev_analysis.submitted_date AS ly_submitted_date, pgdate_part('y'::character varying::text, to_date("right"(agg_afas_rev_analysis."year"::text, 4), 'YYYY'::character varying::text)::timestamp without time zone) AS fiscal_year
                           FROM edw.agg_afas_rev_analysis) b ON b.client::text = a.client::text AND b.fiscal_year = (a.fiscal_year - 1::double precision)) derived_table1
          WHERE derived_table1.dt_rank = 1
          GROUP BY derived_table1.client, derived_table1.submitted_date) dtu ON cy.client::text = dtu.client::text AND cy.submitted_date = dtu.submitted_date
   JOIN edw.agg_afas_rev_analysis ly ON cy.client::text = ly.client::text AND cy.fin_tier_display_sequence = ly.fin_tier_display_sequence AND dtu.ly_submitted_date = ly.submitted_date)
UNION ALL 
 SELECT ly.client, ly."year", ly.fiscal_year, cy.submitted_date, NULL::"unknown" AS ly_submitted_date, ly.submitted_date AS max_ly_submitted_date, ly.sub_population, cy.fin_tier_name, lpad(cy.fin_tier_display_sequence::text, 2, 0::text)::character varying AS fin_tier_display_sequence, (('Level'::character varying::text || ' '::character varying::text) || ly.a_tier_display_sequence::character varying::text)::character varying AS a_tier_display_sequence, ly.student_status, NULL::"unknown" AS goal_student_count, NULL::"unknown" AS cy_student_count, NULL::"unknown" AS ly_student_count, ly.student_count AS max_ly_student_count, NULL::"unknown" AS goal_summary_tuition_revenue, NULL::"unknown" AS goal_summary_total_fees_revenue, NULL::"unknown" AS goal_summary_total_room_board_revenue, NULL::"unknown" AS goal_summary_total_revenue, NULL::"unknown" AS goal_summary_inst_gift_unfunded_tuition, NULL::"unknown" AS goal_summary_inst_gift_funded_gift, NULL::"unknown" AS goal_summary_tuition_exchange_waivers, NULL::"unknown" AS goal_summary_premier_academic, NULL::"unknown" AS goal_summary_room_board, NULL::"unknown" AS goal_summary_campus_based_work, NULL::"unknown" AS goal_summary_net_tuition_fee_revenue, NULL::"unknown" AS goal_summary_overall_including_fees, NULL::"unknown" AS goal_summary_total_inst_gift_aid, NULL::"unknown" AS goal_summary_unfunded_tuition_discount, NULL::"unknown" AS goal_summary_tuition_discount_rate, NULL::"unknown" AS goal_summary_overall_unfunded_discount, NULL::"unknown" AS goal_summary_unfunded_room_board_discount, NULL::"unknown" AS goal_total_tuition_revenue, NULL::"unknown" AS goal_total_fees_revenue, NULL::"unknown" AS goal_toal_room_board_revenue, NULL::"unknown" AS goal_total_overall_revenue, NULL::"unknown" AS cy_total_tuition_revenue, NULL::"unknown" AS cy_total_fees_revenue, NULL::"unknown" AS cy_total_room_board_revenue, NULL::"unknown" AS cy_total_overall_revenue, NULL::"unknown" AS ly_total_tuition_revenue, NULL::"unknown" AS ly_total_fees_revenue, NULL::"unknown" AS ly_total_room_board_revenue, NULL::"unknown" AS ly_total_overall_revenue, ly.total_tuition_revenue AS max_ly_total_tuition_revenue, ly.total_fees_revenue AS max_ly_total_fees_revenue, ly.total_room_board_revenue AS max_ly_total_room_board_revenue, ly.total_overall_revenue AS max_ly_total_overall_revenue, NULL::"unknown" AS goal_net_tuition_fee, NULL::"unknown" AS goal_net_revenue, NULL::"unknown" AS goal_net_room_board, NULL::"unknown" AS goal_net_overall_revenue, NULL::"unknown" AS cy_net_tuition_fee_revenue, NULL::"unknown" AS cy_net_room_board_revenue, NULL::"unknown" AS cy_net_overall_revenue, NULL::"unknown" AS ly_net_tuition_fee_revenue, NULL::"unknown" AS ly_net_room_board_revenue, NULL::"unknown" AS ly_net_overall_revenue, ly.net_tuition_fee_revenue AS max_ly_net_tuition_fee_revenue, ly.net_room_board_revenue AS max_ly_net_room_board_revenue, ly.net_overall_revenue AS max_ly_net_overall_revenue, NULL::"unknown" AS goal_inst_gift_unfunded_tuition, NULL::"unknown" AS goal_inst_gift_unfunded_room_board, NULL::"unknown" AS goal_inst_gift_funded_gift, NULL::"unknown" AS cy_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS cy_inst_gift_unfunded_room_board, NULL::"unknown" AS cy_inst_gift_funded_inst_gift, NULL::"unknown" AS ly_inst_gift_unfunded_gift_tuition, NULL::"unknown" AS ly_inst_gift_unfunded_room_board, NULL::"unknown" AS ly_inst_gift_funded_inst_gift, ly.inst_gift_unfunded_gift_tuition AS max_ly_inst_gift_unfunded_gift_tuition, ly.inst_gift_unfunded_room_board AS max_ly_inst_gift_unfunded_room_board, ly.inst_gift_funded_inst_gift AS max_ly_inst_gift_funded_inst_gift, NULL::"unknown" AS goal_disrate_tuition_discount, NULL::"unknown" AS goal_disrate_unfunded_tuition_discount, NULL::"unknown" AS goal_disrate_unfunded_room_board, NULL::"unknown" AS goal_disrate_overall_discount, NULL::"unknown" AS cy_i__g, NULL::"unknown" AS cy_total_tuition_fees_revenue, NULL::"unknown" AS cy_i_ug, NULL::"unknown" AS cy_disrate_tuition_fees, NULL::"unknown" AS cy_disrate_tuition, NULL::"unknown" AS cy_disrate_overall_numerator, NULL::"unknown" AS cy_disrate_overall_denominator, NULL::"unknown" AS ly_i__g, NULL::"unknown" AS ly_total_tuition_fees_revenue, NULL::"unknown" AS ly_i_ug, NULL::"unknown" AS ly_disrate_tuition_fees, NULL::"unknown" AS ly_disrate_tuition, NULL::"unknown" AS ly_disrate_overall_numerator, NULL::"unknown" AS ly_disrate_overall_denominator, ly.i__g AS max_ly_i__g, ly.total_tuition_fees_revenue AS max_ly_total_tuition_fees_revenue, ly.i_ug AS max_ly_i_ug, ly.disrate_tuition_fees AS max_ly_disrate_tuition_fees, ly.disrate_tuition AS max_ly_disrate_tuition, ly.disrate_overall_numerator AS max_ly_disrate_overall_numerator, ly.disrate_overall_denominator AS max_ly_disrate_overall_denominator, NULL::"unknown" AS cy_act, NULL::"unknown" AS cy_academic_index, NULL::"unknown" AS cy_gpa, NULL::"unknown" AS cy_sat, NULL::"unknown" AS cy_act_student_count, NULL::"unknown" AS cy_academic_index_student_count, NULL::"unknown" AS cy_gpa_student_count, NULL::"unknown" AS cy_sat_student_count, NULL::"unknown" AS ly_act, NULL::"unknown" AS ly_academic_index, NULL::"unknown" AS ly_gpa, NULL::"unknown" AS ly_sat, NULL::"unknown" AS ly_act_student_count, NULL::"unknown" AS ly_academic_index_student_count, NULL::"unknown" AS ly_gpa_student_count, NULL::"unknown" AS ly_sat_student_count, ly.act AS max_ly_act, ly.academic_index AS max_ly_academic_index, ly.gpa AS max_ly_gpa, ly.sat AS max_ly_sat, ly.act_student_count AS max_ly_act_student_count, ly.academic_index_student_count AS max_ly_academic_index_student_count, ly.gpa_student_count AS max_ly_gpa_student_count, ly.sat_student_count AS max_ly_sat_student_count
   FROM ( SELECT DISTINCT cy.client, cy."year", cy.submitted_date, cy.fin_tier_display_sequence, cy.fin_tier_name
           FROM edw.agg_afas_rev_analysis cy
      JOIN ( SELECT agg_afas_rev_analysis.client AS maxclient, "max"(agg_afas_rev_analysis."year"::text) AS max_year
                   FROM edw.agg_afas_rev_analysis
                  GROUP BY agg_afas_rev_analysis.client) my ON cy.client::text = my.maxclient::text AND cy."year"::text = my.max_year) cy
   JOIN ( SELECT a.client, a.fiscal_year, a.submitted_date, b.fiscal_year, b.max_submitted_date
           FROM ( SELECT DISTINCT agg_afas_rev_analysis.client, pgdate_part('y'::character varying::text, to_date("right"(agg_afas_rev_analysis."year"::text, 4), 'YYYY'::character varying::text)::timestamp without time zone) AS fiscal_year, agg_afas_rev_analysis.submitted_date
                   FROM edw.agg_afas_rev_analysis) a
      LEFT JOIN ( SELECT DISTINCT agg_afas_rev_analysis.client, pgdate_part('y'::character varying::text, to_date("right"(agg_afas_rev_analysis."year"::text, 4), 'YYYY'::character varying::text)::timestamp without time zone) AS fiscal_year, "max"(agg_afas_rev_analysis.submitted_date) AS max_submitted_date
                   FROM edw.agg_afas_rev_analysis
                  GROUP BY agg_afas_rev_analysis.client, pgdate_part('y'::character varying::text, to_date("right"(agg_afas_rev_analysis."year"::text, 4), 'YYYY'::character varying::text)::timestamp without time zone)) b ON b.client::text = a.client::text AND b.fiscal_year = (a.fiscal_year - 1::double precision)
     GROUP BY a.client, a.fiscal_year, a.submitted_date, b.fiscal_year, b.max_submitted_date) dtu ON cy.client::text = dtu.client::text AND cy.submitted_date = dtu.submitted_date
   JOIN edw.agg_afas_rev_analysis ly ON dtu.client::text = ly.client::text AND cy.fin_tier_display_sequence = ly.fin_tier_display_sequence AND dtu.max_submitted_date = ly.submitted_date
   ;
commit
;

GRANT SELECT ON edw.agg_afas_rev_analysis TO reportinguser;commit;
GRANT SELECT ON edw.agg_afas_rev_analysis TO seetheridge;commit;
GRANT SELECT ON edw.agg_afas_rev_analysis_report_summary TO reportinguser; commit;
GRANT SELECT ON edw.agg_afas_rev_analysis_report_summary TO seetheridge; commit;
GRANT SELECT ON edw.agg_afas_rev_analysis_goals1 TO reportinguser; commit;
GRANT SELECT ON edw.agg_afas_rev_analysis_goals1 TO seetheridge; commit;
