DROP VIEW IF EXISTS edw.aid_distribution_view
;
commit
;
CREATE OR REPLACE VIEW edw.aid_distribution_view
(
  client,
  client_state,
  state,
  country,
  city,
  county,
  year,
  submitted_date,
  sub_population,
  i_ug,
  tuition,
  fees,
  empb,
  board,
  standing,
  room,
  flag_active,
  i_fg,
  i_rg,
  i__g,
  in_g,
  im_g,
  ip_g,
  it_g,
  io_g,
  i__w,
  i__l,
  institution_key,
  s_student_id,
  entry_stat,
  population_id,
  population_name,
  modified_date,
  name,
  date_canc,
  date_con,
  studentid,
  l_name,
  f_name,
  scity,
  gender,
  housing,
  counselor,
  date_adm,
  a_tier_name,
  a_tier_display_sequence,
  fin_tier_display_sequence,
  fin_tier_name,
  student_id,
  iscensus,
  student_status,
  measuretype,
  measurevalue
)
AS 
 SELECT i.institution_name AS client, i.state AS client_state, 
 case when i.institution_name = 'RNL University' then 'ST' else s.state end as state, s.country, 
 case when i.institution_name = 'RNL University' then 'City' else s.scity end AS city, s.scounty AS county, a.academic_year_name AS "year", dds.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.i_ug, fsd.tuition, fsd.fees, fsd.empb, fsd.board, fsd.standing, fsd.room, fsd.flag_active, fsd.i_fg, fsd.i_rg, fsd.i__g, fsd.in_g, fsd.im_g, fsd.ip_g, fsd.it_g, fsd.io_g, fsd.i__w, fsd.i__l, fsd.institution_key, 
 case when i.institution_name = 'RNL University' then s.student_id * 3 else s.student_id end AS s_student_id, s.entry_stat, p.population_id, p.population_name, "at".modified_date, ft.name, s.date_canc, s.date_con, 
 case when i.institution_name = 'RNL University' then 'ABC' + left(s.studentid,4) else s.studentid end AS studentid, 
case when i.institution_name = 'RNL University' then 'Student' else s.last_name end AS l_name,
  case when i.institution_name = 'RNL University' then 'Demo' else s.first_name end AS f_name, 
 case when i.institution_name = 'RNL University' then 'City' else s.scity end AS scity, s.gender, s.housing, s.counselor, s.date_adm, "at".name AS a_tier_name, "at".display_sequence AS a_tier_display_sequence, 
        CASE
            WHEN ft.display_sequence <= 6::double precision THEN 1::double precision
            ELSE ft.display_sequence
        END AS fin_tier_display_sequence, 
        CASE
            WHEN ft.display_sequence <= 6::double precision THEN 'Need'::character varying
            ELSE ft.name
        END AS fin_tier_name, 
		case when i.institution_name = 'RNL University' then s.student_id * 3 else s.student_id end AS student_id, dds.iscensus, fsd.student_status, fsd.measuretype, fsd.measurevalue
   FROM (((((((((((((((( SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Institutional Need-Based Gift'::character varying AS measuretype, fact_osds_student_detail.in_g AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Institutional Merit-Based Gift'::character varying AS measuretype, fact_osds_student_detail.im_g AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Institutional Premier Academic'::character varying AS measuretype, fact_osds_student_detail.ip_g AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Institutional Talent-Based Gift'::character varying AS measuretype, fact_osds_student_detail.it_g AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Institutional Other Gift'::character varying AS measuretype, fact_osds_student_detail.io_g AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Tuition Exchange'::character varying AS measuretype, fact_osds_student_detail.exchange AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Sub-Total Institutional Gift Aid'::character varying AS measuretype, fact_osds_student_detail.i__g AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'State Grant'::character varying AS measuretype, fact_osds_student_detail.state_gift AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Federal Grant (Pell) '::character varying AS measuretype, fact_osds_student_detail.fed_gift AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Federal Loan'::character varying AS measuretype, fact_osds_student_detail.fed_loan AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Private Loans'::character varying AS measuretype, fact_osds_student_detail.priv_loan AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Institutional Work Study'::character varying AS measuretype, fact_osds_student_detail.i__w AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Institutional Loan'::character varying AS measuretype, fact_osds_student_detail.i__l AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Campus-Based Gift- (Seog)'::character varying AS measuretype, fact_osds_student_detail.cb_gift AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Campus-Based Loan (Perkins)'::character varying AS measuretype, fact_osds_student_detail.cb_loan AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Merit/Premier Funds Meeting Need'::character varying AS measuretype, fact_osds_student_detail.merit_need AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_pending, fact_osds_student_detail.dataset_key, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.empb, fact_osds_student_detail.board, fact_osds_student_detail.standing, fact_osds_student_detail.room, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.i__g, fact_osds_student_detail.in_g, fact_osds_student_detail.im_g, fact_osds_student_detail.ip_g, fact_osds_student_detail.it_g, fact_osds_student_detail.io_g, fact_osds_student_detail.i__w, fact_osds_student_detail.i__l, fact_osds_student_detail.institution_key, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fact_osds_student_detail.flag_pending = 1 THEN 'pending'::character varying
                    ELSE NULL::character varying
                END AS student_status, 'Campus Based Work Study'::character varying AS measuretype, fact_osds_student_detail.cb_work AS measurevalue
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1) fsd
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN (select institution_key, institution_name, state from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name, state from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar;
commit;

GRANT SELECT ON edw.aid_distribution_view TO reportinguser;
commit;