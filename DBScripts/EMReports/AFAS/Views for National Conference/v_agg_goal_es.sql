DROP VIEW IF EXISTS edw.v_agg_goal_es
;
commit
;
CREATE OR REPLACE VIEW edw.v_agg_goal_es
(
  client,
  year,
  sub_population,
  goal,
  institution_key,
  p_institution_key,
  academic_year_key,
  fgd_institution_key,
  fgd_academic_year_key,
  financial_tier_key,
  population_key,
  academic_tier_key,
  tuitionrevenue,
  enrolledcount,
  admittedcount,
  totalrevenue,
  roomboardrevenue,
  averagenettuitionrevenue,
  fundedinstitutionalgiftaid,
  unfundedinstitutionalgiftaid,
  tuitiondiscountrate,
  overallunfundeddiscount,
  actcomposite,
  collegegpa,
  hsgpa,
  satcomposite,
  overallincludingfees,
  unfundedtuitiondiscount,
  averagenetrevenue,
  requiredfeerevenue,
  cellnumber,
  student_status,
  avgmeasuretype,
  avgmeasuredata,
  dismeasuretype,
  dismeasuredata
)
AS 
 SELECT s.client, s."year", s.sub_population, s.goal, s.institution_key, s.p_institution_key, s.academic_year_key, s.fgd_institution_key, s.fgd_academic_year_key, s.financial_tier_key, s.population_key, s.academic_tier_key, s.tuitionrevenue, s.enrolledcount, s.admittedcount, s.totalrevenue, s.roomboardrevenue, s.averagenettuitionrevenue, s.fundedinstitutionalgiftaid, s.unfundedinstitutionalgiftaid, s.tuitiondiscountrate, s.overallunfundeddiscount, s.actcomposite, s.collegegpa, s.hsgpa, s.satcomposite, "replace"(s.overallincludingfees::character varying::text, ','::character varying::text, ''::character varying::text)::double precision AS overallincludingfees, s.unfundedtuitiondiscount, s.averagenetrevenue, s.requiredfeerevenue, s.cellnumber, s.student_status, s.avgmeasuretype, s.avgmeasuredata, s.dismeasuretype, s.dismeasuredata
   FROM ( SELECT i.institution_name AS client, a.academic_year_name AS "year", p.population_name AS sub_population, fgd.goal, i.institution_key, p.institution_key AS p_institution_key, a.academic_year_key, fgd.institution_key AS fgd_institution_key, fgd.academic_year_key AS fgd_academic_year_key, fgd.financial_tier_key, fgd.population_key, fgd.academic_tier_key, fgd.tuitionrevenue, fgd.enrolledcount, fgd.admittedcount, fgd.totalrevenue, fgd.roomboardrevenue, fgd.averagenettuitionrevenue, fgd.fundedinstitutionalgiftaid, fgd.unfundedinstitutionalgiftaid, fgd.tuitiondiscountrate, fgd.overallunfundeddiscount, fgd.actcomposite, fgd.collegegpa, fgd.hsgpa, fgd.satcomposite, "replace"(fgd.overallincludingfees::character varying::text, ','::character varying::text, ''::character varying::text)::double precision AS overallincludingfees, fgd.unfundedtuitiondiscount, fgd.averagenetrevenue, fgd.requiredfeerevenue, fgd.cellnumber, fgd.student_status, fgd.avgmeasuretype, fgd.avgmeasuredata, fgd.dismeasuretype, fgd.dismeasuredata
           FROM ((( SELECT fact_osds_goal_detail.admittedcount AS goal, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees, 'Admitted'::character varying AS student_status, 'Avg_t&f'::character varying AS avgmeasuretype, sum(fact_osds_goal_detail.averagenetrevenue::double precision) AS avgmeasuredata, 'Dis_T&F'::character varying AS dismeasuretype, fact_osds_goal_detail.tuitiondiscountrate AS dismeasuredata
                   FROM edw.fact_osds_goal_detail
                  GROUP BY fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees
        UNION ALL 
                 SELECT fact_osds_goal_detail.enrolledcount AS goal, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees, 'Net Confirmed'::character varying AS student_status, 'Avg_t&f'::character varying AS avgmeasuretype, sum(fact_osds_goal_detail.averagenetrevenue::double precision) AS avgmeasuredata, 'Dis_T&F'::character varying AS dismeasuretype, fact_osds_goal_detail.tuitiondiscountrate AS dismeasuredata
                   FROM edw.fact_osds_goal_detail
                  GROUP BY fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees)
        UNION ALL 
                 SELECT fact_osds_goal_detail.admittedcount AS goal, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees, 'Admitted'::character varying AS student_status, 'Avg_OV'::character varying AS avgmeasuretype, sum(fact_osds_goal_detail.overallincludingfees::double precision) / 
                        CASE
                            WHEN sum(fact_osds_goal_detail.admittedcount) = 0 THEN 1::bigint
                            ELSE sum(fact_osds_goal_detail.admittedcount)
                        END::double precision AS avgmeasuredata, 'Dis_OV'::character varying AS dismeasuretype, fact_osds_goal_detail.overallunfundeddiscount AS dismeasuredata
                   FROM edw.fact_osds_goal_detail
                  GROUP BY fact_osds_goal_detail.tuitionrevenue + fact_osds_goal_detail.requiredfeerevenue + fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees)
        UNION ALL 
                 SELECT fact_osds_goal_detail.enrolledcount AS goal, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees, 'Net Confirmed'::character varying AS student_status, 'Avg_OV'::character varying AS avgmeasuretype, sum(fact_osds_goal_detail.overallincludingfees::double precision) / 
                        CASE
                            WHEN sum(fact_osds_goal_detail.enrolledcount) = 0 THEN 1::bigint
                            ELSE sum(fact_osds_goal_detail.enrolledcount)
                        END::double precision AS avgmeasuredata, 'Dis_OV'::character varying AS dismeasuretype, fact_osds_goal_detail.overallunfundeddiscount AS dismeasuredata
                   FROM edw.fact_osds_goal_detail
                  GROUP BY fact_osds_goal_detail.tuitionrevenue + fact_osds_goal_detail.requiredfeerevenue + fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.overallincludingfees) fgd
      JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fgd.institution_key = i.institution_key
   JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   LEFT JOIN edw.dim_osds_financial_tier ft ON fgd.financial_tier_key = ft.financial_tier_key
   LEFT JOIN edw.dim_osds_academic_tier "at" ON fgd.academic_tier_key = "at".academic_tier_key
  WHERE fgd.cellnumber < 56 AND p.flag_active::character varying::text = 1::character varying::text AND (i.institution_key IN ( SELECT DISTINCT fact_osds_goal_detail.institution_key
   FROM edw.fact_osds_goal_detail))
  GROUP BY i.institution_name, a.academic_year_name, p.population_name, fgd.goal, i.institution_key, p.institution_key, a.academic_year_key, fgd.institution_key, fgd.academic_year_key, fgd.financial_tier_key, fgd.population_key, fgd.academic_tier_key, fgd.tuitionrevenue, fgd.enrolledcount, fgd.admittedcount, fgd.totalrevenue, fgd.roomboardrevenue, fgd.averagenettuitionrevenue, fgd.fundedinstitutionalgiftaid, fgd.unfundedinstitutionalgiftaid, fgd.tuitiondiscountrate, fgd.overallunfundeddiscount, fgd.actcomposite, fgd.collegegpa, fgd.hsgpa, fgd.satcomposite, fgd.overallincludingfees, fgd.unfundedtuitiondiscount, fgd.averagenetrevenue, fgd.requiredfeerevenue, fgd.cellnumber, fgd.student_status, fgd.avgmeasuretype, fgd.avgmeasuredata, fgd.dismeasuretype, fgd.dismeasuredata) s
  GROUP BY s.client, s."year", s.sub_population, s.goal, s.institution_key, s.p_institution_key, s.academic_year_key, s.fgd_institution_key, s.fgd_academic_year_key, s.financial_tier_key, s.population_key, s.academic_tier_key, s.tuitionrevenue, s.enrolledcount, s.admittedcount, s.totalrevenue, s.roomboardrevenue, s.averagenettuitionrevenue, s.fundedinstitutionalgiftaid, s.unfundedinstitutionalgiftaid, s.tuitiondiscountrate, s.overallunfundeddiscount, s.actcomposite, s.collegegpa, s.hsgpa, s.satcomposite, "replace"(s.overallincludingfees::character varying::text, ','::character varying::text, ''::character varying::text)::double precision, s.unfundedtuitiondiscount, s.averagenetrevenue, s.requiredfeerevenue, s.cellnumber, s.student_status, s.avgmeasuretype, s.avgmeasuredata, s.dismeasuretype, s.dismeasuredata;
commit;

GRANT SELECT ON edw.v_agg_goal_es TO reportinguser;
commit;

