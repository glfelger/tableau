DROP VIEW IF EXISTS edw.agg_population_view
;
commit
;
CREATE OR REPLACE VIEW edw.agg_population_view
(
  studentid,
  l_name,
  f_name,
  scity,
  gender,
  housing,
  institution_key,
  student_detail_key,
  dataset_key,
  flag_active,
  academic_year_key,
  student_key,
  dept1_admit,
  isource_cd,
  counselor,
  market_segment,
  date_adm,
  date_con,
  date_canc,
  academictiersequence,
  financialtiersequence,
  client,
  client_state,
  state,
  country,
  city,
  scounty,
  year,
  submitted_date,
  sub_population,
  population_order,
  student_id,
  student_status,
  iscensus
)
AS 
 SELECT case when i.institution_name = 'RNL University' then 'ABC' + left(s.studentid,4) else s.studentid end AS studentid, 
case when i.institution_name = 'RNL University' then 'Student' else s.last_name end AS l_name,
  case when i.institution_name = 'RNL University' then 'Demo' else s.first_name end AS f_name,  
 case when i.institution_name = 'RNL University' then 'City' else s.scity end AS scity, 
 s.gender, s.housing, fsd.institution_key, fsd.student_detail_key, fsd.dataset_key, fsd.flag_active, fsd.academic_year_key, fsd.student_key, s.dept1_admit, s.isource_cd, s.counselor, s.market_segment, s.date_adm, s.date_con, s.date_canc, "at".display_sequence AS academictiersequence, ft.display_sequence AS financialtiersequence, i.institution_name AS client, i.state AS client_state, 
 case when i.institution_name = 'RNL University' then 'ST' else s.state end as state, s.country, 
 case when i.institution_name = 'RNL University' then 'City' else s.scity end AS city, s.scounty, a.academic_year_name AS "year", dds.transmit_date AS submitted_date, p.population_name AS sub_population, p.population_order, 
 case when i.institution_name = 'RNL University' then s.student_id * 3 else s.student_id end AS student_id, fsd.student_status, dds.iscensus
   FROM (((( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'ADMITTED'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'NET CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'GROSS CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_gross_confirmed = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'PENDING'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'CONFIRMED CANCELLED'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_confirmed_cancelled = 1) fsd
   JOIN (select institution_key, institution_name, state from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name, state from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   LEFT JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar;
commit;

GRANT SELECT ON edw.agg_population_view TO reportinguser;
commit;
