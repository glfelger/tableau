DROP VIEW IF EXISTS edw.financial_aid_matrix_goals
;
commit
;
CREATE OR REPLACE VIEW edw.financial_aid_matrix_goals
(
  client,
  category,
  year,
  sub_population,
  institution_key,
  academic_year_key,
  financial_tier_key,
  population_key,
  academic_tier_key,
  cellnumber,
  goal_student_count,
  goal_admitted_count,
  goal_net_confirmed,
  averageneed,
  percentneedmetwithgift,
  averageinstitutionalaid,
  averagenettuitionrevenue,
  a_tier_name,
  f_tier_name,
  f_tier_display_sequence,
  a_tier_display_sequence,
  student_status
)
AS 
 SELECT i.institution_name AS client, fgd.category, a.academic_year_name AS "year", p.population_name AS sub_population, fgd.institution_key, fgd.academic_year_key, fgd.financial_tier_key, fgd.population_key, fgd.academic_tier_key, fgd.cellnumber, fgd.goal_student_count, fgd.goal_admitted_count, fgd.goal_net_confirmed, fgd.averageneed, fgd.percentneedmetwithgift, fgd.institutionalgiftaid AS averageinstitutionalaid, fgd.averagenetrevenue AS averagenettuitionrevenue, "at".name AS a_tier_name, ft.name AS f_tier_name, ft.display_sequence AS f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence, fgd.student_status
   FROM (((((( SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, 0 AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'NONE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, 'NET CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail_revenue
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.admittedcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, 0 AS goal_net_confirmed, 'NONE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, 'ADMITTED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.admittedcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'NONE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, 'YIELD RATE'::character varying AS student_status
           FROM edw.fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'AVERAGE INSTITUTIONAL GIFT'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, 'NET CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'AVERAGE NET TUITION AND FEE REVENUE'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, 'NET CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'AVERAGE NEEED'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, 'NET CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail_revenue)
UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed, 'MET WITH GIFT'::character varying AS category, fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.financial_tier_key, fact_osds_goal_detail_revenue.population_key, fact_osds_goal_detail_revenue.academic_tier_key, fact_osds_goal_detail_revenue.cellnumber, fact_osds_goal_detail_revenue.averageneed, fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, fact_osds_goal_detail_revenue.averagenetrevenue, 'NET CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail_revenue) fgd
   JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fgd.institution_key = i.institution_key
   JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   LEFT JOIN edw.dim_osds_financial_tier ft ON fgd.financial_tier_key = ft.financial_tier_key
   LEFT JOIN edw.dim_osds_academic_tier "at" ON fgd.academic_tier_key = "at".academic_tier_key
  WHERE p.flag_active::character varying::text = 1::character varying::text AND fgd.cellnumber < 100;
commit;

GRANT SELECT ON edw.financial_aid_matrix_goals TO reportinguser;
commit;
