DROP VIEW IF EXISTS edw.v_agg_academiccredentials_goal
;
commit
;
CREATE OR REPLACE VIEW edw.v_agg_academiccredentials_goal
(
  client,
  year,
  sub_population,
  a_tier_name,
  hsgpa,
  collegegpa,
  predictedgpa,
  recalculatedhsgpa,
  satcomposite,
  actcomposite,
  classrank,
  academicindex,
  goal,
  ngoal,
  cellnumber,
  student_status,
  measuretype,
  measurevalue
)
AS 
 SELECT i.institution_name AS client, a.academic_year_name AS "year", p.population_name AS sub_population, "at".name AS a_tier_name, fgd.hsgpa, fgd.collegegpa, fgd.predictedgpa, fgd.recalculatedhsgpa, fgd.satcomposite, fgd.actcomposite, fgd.classrank, fgd.academicindex, fgd.admittedcount AS goal, fgd.enrolledcount AS ngoal, fgd.cellnumber, fgd.student_status, fgd.measuretype, fgd.measurevalue
   FROM ((((((( SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'ADMITTED'::character varying AS student_status, 'High School GPA'::character varying AS measuretype, fact_osds_goal_detail.hsgpa AS measurevalue
           FROM edw.fact_osds_goal_detail
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'ADMITTED'::character varying AS student_status, 'Recalculated GPA'::character varying AS measuretype, fact_osds_goal_detail.recalculatedhsgpa AS measurevalue
           FROM edw.fact_osds_goal_detail)
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'ADMITTED'::character varying AS student_status, 'College GPA'::character varying AS measuretype, fact_osds_goal_detail.collegegpa AS measurevalue
           FROM edw.fact_osds_goal_detail)
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'ADMITTED'::character varying AS student_status, 'Predicted GPA'::character varying AS measuretype, fact_osds_goal_detail.predictedgpa AS measurevalue
           FROM edw.fact_osds_goal_detail)
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'NET CONFIRMED'::character varying AS student_status, 'High School GPA'::character varying AS measuretype, fact_osds_goal_detail.hsgpa AS measurevalue
           FROM edw.fact_osds_goal_detail)
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'NET CONFIRMED'::character varying AS student_status, 'Recalculated GPA'::character varying AS measuretype, fact_osds_goal_detail.recalculatedhsgpa AS measurevalue
           FROM edw.fact_osds_goal_detail)
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'NET CONFIRMED'::character varying AS student_status, 'College GPA'::character varying AS measuretype, fact_osds_goal_detail.collegegpa AS measurevalue
           FROM edw.fact_osds_goal_detail)
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.hsgpa, fact_osds_goal_detail.collegegpa, fact_osds_goal_detail.predictedgpa, fact_osds_goal_detail.recalculatedhsgpa, fact_osds_goal_detail.satcomposite, fact_osds_goal_detail.actcomposite, fact_osds_goal_detail.classrank, fact_osds_goal_detail.academicindex, 'NET CONFIRMED'::character varying AS student_status, 'Predicted GPA'::character varying AS measuretype, fact_osds_goal_detail.predictedgpa AS measurevalue
           FROM edw.fact_osds_goal_detail) fgd
   JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fgd.institution_key = i.institution_key
   JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   LEFT JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = fgd.financial_tier_key
   LEFT JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = fgd.academic_tier_key
  WHERE fgd.cellnumber >= 0 AND fgd.cellnumber < 56 AND p.flag_active::text = 1::text;
commit;

GRANT SELECT ON edw.v_agg_academiccredentials_goal TO reportinguser;
commit;
