DROP VIEW IF EXISTS edw.financial_aid_matrix_view
;
commit
;
CREATE OR REPLACE VIEW edw.financial_aid_matrix_view
(
  client,
  academic_year,
  academicyearid,
  year,
  submitted_date,
  sub_population,
  population_order,
  institution_key,
  academictiersequence,
  financialtiersequence,
  a_tier_name,
  fin_tier_name,
  student_status,
  category,
  need,
  need_wgift,
  gift,
  i__g,
  tuition,
  fees,
  student_count,
  admitted_count,
  net_confirmed_count,
  pending_count
)
AS 
 SELECT i.institution_name AS client, a.academic_start_year AS academic_year, a.academic_year_id AS academicyearid, a.academic_year_name AS "year", dds.transmit_date AS submitted_date, p.population_name AS sub_population, p.population_order, fsd.institution_key, "at".display_sequence AS academictiersequence, ft.display_sequence AS financialtiersequence, "at".name AS a_tier_name, ft.name AS fin_tier_name, fsd.student_status, fsd.category, sum(fsd.need) AS need, sum(fsd.need_wgift) AS need_wgift, sum(fsd.gift) AS gift, sum(fsd.i__g) AS i__g, sum(fsd.tuition) AS tuition, sum(fsd.fees) AS fees, sum(fsd.student_count) AS student_count, sum(fsd.admitted_count) AS admitted_count, sum(fsd.net_confirmed_count) AS net_confirmed_count, sum(fsd.pending_count) AS pending_count
   FROM (((((( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'ADMITTED'::character varying AS student_status, 1 AS student_count, 1 AS admitted_count, 0 AS net_confirmed_count, 0 AS pending_count, 'NONE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'NONE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'AVERAGE INSTITUTIONAL GIFT'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'AVERAGE NET TUITION AND FEE REVENUE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'AVERAGE NEEED'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status, 1 AS student_count, 0 AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, fact_osds_student_detail.flag_pending AS pending_count, 'MET WITH GIFT'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'YIELD RATE'::character varying AS student_status, 1 AS student_count, fact_osds_student_detail.flag_admitted AS admitted_count, fact_osds_student_detail.flag_net_confirmed AS net_confirmed_count, 0 AS pending_count, 'NONE'::character varying AS category
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_admitted = 1) fsd
   JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar
  GROUP BY i.institution_name, a.academic_start_year, a.academic_year_id, a.academic_year_name, dds.transmit_date, p.population_name, p.population_order, fsd.institution_key, "at".display_sequence, ft.display_sequence, "at".name, ft.name, fsd.student_status, fsd.category;
commit;

GRANT SELECT ON edw.financial_aid_matrix_view TO reportinguser;
commit;