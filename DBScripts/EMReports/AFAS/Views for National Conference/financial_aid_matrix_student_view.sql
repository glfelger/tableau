DROP VIEW IF EXISTS edw.financial_aid_matrix_student_view
;
commit
;
CREATE OR REPLACE VIEW edw.financial_aid_matrix_student_view
(
  client,
  academicyearid,
  studentid,
  l_name,
  f_name,
  submitted_date,
  sub_population,
  student_status,
  city,
  state,
  academictiersequence,
  financialtiersequence,
  gender,
  housing,
  major1_admit,
  efc_9_month,
  need,
  gap,
  gift,
  i__g
)
AS 
 SELECT i.institution_name AS client, a.academic_year_id AS academicyearid, case when i.institution_name = 'RNL University' then 'ABC' + left(s.studentid,4) else s.studentid end AS studentid, 
case when i.institution_name = 'RNL University' then 'Student' else s.last_name end AS l_name,
  case when i.institution_name = 'RNL University' then 'Demo' else s.first_name end AS f_name,
 dds.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.student_status, case when i.institution_name = 'RNL University' then 'City' else s.scity end AS city, 
 case when i.institution_name = 'RNL University' then 'ST' else s.state end as state, "at".display_sequence AS academictiersequence, ft.display_sequence AS financialtiersequence, s.gender, s.housing, s.major1_admit, fsd.efc_9_month, fsd.need, fsd.gap, fsd.gift, fsd.i__g
   FROM (( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'ADMITTED'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 
                CASE
                    WHEN fact_osds_student_detail.flag_net_confirmed = 1 THEN 'NET CONFIRMED'::character varying
                    ELSE 'PENDING'::character varying
                END AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, fact_osds_student_detail.need, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.need_wgift, fact_osds_student_detail.gift, fact_osds_student_detail.i__g, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, 'YIELD RATE'::character varying AS student_status
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1 OR fact_osds_student_detail.flag_admitted = 1) fsd
   JOIN (select institution_key, institution_name, state from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name, state from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar;
commit;

GRANT SELECT ON edw.financial_aid_matrix_student_view TO reportinguser;
commit;
