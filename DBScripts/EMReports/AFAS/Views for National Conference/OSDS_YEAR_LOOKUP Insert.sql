insert into edw.dim_osds_year_lookup
select institution_key, 'RNL University' as client, academic_year_id, academic_year_key, academic_start_year, 
academic_year_name, submitted_date, ly_submitted_date, max_ly_submitted_date 
from edw.dim_osds_year_lookup 
where client = 'Mercer University'
;
commit
;