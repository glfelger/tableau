DROP VIEW IF EXISTS edw.v_agg_need_goal
;
commit
;
CREATE OR REPLACE VIEW edw.v_agg_need_goal
(
  client,
  year,
  sub_population,
  goal,
  institution_key,
  p_institution_key,
  academic_year_key,
  fgd_institution_key,
  fgd_academic_year_key,
  financial_tier_key,
  population_key,
  academic_tier_key,
  tuitionrevenue,
  totalrevenue,
  roomboardrevenue,
  averagenettuitionrevenue,
  fundedinstitutionalgiftaid,
  unfundedinstitutionalgiftaid,
  tuitiondiscountrate,
  overallunfundeddiscount,
  unfundedtuitiondiscount,
  averagenetrevenue,
  requiredfeerevenue,
  cellnumber,
  a_tier_name,
  academic_tier_id,
  at_academic_tier_key,
  modified_date,
  ft_financial_tier_key,
  financial_tier_id,
  name,
  f_tier_display_sequence,
  a_tier_display_sequence,
  student_status
)
AS 
 SELECT i.institution_name AS client, a.academic_year_name AS "year", p.population_name AS sub_population, fgd.goal, i.institution_key, p.institution_key AS p_institution_key, a.academic_year_key, fgd.institution_key AS fgd_institution_key, fgd.academic_year_key AS fgd_academic_year_key, fgd.financial_tier_key, fgd.population_key, fgd.academic_tier_key, fgd.tuitionrevenue, fgd.totalrevenue, fgd.roomboardrevenue, fgd.averagenettuitionrevenue, fgd.fundedinstitutionalgiftaid, fgd.unfundedinstitutionalgiftaid, fgd.tuitiondiscountrate, fgd.overallunfundeddiscount, fgd.unfundedtuitiondiscount, fgd.averagenetrevenue, fgd.requiredfeerevenue, fgd.cellnumber, "at".name AS a_tier_name, "at".academic_tier_id, "at".academic_tier_key AS at_academic_tier_key, "at".modified_date, ft.financial_tier_key AS ft_financial_tier_key, ft.financial_tier_id, ft.name, ft.display_sequence AS f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence, fgd.student_status
   FROM ( SELECT fact_osds_goal_detail.admittedcount AS goal, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, 'Admitted'::character varying AS student_status
           FROM edw.fact_osds_goal_detail fact_osds_goal_detail
UNION ALL 
         SELECT fact_osds_goal_detail.enrolledcount AS goal, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.tuitionrevenue, fact_osds_goal_detail.totalrevenue, fact_osds_goal_detail.roomboardrevenue, fact_osds_goal_detail.cellnumber, fact_osds_goal_detail.averagenettuitionrevenue, fact_osds_goal_detail.fundedinstitutionalgiftaid, fact_osds_goal_detail.unfundedinstitutionalgiftaid, fact_osds_goal_detail.tuitiondiscountrate, fact_osds_goal_detail.overallunfundeddiscount, fact_osds_goal_detail.unfundedtuitiondiscount, fact_osds_goal_detail.averagenetrevenue, fact_osds_goal_detail.requiredfeerevenue, 'Net Confirmed'::character varying AS student_status
           FROM edw.fact_osds_goal_detail fact_osds_goal_detail) fgd
   JOIN (select institution_key, institution_name from edw.dim_osds_institution 
          union all
          select institution_key, 'RNL University' as institution_name from edw.dim_osds_institution
          where institution_name = 'Mercer University'
    ) i ON fgd.institution_key = i.institution_key
   JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = fgd.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = fgd.academic_tier_key
  WHERE fgd.cellnumber > 0 AND fgd.cellnumber < 56 AND p.flag_active::text = 1::text;
commit;

GRANT SELECT ON edw.v_agg_need_goal TO reportinguser;
commit;
