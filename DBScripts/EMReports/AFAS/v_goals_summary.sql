DROP VIEW IF EXISTS edw.v_goals_summary ;

CREATE OR REPLACE VIEW edw.v_goals_summary
(
  client,
  academic_year,
  sub_population,
  cy_submitted_date,
  admittedcount,
  enrolledcount,
  tuitionrevenue,
  netrevenue,
  requiredfeerevenue,
  roomboardrevenue,
  totalrevenue,
  unfundedinstitutionalgiftaid,
  fundedinstitutionalgiftaid,
  satcomposite,
  actcomposite,
  collegegpa,
  hsgpa,
  predictedgpa,
  recalculatedhsgpa,
  classrank,
  academicindex,
  tuitionexchangewaivers,
  premieracademic,
  roomboard,
  campusbasedwork,
  campusbasedgift,
  campusbasedloan,
  nettuitionfeerevenue,
  overallincludingfees,
  ins_need,
  ins_merit,
  talent_gift,
  other_aid,
  work_study,
  inst_loan,
  gs_state_grant,
  gs_federal_grant,
  gs_federal_loan,
  gs_ins_loan,
  gs_campus_gift,
  gs_campus_loan,
  total_aid,
  employeedependentwaivers,
  otherchargesrevenue,
  directroomcharges,
  directrequiredfees,
  directtuition,
  directboardcharges,
  directothercharges,
  privategift,
  privatework,
  privateloan,
  numberinresidencehall,
  unfundedroomboarddiscount,
  tuitionwithoutunfundedgiftaid
)
AS 
 SELECT i.institution_name AS client, a.academic_start_year AS academic_year, p.population_name AS sub_population, b.submitted_date AS cy_submitted_date, fgd.admittedcount, fgd.enrolledcount, fgd.tuitionrevenue, fgd.netrevenue, fgd.requiredfeerevenue, fgd.roomboardrevenue, fgd.totalrevenue, fgd.unfundedinstitutionalgiftaid, fgd.fundedinstitutionalgiftaid, fgd.satcomposite, fgd.actcomposite, fgd.collegegpa, fgd.hsgpa, fgd.predictedgpa, fgd.recalculatedhsgpa, fgd.classrank, fgd.academicindex, fgd.tuitionexchangewaivers, fgd.premieracademic, fgd.roomboard, fgd.campusbasedwork, fgd.campusbasedgift, fgd.campusbasedloan, fgd.nettuitionfeerevenue, fgd.overallincludingfees, fgd.ins_need, fgd.ins_merit, fgd.talent_gift, fgd.other_aid, fgd.work_study, fgd.inst_loan, fgd.gs_state_grant, fgd.gs_federal_grant, fgd.gs_federal_loan, fgd.gs_ins_loan, fgd.gs_campus_gift, fgd.gs_campus_loan, fgd.total_aid, fgd.employeedependentwaivers, fgd.otherchargesrevenue, fgd.directroomcharges, fgd.directrequiredfees, fgd.directtuition, fgd.directboardcharges, fgd.directothercharges, fgd.privategift, fgd.privatework, fgd.privateloan, fgd.numberinresidencehall, fgd.unfundedroomboarddiscount, fgd.tuitionwithoutunfundedgiftaid
   FROM ( SELECT DISTINCT fgd.institution_key, fgd.academic_year_key, fgd.population_key, 0 AS admittedcount, 0 AS enrolledcount, fgd.tuitionrevenue, fgd.netrevenue, fgd.requiredfeerevenue, fgd.roomboardrevenue, fgd.totalrevenue, fgd.unfundedinstitutionalgiftaid, fgd.fundedinstitutionalgiftaid, fgd.satcomposite, fgd.actcomposite, fgd.collegegpa, fgd.hsgpa, fgd.predictedgpa, fgd.recalculatedhsgpa, fgd.classrank, fgd.academicindex, fgd.tuitionexchangewaivers, fgd.premieracademic, fgd.roomboard, fgd.campusbasedwork, fgd.campusbasedgift, fgd.campusbasedloan, fgd.nettuitionfeerevenue, fgd.overallincludingfees, fgd.ins_need, fgd.ins_merit, fgd.talent_gift, fgd.other_aid, fgd.institutionalworkstudy AS work_study, fgd.institutionalloan AS inst_loan, fgd.gs_state_grant, fgd.gs_federal_grant, fgd.gs_federal_loan, fgd.gs_ins_loan, fgd.gs_campus_gift, fgd.gs_campus_loan, fgd.total_aid, fgd.employeedependentwaivers, fgd.otherchargesrevenue, fgd.directroomcharges, fgd.directrequiredfees, fgd.directtuition, fgd.directboardcharges, fgd.directothercharges, fgd.privategift, fgd.privatework, fgd.privateloan, fgd.numberinresidencehall, fgd.unfundedroomboarddiscount, fgd.tuitionwithoutunfundedgiftaid
           FROM fact_osds_goal_detail_revenue fgd
          WHERE fgd.source_type::text = 'GoalSummary'::character varying::text
UNION ALL 
         SELECT fgd.institution_key, fgd.academic_year_key, fgd.population_key, sum(fgd.admittedcount) AS admittedcount, sum(fgd.enrolledcount) AS enrolledcount, 0 AS tuitionrevenue, 0 AS netrevenue, 0 AS requiredfeerevenue, 0 AS roomboardrevenue, 0 AS totalrevenue, 0 AS unfundedinstitutionalgiftaid, 0 AS fundedinstitutionalgiftaid, 0 AS satcomposite, 0 AS actcomposite, 0 AS collegegpa, 0 AS hsgpa, 0 AS predictedgpa, 0 AS recalculatedhsgpa, 0 AS classrank, 0 AS academicindex, 0 AS tuitionexchangewaivers, 0 AS premieracademic, 0 AS roomboard, 0 AS campusbasedwork, 0 AS campusbasedgift, 0 AS campusbasedloan, 0 AS nettuitionfeerevenue, 0 AS overallincludingfees, 0 AS ins_need, 0 AS ins_merit, 0 AS talent_gift, 0 AS other_aid, 0 AS work_study, 0 AS inst_loan, 0 AS gs_state_grant, 0 AS gs_federal_grant, 0 AS gs_federal_loan, 0 AS gs_ins_loan, 0 AS gs_campus_gift, 0 AS gs_campus_loan, 0 AS total_aid, 0 AS employeedependentwaivers, 0 AS otherchargesrevenue, 0 AS directroomcharges, 0 AS directrequiredfees, 0 AS directtuition, 0 AS directboardcharges, 0 AS directothercharges, 0 AS privategift, 0 AS privatework, 0 AS privateloan, 0 AS numberinresidencehall, 0 AS unfundedroomboarddiscount, 0 AS tuitionwithoutunfundedgiftaid
           FROM fact_osds_goal_detail_revenue fgd
          WHERE fgd.source_type::text = 'GoalDetail'::character varying::text AND fgd.cellnumber = 9999
          GROUP BY fgd.institution_key, fgd.academic_year_key, fgd.population_key) fgd
   JOIN dim_osds_institution i ON fgd.institution_key = i.institution_key
   LEFT JOIN dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN dim_osds_population p ON fgd.population_key = p.population_key
   JOIN dim_osds_year_lookup b ON fgd.institution_key = b.institution_key
  WHERE p.flag_active::character varying::text = 1::character varying::text AND a.flag_active::character varying::text = 1::character varying::text;

GRANT SELECT ON edw.v_goals_summary TO reportinguser;
GRANT SELECT ON edw.v_goals_summary TO group biuser;
