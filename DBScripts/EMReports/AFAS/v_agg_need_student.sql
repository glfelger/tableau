DROP VIEW IF EXISTS edw.v_agg_need_student
commit;
CREATE OR REPLACE VIEW edw.v_agg_need_student
(
  client,
  client_state,
  state,
  country,
  city,
  scounty,
  year,
  submitted_date,
  sub_population,
  student_status,
  l_name,
  f_name,
  housing,
  counselor,
  efc_9_month,
  gap,
  budget,
  gift,
  state_gift,
  fed_gift,
  cb_gift,
  need,
  flag_gross_confirmed,
  flag_net_confirmed,
  flag_confirmed_cancelled,
  flag_pending,
  i__g,
  i_ug,
  tuition,
  fees,
  board,
  room,
  student_detail_key,
  flag_active,
  avg_net_revenue,
  avg_net_room_board_revenue,
  avg_net_tuition_fee_revenue,
  fees_revenue,
  flag_admitted,
  funded_gift_tuition,
  gift_room_board,
  i_fg,
  i_rg,
  net_overall_revenue,
  net_room_board_revenue,
  net_tuition_fee_revenue,
  room_board_revenue,
  tuition_revenue,
  unfunded_gift_tuition,
  population_id,
  population_name,
  population_key,
  consolidated,
  institution_key,
  econometricsortorder,
  date_canc,
  date_con,
  last_modified_job_id,
  student_key,
  student_id,
  studentid,
  entry_stat,
  hsgpa,
  sat_comp,
  act_comp,
  dataset_key,
  a_tier_name,
  academic_tier_id,
  academic_tier_key,
  modified_date,
  financial_tier_key,
  financial_tier_id,
  name,
  f_tier_display_sequence,
  a_tier_display_sequence,
  iscensus
)
AS 
 SELECT i.institution_name AS client, i.state AS client_state, s.state, s.country, s.scity AS city, s.scounty, a.academic_year_name AS "year", date(dds.transmit_date) AS submitted_date, p.population_name AS sub_population, fsd.student_status, s.last_name AS l_name, s.first_name AS f_name, s.housing, s.counselor, fsd.efc_9_month, fsd.gap, fsd.budget, fsd.gift, fsd.state_gift, fsd.fed_gift, fsd.cb_gift, fsd.need, fsd.flag_gross_confirmed, fsd.flag_net_confirmed, fsd.flag_confirmed_cancelled, fsd.flag_pending, fsd.i__g, fsd.i_ug, fsd.tuition, fsd.fees, fsd.board, fsd.room, fsd.student_detail_key, fsd.flag_active, fsd.avg_net_revenue, fsd.avg_net_room_board_revenue, fsd.avg_net_tuition_fee_revenue, fsd.fees_revenue, fsd.flag_admitted, fsd.funded_gift_tuition, fsd.gift_room_board, fsd.i_fg, fsd.i_rg, fsd.net_overall_revenue, fsd.net_room_board_revenue, fsd.net_tuition_fee_revenue, fsd.room_board_revenue, fsd.tuition_revenue, fsd.unfunded_gift_tuition, p.population_id, p.population_name, p.population_key, p.consolidated, p.institution_key, p.econometricsortorder, s.date_canc, s.date_con, s.last_modified_job_id, s.student_key, s.student_id, s.studentid, s.entry_stat, fsd.hsgpa, fsd.sat_comp, fsd.act_comp, dds.dataset_key, "at".name AS a_tier_name, "at".academic_tier_id, "at".academic_tier_key, "at".modified_date, ft.financial_tier_key, ft.financial_tier_id, ft.name, ft.display_sequence AS f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence, dds.iscensus
   FROM (((( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Admitted'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.budget, fact_osds_student_detail.gift, fact_osds_student_detail.state_gift, fact_osds_student_detail.fed_gift, fact_osds_student_detail.cb_gift, fact_osds_student_detail.need, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp
           FROM fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Gross Confirmed'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.budget, fact_osds_student_detail.gift, fact_osds_student_detail.state_gift, fact_osds_student_detail.fed_gift, fact_osds_student_detail.cb_gift, fact_osds_student_detail.need, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp
           FROM fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_con = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Net Confirmed'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.budget, fact_osds_student_detail.gift, fact_osds_student_detail.state_gift, fact_osds_student_detail.fed_gift, fact_osds_student_detail.cb_gift, fact_osds_student_detail.need, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp
           FROM fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_con = 1 AND fact_osds_student_detail.flag_canc <> 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Confirmed Cancelled'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.budget, fact_osds_student_detail.gift, fact_osds_student_detail.state_gift, fact_osds_student_detail.fed_gift, fact_osds_student_detail.cb_gift, fact_osds_student_detail.need, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp
           FROM fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_con = 1 AND fact_osds_student_detail.flag_canc = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Pending'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.efc_9_month, fact_osds_student_detail.gap, fact_osds_student_detail.budget, fact_osds_student_detail.gift, fact_osds_student_detail.state_gift, fact_osds_student_detail.fed_gift, fact_osds_student_detail.cb_gift, fact_osds_student_detail.need, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp
           FROM fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1 AND fact_osds_student_detail.flag_con <> 1 AND fact_osds_student_detail.flag_canc <> 1) fsd
   JOIN dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN dim_osds_student s ON fsd.student_key = s.student_key
   JOIN dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar;
commit;

GRANT SELECT ON edw.v_agg_need_student TO reportinguser;
commit;
