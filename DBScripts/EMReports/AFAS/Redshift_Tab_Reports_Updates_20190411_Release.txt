insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Internal Auditing', 'SmartView Usage Analysis', 'https://reporting.ruffalonl.com/#/views/SmartView_Activity_Report/UsageSummary', 'Y', 'Dashboard', 'Internal', 'Usage Reports')
;
commit
;
insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Projections', 'https://reporting.ruffalonl.com/#/views/Projections/Projections', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;
