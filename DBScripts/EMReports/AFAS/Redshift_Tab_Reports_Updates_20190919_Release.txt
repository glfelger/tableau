insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Velocity', 'https://reporting.ruffalonl.com/#/views/Velocity/StudentCounts', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;
