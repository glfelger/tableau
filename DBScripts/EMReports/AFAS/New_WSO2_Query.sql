select distinct client_name, email, reportname, report_type, report_group, module, generic_url, vision_project_type, report_order 
from 
(
SELECT DISTINCT INS.NAME  AS client_name,  
PER.email as email,
per.isemployee, 
prjtype.projecttype_desc 
FROM EDW.NLAUTH_PERSONS PER
join EDW.nlauth_institution ins
  on per.institutionid = ins.institutionid
LEFT JOIN EDW.NLAUTH_PROJECTPERSON PRJPER
  on per.personid = prjper.personid
LEFT JOIN EDW.NLAUTH_PROJECT PRJ
  on prjper.projectid = prj.projectid
LEFT JOIN EDW.NLAUTH_REFPROJECTTYPE PRJTYPE
  on PRJ.ProjectType_Code = PRJTYPE.ProjectType_Code
where LOWER(PER.email) = Lower(:userName)
 and PER.ISACTIVE = 1
 ) NLAUTH 
JOIN edw.Tableau_Reports tr ON 1=1
WHERE (
        nlauth.projecttype_desc = tr.vision_project_type
        or nlauth.isemployee = 1
     )
AND tr.is_active = 'Y'
ORDER  BY report_group, 
          report_order, 
          reportname