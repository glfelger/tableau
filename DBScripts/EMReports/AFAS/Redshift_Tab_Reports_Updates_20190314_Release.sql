
update edw.tableau_reports set vision_project_type = 'FM-Phonathon Reports' where report_group = 'Annual Giving' and vision_project_type = 'FM';commit;end;

insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Week to Week', 'https://reporting.ruffalonl.com/#/views/WeektoWeek/WeektoWeek', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;
insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Goals Summary', 'https://reporting.ruffalonl.com/#/views/GoalsSummary/GoalsSummary', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;