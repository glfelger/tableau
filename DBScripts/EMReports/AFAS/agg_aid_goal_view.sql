DROP VIEW IF EXISTS edw.agg_aid_goal_view
;
COMMIT 
;
CREATE OR REPLACE VIEW edw.agg_aid_goal_view
AS 
 SELECT i.institution_name AS client, a.academic_year_name AS "year", p.population_name AS sub_population, i.institution_key, a.academic_year_key, fgd.population_key, fgd.student_status, fgd.measurevalue, fgd.measuretype
   FROM (((((((((((((((( SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.ins_need AS measurevalue, 'Institutional Need-Based Gift'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary'
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.ins_merit AS measurevalue, 'Institutional Merit-Based Gift'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.premieracademic AS measurevalue, 'Institutional Premier Academic'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct  fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.talent_gift AS measurevalue, 'Institutional Talent-Based Gift'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct  fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.other_aid AS measurevalue, 'Institutional Other Gift'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct  fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.tuitionexchangewaivers AS measurevalue, 'Tuition Exchange'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct  fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.total_aid AS measurevalue, 'Sub-Total Institutional Gift Aid'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct  fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.gs_state_grant AS measurevalue, 'State Grant'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.gs_federal_grant AS measurevalue, 'Federal Grant (Pell)'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct  fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.gs_federal_loan AS measurevalue, 'Federal Loan'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.privateloan AS measurevalue, 'Private Loans'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct  fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.institutionalworkstudy AS measurevalue, 'Institutional Work Study'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.gs_ins_loan AS measurevalue, 'Institutional Loan'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.gs_campus_gift AS measurevalue, 'Campus-Based Gift- (Seog)'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.gs_campus_loan AS measurevalue, 'Campus-Based Loan (Perkins)'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.percentneedmetwithgift AS measurevalue, 'Merit/Premier Funds Meeting Need'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary')
UNION ALL 
         SELECT distinct fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, fact_osds_goal_detail_revenue.population_key, 'net confirmed'::character varying AS student_status, fact_osds_goal_detail_revenue.campusbasedwork AS measurevalue, 'Campus Based Work Study'::character varying AS measuretype
           FROM edw.fact_osds_goal_detail_revenue 
           where source_type = 'GoalSummary'
           ) fgd
   JOIN edw.dim_osds_institution i ON fgd.institution_key = i.institution_key
   JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   where p.flag_active = 1
;
COMMIT
;

GRANT SELECT ON edw.agg_aid_goal_view TO reportinguser;commit;
GRANT SELECT ON edw.agg_aid_goal_view TO seetheridge;commit;

