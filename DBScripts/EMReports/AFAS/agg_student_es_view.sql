DROP VIEW IF EXISTS edw.agg_student_es_view
;
commit
;
CREATE OR REPLACE VIEW edw.agg_student_es_view
(
  client,
  client_state,
  state,
  country,
  city,
  scounty,
  year,
  submitted_date,
  sub_population,
  student_status,
  flag_gross_confirmed,
  flag_net_confirmed,
  flag_confirmed_cancelled,
  flag_pending,
  i__g,
  i_ug,
  tuition,
  fees,
  board,
  room,
  student_detail_key,
  flag_active,
  avg_net_revenue,
  avg_net_room_board_revenue,
  avg_net_tuition_fee_revenue,
  fees_revenue,
  flag_admitted,
  funded_gift_tuition,
  gift_room_board,
  i_fg,
  i_rg,
  empb, 
  net_overall_revenue,
  net_room_board_revenue,
  net_tuition_fee_revenue,
  room_board_revenue,
  tuition_revenue,
  unfunded_gift_tuition,
  population_id,
  population_name,
  population_key,
  consolidated,
  institution_key,
  econometricsortorder,
  date_canc,
  date_con,
  last_modified_job_id,
  student_key,
  student_id,
  studentid,
  entry_stat,
  l_name,
  f_name,
  scity,
  housing,
  dept1_admit,
  gender,
  isource_cd,
  counselor,
  market_segment,
  date_adm,
  efc_9_month,
  f_tier_display_sequence,
  a_tier_display_sequence,
  hsgpa,
  sat_comp,
  act_comp,
  dataset_key,
  avgmeasuretype,
  avgmeasuredata,
  dismeasuretype,
  dismeasuredata,
  iscensus
)
AS 
 SELECT i.institution_name AS client, i.state AS client_state, s.state, s.country, s.scity AS city, s.scounty, a.academic_year_name AS "year", date(dds.transmit_date) AS submitted_date, p.population_name AS sub_population, fsd.student_status, fsd.flag_gross_confirmed, fsd.flag_net_confirmed, fsd.flag_confirmed_cancelled, fsd.flag_pending, fsd.i__g, fsd.i_ug, fsd.tuition, fsd.fees, fsd.board, fsd.room, fsd.student_detail_key, fsd.flag_active, fsd.avg_net_revenue, fsd.avg_net_room_board_revenue, fsd.avg_net_tuition_fee_revenue, fsd.fees_revenue, fsd.flag_admitted, fsd.funded_gift_tuition, fsd.gift_room_board, fsd.i_fg, fsd.i_rg, fsd.empb, fsd.net_overall_revenue, fsd.net_room_board_revenue, fsd.net_tuition_fee_revenue, fsd.room_board_revenue, fsd.tuition_revenue, fsd.unfunded_gift_tuition, p.population_id, p.population_name, p.population_key, p.consolidated, p.institution_key, p.econometricsortorder, s.date_canc, s.date_con, s.last_modified_job_id, s.student_key, s.student_id, s.studentid, s.entry_stat, s.last_name AS l_name, s.first_name AS f_name, s.scity, s.housing, s.dept1_admit, s.gender, s.isource_cd, s.counselor, s.market_segment, s.date_adm, fsd.efc_9_month, ft.display_sequence AS f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence, fsd.hsgpa, fsd.sat_comp, fsd.act_comp, dds.dataset_key, fsd.avgmeasuretype, fsd.avgmeasuredata, fsd.dismeasuretype, fsd.dismeasuredata, dds.iscensus
   FROM ((((((((( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Admitted'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_T&F'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees - fact_osds_student_detail.i__g AS avgmeasuredata, 'Dis_T&F'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i__g / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Gross Confirmed'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_T&F'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees - fact_osds_student_detail.i__g AS avgmeasuredata, 'Dis_T&F'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i__g / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_gross_confirmed = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Net Confirmed'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_T&F'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees - fact_osds_student_detail.i__g AS avgmeasuredata, 'Dis_T&F'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i__g / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Confirmed Cancelled'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_T&F'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees - fact_osds_student_detail.i__g AS avgmeasuredata, 'Dis_T&F'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i__g / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_confirmed_cancelled = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Pending'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_T&F'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees - fact_osds_student_detail.i__g AS avgmeasuredata, 'Dis_T&F'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i__g / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_pending = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Admitted'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_OV'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees + fact_osds_student_detail.room + fact_osds_student_detail.board - fact_osds_student_detail.i_ug AS avgmeasuredata, 'Dis_OV'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i_ug / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue + fact_osds_student_detail.room_board_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Gross Confirmed'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_OV'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees + fact_osds_student_detail.room + fact_osds_student_detail.board - fact_osds_student_detail.i_ug AS avgmeasuredata, 'Dis_OV'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i_ug / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue + fact_osds_student_detail.room_board_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_gross_confirmed = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Net Confirmed'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_OV'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees + fact_osds_student_detail.room + fact_osds_student_detail.board - fact_osds_student_detail.i_ug AS avgmeasuredata, 'Dis_OV'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i_ug / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue + fact_osds_student_detail.room_board_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_net_confirmed = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Confirmed Cancelled'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_OV'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees + fact_osds_student_detail.room + fact_osds_student_detail.board - fact_osds_student_detail.i_ug AS avgmeasuredata, 'Dis_OV'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i_ug / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue + fact_osds_student_detail.room_board_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_confirmed_cancelled = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Pending'::character varying AS student_status, fact_osds_student_detail.flag_gross_confirmed, fact_osds_student_detail.flag_net_confirmed, fact_osds_student_detail.flag_confirmed_cancelled, fact_osds_student_detail.flag_pending, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.avg_net_revenue, fact_osds_student_detail.avg_net_room_board_revenue, fact_osds_student_detail.avg_net_tuition_fee_revenue, fact_osds_student_detail.fees_revenue, fact_osds_student_detail.flag_admitted, fact_osds_student_detail.funded_gift_tuition, fact_osds_student_detail.gift_room_board, fact_osds_student_detail.i_fg, fact_osds_student_detail.i_rg, fact_osds_student_detail.empb, fact_osds_student_detail.net_overall_revenue, fact_osds_student_detail.net_room_board_revenue, fact_osds_student_detail.net_tuition_fee_revenue, fact_osds_student_detail.room_board_revenue, fact_osds_student_detail.tuition_revenue, fact_osds_student_detail.unfunded_gift_tuition, fact_osds_student_detail.hsgpa, fact_osds_student_detail.sat_comp, fact_osds_student_detail.act_comp, 'Avg_OV'::character varying AS avgmeasuretype, fact_osds_student_detail.tuition + fact_osds_student_detail.fees + fact_osds_student_detail.room + fact_osds_student_detail.board - fact_osds_student_detail.i_ug AS avgmeasuredata, 'Dis_OV'::character varying AS dismeasuretype, 
                CASE
                    WHEN (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue) > 0 THEN fact_osds_student_detail.i_ug / (fact_osds_student_detail.tuition_revenue + fact_osds_student_detail.fees_revenue + fact_osds_student_detail.room_board_revenue)
                    ELSE NULL::integer
                END AS dismeasuredata, fact_osds_student_detail.efc_9_month
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_pending = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar 
  GROUP BY i.institution_name, i.state, s.state, s.country, s.scity, s.scounty, a.academic_year_name, dds.transmit_date, p.population_name, fsd.student_status, fsd.flag_gross_confirmed, fsd.flag_net_confirmed, fsd.flag_confirmed_cancelled, fsd.flag_pending, fsd.i__g, fsd.i_ug, fsd.tuition, fsd.fees, fsd.board, fsd.room, fsd.student_detail_key, fsd.flag_active, fsd.avg_net_revenue, fsd.avg_net_room_board_revenue, fsd.avg_net_tuition_fee_revenue, fsd.fees_revenue, fsd.flag_admitted, fsd.funded_gift_tuition, fsd.gift_room_board, fsd.i_fg, fsd.i_rg, fsd.empb, fsd.net_overall_revenue, fsd.net_room_board_revenue, fsd.net_tuition_fee_revenue, fsd.room_board_revenue, fsd.tuition_revenue, fsd.unfunded_gift_tuition, p.population_id, p.population_key, p.consolidated, p.institution_key, p.econometricsortorder, s.date_canc, s.date_con, s.last_modified_job_id, s.student_key, s.student_id, s.studentid, s.entry_stat, s.last_name, s.first_name, s.housing, s.dept1_admit, s.gender, s.isource_cd, s.counselor, s.market_segment, s.date_adm, fsd.efc_9_month, ft.display_sequence, "at".display_sequence, fsd.hsgpa, fsd.sat_comp, fsd.act_comp, dds.dataset_key, fsd.avgmeasuretype, fsd.avgmeasuredata, fsd.dismeasuretype, fsd.dismeasuredata, dds.iscensus
  ORDER BY p.econometricsortorder, p.population_name, dds.transmit_date;

commit;

GRANT SELECT ON edw.agg_student_es_view TO reportinguser;commit;
GRANT SELECT ON edw.agg_student_es_view TO seetheridge;commit;
