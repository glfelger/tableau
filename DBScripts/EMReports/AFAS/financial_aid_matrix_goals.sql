DROP VIEW IF EXISTS edw.financial_aid_matrix_goals
;
commit
;
CREATE OR REPLACE VIEW edw.financial_aid_matrix_goals
(
  client,
  category,
  year,
  sub_population,
  institution_key,
  academic_year_key,
  population_key,
  cellnumber,
  goal_student_count,
  goal_admitted_count,
  goal_net_confirmed,
  total_need,
  averageneed,
  percentneedmetwithgift,
  averageinstitutionalaid,
  averagenettuitionrevenue,
  avg_net_tuit_rev,
  nettuitionfeerevenue,
  netrevenue,
  total_aid,
  avg_inst_aid,
  total_gifts,
  f_tier_display_sequence,
  a_tier_display_sequence,
  student_status
)
AS 
 SELECT i.institution_name AS client, categories.category, a.academic_year_name AS "year", p.population_name AS sub_population, 
 fgd.institution_key, fgd.academic_year_key, fgd.population_key, fgd.cellnumber,
  fgd.goal_student_count, fgd.goal_admitted_count, fgd.goal_net_confirmed, fgd.goal_student_count * fgd.averageneed AS total_need,
   fgd.averageneed, fgd.percentneedmetwithgift, fgd.institutionalgiftaid AS averageinstitutionalaid, 
   fgd.averagenetrevenue AS averagenettuitionrevenue, fgd.averagenettuitionrevenue AS avg_net_tuit_rev, fgd.nettuitionfeerevenue,
    fgd.netrevenue, fgd.total_aid, fgd.averageinstitutionalaid AS avg_inst_aid, fgd.total_gifts * fgd.goal_student_count AS total_gifts,
     fgd.f_tier_display_sequence, fgd.a_tier_display_sequence, fgd.student_status
     
from (
		select 'AVERAGE INSTITUTIONAL GIFT' as category
    union all 
    select 'AVERAGE NET TUITION AND FEE REVENUE' as category
    union all
    select 'AVERAGE NEEED' as category
    union all
    select 'MET WITH GIFT' as category
    ) categories
    
join (
   
   

   SELECT fact_osds_goal_detail_revenue.admittedcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, 
   fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed,  
   fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, 
   fact_osds_goal_detail_revenue.population_key, 
 
   fact_osds_goal_detail_revenue.cellnumber, 
   case 
        when cellnumber in (1,2,3,4,5,2001) then 1
        when cellnumber in (6,7,8,9,10,2002) then 2
        when cellnumber in (11,12,13,14,15,2003) then 3
        when cellnumber in (16,17,18,19,20,2004) then 4
        when cellnumber in (21,22,23,24,25,2005) then 5
        when cellnumber in (26,27,28,29,30,2006) then 6
        when cellnumber in (31,32,33,34,35,2007) then 7
        when cellnumber in (311,321,331,341,351,2071) then 71 
        when cellnumber in (312,322,332,342,352,2072) then 72
        when cellnumber in (313,323,333,343,353,2073) then 73
        when cellnumber in (36,37,38,39,40,2008) then 8
        when cellnumber in (361,371,381,391,401,2081) then 81
        when cellnumber in (362,372,382,392,402,2082) then 82
        when cellnumber in (363,373,383,393,403,2083) then 83
        when cellnumber in (41,42,43,44,45,2009) then 9
        when cellnumber in (46,47,48,49,50,2010) then 10
        when cellnumber in (51,52,53,54,55,2011) then 11
        when cellnumber in (1001,1002,1003,1004,1005,9999) then 200
   end as f_tier_display_sequence, 
   
  case 
        when cellnumber in (1,6,11,16,21,26,31,311,312,313,36,361,362,363,41,46,51,1001) then 1
        when cellnumber in (2,7,12,17,22,27,32,321,322,323,37,371,372,373,42,47,52,1002) then 2
        when cellnumber in (3,8,13,18,23,28,33,331,332,333,38,381,382,383,43,48,53,1003) then 3
        when cellnumber in (4,9,14,19,24,29,34,341,342,343,39,391,392,393,44,49,54,1004) then 4
        when cellnumber in (5,10,15,20,25,30,35,351,352,353,40,401,402,403,45,50,55,1005) then 5
        when cellnumber in (2001,2002,2003,2004,2005,2006,2007,2071,2072,2073,2008,2081,2082,2083,2009,2010,2011,9999) then 100

   end as a_tier_display_sequence, 

   fact_osds_goal_detail_revenue.averageneed, 
   fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, 
   fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, 
   fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, 
   fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, 
   fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 
   'ADMITTED'::character varying AS student_status
           FROM fact_osds_goal_detail_revenue

UNION ALL 
         SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, 
   fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed,  
   fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, 
   fact_osds_goal_detail_revenue.population_key, 
 
   fact_osds_goal_detail_revenue.cellnumber, 
   case 
        when cellnumber in (1,2,3,4,5,2001) then 1
        when cellnumber in (6,7,8,9,10,2002) then 2
        when cellnumber in (11,12,13,14,15,2003) then 3
        when cellnumber in (16,17,18,19,20,2004) then 4
        when cellnumber in (21,22,23,24,25,2005) then 5
        when cellnumber in (26,27,28,29,30,2006) then 6
        when cellnumber in (31,32,33,34,35,2007) then 7
        when cellnumber in (311,321,331,341,351,2071) then 71 
        when cellnumber in (312,322,332,342,352,2072) then 72
        when cellnumber in (313,323,333,343,353,2073) then 73
        when cellnumber in (36,37,38,39,40,2008) then 8
        when cellnumber in (361,371,381,391,401,2081) then 81
        when cellnumber in (362,372,382,392,402,2082) then 82
        when cellnumber in (363,373,383,393,403,2083) then 83
        when cellnumber in (41,42,43,44,45,2009) then 9
        when cellnumber in (46,47,48,49,50,2010) then 10
        when cellnumber in (51,52,53,54,55,2011) then 11
        when cellnumber in (1001,1002,1003,1004,1005,9999) then 200
   end as f_tier_display_sequence, 
   
  case 
        when cellnumber in (1,6,11,16,21,26,31,311,312,313,36,361,362,363,41,46,51,1001) then 1
        when cellnumber in (2,7,12,17,22,27,32,321,322,323,37,371,372,373,42,47,52,1002) then 2
        when cellnumber in (3,8,13,18,23,28,33,331,332,333,38,381,382,383,43,48,53,1003) then 3
        when cellnumber in (4,9,14,19,24,29,34,341,342,343,39,391,392,393,44,49,54,1004) then 4
        when cellnumber in (5,10,15,20,25,30,35,351,352,353,40,401,402,403,45,50,55,1005) then 5
        when cellnumber in (2001,2002,2003,2004,2005,2006,2007,2071,2072,2073,2008,2081,2082,2083,2009,2010,2011,9999) then 100

   end as a_tier_display_sequence, 

   fact_osds_goal_detail_revenue.averageneed, 
   fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, 
   fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, 
   fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, 
   fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, 
   fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 
   'YIELD RATE'::character varying AS student_status 
    FROM fact_osds_goal_detail_revenue
UNION ALL 
   SELECT fact_osds_goal_detail_revenue.enrolledcount AS goal_student_count, fact_osds_goal_detail_revenue.admittedcount AS goal_admitted_count, 
   fact_osds_goal_detail_revenue.enrolledcount AS goal_net_confirmed,  
   fact_osds_goal_detail_revenue.institution_key, fact_osds_goal_detail_revenue.academic_year_key, 
   fact_osds_goal_detail_revenue.population_key, 
 
   fact_osds_goal_detail_revenue.cellnumber, 
   case 
        when cellnumber in (1,2,3,4,5,2001) then 1
        when cellnumber in (6,7,8,9,10,2002) then 2
        when cellnumber in (11,12,13,14,15,2003) then 3
        when cellnumber in (16,17,18,19,20,2004) then 4
        when cellnumber in (21,22,23,24,25,2005) then 5
        when cellnumber in (26,27,28,29,30,2006) then 6
        when cellnumber in (31,32,33,34,35,2007) then 7
        when cellnumber in (311,321,331,341,351,2071) then 71 
        when cellnumber in (312,322,332,342,352,2072) then 72
        when cellnumber in (313,323,333,343,353,2073) then 73
        when cellnumber in (36,37,38,39,40,2008) then 8
        when cellnumber in (361,371,381,391,401,2081) then 81
        when cellnumber in (362,372,382,392,402,2082) then 82
        when cellnumber in (363,373,383,393,403,2083) then 83
        when cellnumber in (41,42,43,44,45,2009) then 9
        when cellnumber in (46,47,48,49,50,2010) then 10
        when cellnumber in (51,52,53,54,55,2011) then 11
        when cellnumber in (1001,1002,1003,1004,1005,9999) then 200
   end as f_tier_display_sequence, 
   
  case 
        when cellnumber in (1,6,11,16,21,26,31,311,312,313,36,361,362,363,41,46,51,1001) then 1
        when cellnumber in (2,7,12,17,22,27,32,321,322,323,37,371,372,373,42,47,52,1002) then 2
        when cellnumber in (3,8,13,18,23,28,33,331,332,333,38,381,382,383,43,48,53,1003) then 3
        when cellnumber in (4,9,14,19,24,29,34,341,342,343,39,391,392,393,44,49,54,1004) then 4
        when cellnumber in (5,10,15,20,25,30,35,351,352,353,40,401,402,403,45,50,55,1005) then 5
        when cellnumber in (2001,2002,2003,2004,2005,2006,2007,2071,2072,2073,2008,2081,2082,2083,2009,2010,2011,9999) then 100

   end as a_tier_display_sequence, 

   fact_osds_goal_detail_revenue.averageneed, 
   fact_osds_goal_detail_revenue.percentneedmetwithgift, fact_osds_goal_detail_revenue.institutionalgiftaid, 
   fact_osds_goal_detail_revenue.averagenetrevenue, fact_osds_goal_detail_revenue.averagenettuitionrevenue, 
   fact_osds_goal_detail_revenue.nettuitionfeerevenue, fact_osds_goal_detail_revenue.netrevenue, 
   fact_osds_goal_detail_revenue.total_aid, fact_osds_goal_detail_revenue.averageinstitutionalaid, 
   fact_osds_goal_detail_revenue.institutionalneedbasedgift + fact_osds_goal_detail_revenue.institutionalmeritbasedgift + fact_osds_goal_detail_revenue.institutionalpremiergift + fact_osds_goal_detail_revenue.institutionaltalentbasedgift + fact_osds_goal_detail_revenue.otherinstitutionalgiftaid + fact_osds_goal_detail_revenue.campusbasedgift + fact_osds_goal_detail_revenue.federalgrant + fact_osds_goal_detail_revenue.stategrant AS total_gifts, 
   'NET CONFIRMED'::character varying AS student_status 
    FROM fact_osds_goal_detail_revenue

   ) fgd
   on 1 = 1
   
   JOIN dim_osds_institution i ON fgd.institution_key = i.institution_key
   JOIN dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN dim_osds_population p ON fgd.population_key = p.population_key

  WHERE p.flag_active::character varying::text = 1::character varying::text ;
commit
;
GRANT SELECT ON edw.financial_aid_matrix_goals TO reportinguser;
GRANT SELECT ON edw.financial_aid_matrix_goals TO group qauser; 
GRANT SELECT ON edw.financial_aid_matrix_goals TO group biuser;
commit;