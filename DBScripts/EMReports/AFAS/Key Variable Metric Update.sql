update edw.tableau_reports 
set report_name = 'Key Variable Metrics',
set generic_url = 'https://reporting.ruffalonl.com/#/views/KeyVariableMetrics/KeyVariableMetrics'
where id = 106;
commit;