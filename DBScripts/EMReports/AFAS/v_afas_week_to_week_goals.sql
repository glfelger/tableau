DROP VIEW IF EXISTS edw.v_afas_week_to_week_goals;
commit;
CREATE OR REPLACE VIEW edw.v_afas_week_to_week_goals
(
  client,
  institution_key,
  academic_start_year,
  source_type,
  submitted_date,
  financial_tier_key,
  sub_population,
  student_status,
  academic_tier_key,
  f_tier_display_sequence,
  a_tier_display_sequence,
  goal_student_count,
  goal_yield_denom,
  hsgpa,
  satcomposite,
  actcomposite,
  recalculatedhsgpa,
  collegegpa,
  net_revenue,
  net_overall_revenue,
  tuition_fees_revenue,
  net_tuition_fees,
  i__g,
  new_inst_rat
)
AS 
 SELECT i.institution_name AS client, i.institution_key, a.academic_start_year, fgd.source_type, yl.submitted_date, fgd.financial_tier_key,
  p.population_name AS sub_population, fgd.student_status, fgd.academic_tier_key, ft2.display_sequence AS f_tier_display_sequence, at2.display_sequence AS a_tier_display_sequence, 
  fgd.goal_student_count, fgd.goal_yield_denom, fgd.hsgpa,
  fgd.satcomposite, fgd.actcomposite, fgd.recalculatedhsgpa, fgd.collegegpa, fgd.net_revenue, fgd.net_overall_revenue, fgd.tuition_fee_revenue AS tuition_fees_revenue, fgd.nettuitionfeerevenue as net_tuition_fees, 
  fgd.i__g, fgd.new_inst_rat
   FROM ((( SELECT fgd.institution_key, fgd.academic_year_key, fgd.source_type, 
                CASE
                    WHEN fgd.cellnumber >= 2001 AND fgd.cellnumber <= 2015 THEN fgd.financial_tier_key
                    ELSE NULL::integer
                END AS financial_tier_key, fgd.population_key, 
                CASE
                    WHEN fgd.cellnumber >= 1001 AND fgd.cellnumber <= 1007 THEN fgd.academic_tier_key
                    ELSE NULL::integer
                END AS academic_tier_key, 'Net Confirmed'::character varying AS student_status, fgd.enrolledcount AS goal_student_count, 0 AS goal_yield_denom, 0 AS hsgpa, 0 AS satcomposite, 0 AS actcomposite, 0 AS recalculatedhsgpa, 0 AS collegegpa, 0 AS net_revenue, 0 AS net_overall_revenue, 0 AS tuition_fee_revenue, 0 AS i__g, 0 AS new_inst_rat, 0 as nettuitionfeerevenue
           FROM edw.fact_osds_goal_detail_revenue fgd
          WHERE fgd.source_type::text = 'GoalDetail'::character varying::text
UNION ALL 
         SELECT fgd.institution_key, fgd.academic_year_key, fgd.source_type, 
                CASE
                    WHEN fgd.cellnumber >= 2001 AND fgd.cellnumber <= 2015 THEN fgd.financial_tier_key
                    ELSE NULL::integer
                END AS financial_tier_key, fgd.population_key, 
                CASE
                    WHEN fgd.cellnumber >= 1001 AND fgd.cellnumber <= 1007 THEN fgd.academic_tier_key
                    ELSE NULL::integer
                END AS academic_tier_key, 'Net Confirmed'::character varying AS student_status, 0 AS goal_student_count, fgd.admittedcount AS goal_yield_denom, 0 AS hsgpa, 0 AS satcomposite, 0 AS actcomposite, 0 AS recalculatedhsgpa, 0 AS collegegpa, 0 AS net_revenue, 0 AS net_overall_revenue, 0 AS tuition_fee_revenue, 0 AS i__g, 0 AS new_inst_rat, 0 as nettuitionfeerevenue
           FROM edw.fact_osds_goal_detail_revenue fgd
          WHERE fgd.source_type::text = 'GoalDetail'::character varying::text)
UNION ALL 
         SELECT fgd.institution_key, fgd.academic_year_key, fgd.source_type, 
                CASE
                    WHEN fgd.cellnumber >= 2001 AND fgd.cellnumber <= 2015 THEN fgd.financial_tier_key
                    ELSE NULL::integer
                END AS financial_tier_key, fgd.population_key, 
                CASE
                    WHEN fgd.cellnumber >= 1001 AND fgd.cellnumber <= 1007 THEN fgd.academic_tier_key
                    ELSE NULL::integer
                END AS academic_tier_key, 'Admitted'::character varying AS student_status, fgd.admittedcount AS goal_student_count, 0 AS goal_yield_denom, 0 AS hsgpa, 0 AS satcomposite, 0 AS actcomposite, 0 AS recalculatedhsgpa, 0 AS collegegpa, 0 AS net_revenue, 0 AS net_overall_revenue, 0 AS tuition_fee_revenue, 0 AS i__g, 0 AS new_inst_rat, 0 as nettuitionfeerevenue
           FROM edw.fact_osds_goal_detail_revenue fgd
          WHERE fgd.source_type::text = 'GoalDetail'::character varying::text)
UNION ALL 
         SELECT fgd.institution_key, fgd.academic_year_key, fgd.source_type, fgd.financial_tier_key, fgd.population_key, fgd.academic_tier_key, 'Net Confirmed'::character varying AS student_status, fgd.enrolledcount AS goal_student_count, 0 AS goal_yield_denom, fgd.hsgpa, fgd.satcomposite, fgd.actcomposite, fgd.recalculatedhsgpa, fgd.collegegpa, fgd.tuitionrevenue + fgd.requiredfeerevenue - fgd.unfundedinstitutionalgiftaid AS net_revenue, fgd.totalrevenue - fgd.employeedependentwaivers - fgd.unfundedinstitutionalgiftaid AS net_overall_revenue, fgd.tuitionrevenue + fgd.requiredfeerevenue AS tuition_fee_revenue, fgd.ins_merit + fgd.ins_need + fgd.talent_gift + fgd.other_aid + fgd.premieracademic AS i__g, fgd.academicindex AS new_inst_rat, nettuitionfeerevenue
           FROM fact_osds_goal_detail_revenue fgd
          WHERE fgd.source_type::text = 'GoalSummary'::character varying::text AND fgd.cellnumber = 9999) fgd
   JOIN edw.dim_osds_institution i ON fgd.institution_key = i.institution_key
   JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   JOIN edw.dim_osds_year_lookup yl ON fgd.institution_key = yl.institution_key AND a.academic_start_year = yl.academic_start_year
   LEFT JOIN edw.dim_osds_financial_tier ft2 ON fgd.financial_tier_key = ft2.financial_tier_key
   LEFT JOIN edw.dim_osds_academic_tier at2 ON fgd.academic_tier_key = at2.academic_tier_key
;
commit;
GRANT SELECT ON edw.v_afas_week_to_week_goals TO group qauser; 
GRANT SELECT ON edw.v_afas_week_to_week_goals TO group biuser;
GRANT SELECT ON edw.v_afas_week_to_week_goals TO reportinguser;
commit;

