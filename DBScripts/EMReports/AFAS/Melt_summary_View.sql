DROP VIEW IF EXISTS edw.melt_summary_view 
;

COMMIT
;

CREATE OR REPLACE VIEW edw.melt_summary_view
(
  client,
  institution_key,
  year,
  academic_start_year,
  submitted_date,
  sub_population,
  fafsa_status,
  packaged_status,
  student_status,
  student_status_param,
  net_confirmed,
  melt_confirmed,
  student_count,
  callout_gross_confirmed,
  callout_melt_confirmed,
  f_tier_display_sequence,
  actual_f_tier_display_sequence,
  a_tier_display_sequene,
  need_distribution,
  academic_distribution,
  student_id,
  last_name,
  first_name,
  state,
  szip_code,
  housing,
  gap,
  i__g,
  in_g,
  ip_g,
  im_g,
  isource_cd,
  counselor,
  date_isir_rcvd
)
AS 
 SELECT i.institution_name AS client, p.institution_key, a.academic_year_name AS "year", a.academic_start_year, date(dds.transmit_date) AS submitted_date, p.population_name AS sub_population, fsd.fafsa_status, 
        CASE
            WHEN s.flag_packaged IS NULL OR s.date_packaged IS NULL THEN 'Not Packaged'::character varying
            ELSE 'Packaged'::character varying
        END AS packaged_status, fsd.student_status, fsd.student_status_param, fsd.net_confirmed, fsd.melt_confirmed, fsd.student_count, fsd.callout_gross_confirmed, fsd.callout_melt_confirmed, ft.display_sequence AS f_tier_display_sequence, ft2.display_sequence AS actual_f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequene, ft.name AS need_distribution, "at".name AS academic_distribution, s.studentid AS student_id, s.last_name, s.first_name, s.state, s.szip_code, s.housing, fsd.gap, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, s.isource_cd, s.counselor, fsd.date_isir_rcvd
   FROM (( SELECT fosd.institution_key, fosd.student_detail_key, fosd.dataset_key, fosd.academic_year_key, fosd.student_key, fosd.gap, fosd.i__g, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, 
                CASE
                    WHEN fosd.flag_isir_rcvd = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 
                CASE
                    WHEN fosd.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    ELSE 'Melt'::character varying
                END AS student_status, 
                CASE
                    WHEN fosd.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    ELSE 'Melt'::character varying
                END AS student_status_param, 0 AS net_confirmed, 0 AS melt_confirmed, 0 AS student_count, fosd.flag_net_confirmed + fosd.flag_confirmed_cancelled AS callout_gross_confirmed, fosd.flag_confirmed_cancelled AS callout_melt_confirmed
           FROM edw.fact_osds_student_detail fosd
          WHERE fosd.flag_gross_confirmed
UNION ALL 
         SELECT fosd.institution_key, fosd.student_detail_key, fosd.dataset_key, fosd.academic_year_key, fosd.student_key, fosd.gap, fosd.i__g, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, 
                CASE
                    WHEN fosd.flag_isir_rcvd = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 'Net Confirmed'::character varying AS student_status, 'None'::character varying AS student_status_param, fosd.flag_net_confirmed AS net_confirmed, fosd.flag_confirmed_cancelled AS melt_confirmed, fosd.flag_net_confirmed AS student_count, 0 AS callout_gross_confirmed, 0 AS callout_melt_confirmed
           FROM edw.fact_osds_student_detail fosd
          WHERE fosd.flag_gross_confirmed = 1)
UNION ALL 
         SELECT fosd.institution_key, fosd.student_detail_key, fosd.dataset_key, fosd.academic_year_key, fosd.student_key, fosd.gap, fosd.i__g, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, 
                CASE
                    WHEN fosd.flag_isir_rcvd = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 'Melt'::character varying AS student_status, 'None'::character varying AS student_status_param, fosd.flag_net_confirmed AS net_confirmed, fosd.flag_confirmed_cancelled AS melt_confirmed, fosd.flag_confirmed_cancelled AS student_count, 0 AS callout_gross_confirmed, 0 AS callout_melt_confirmed
           FROM edw.fact_osds_student_detail fosd
          WHERE fosd.flag_gross_confirmed = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN ( SELECT dim_osds_student_population.student_detail_key, dim_osds_student_population.population_key, dim_osds_student_population.financial_tier_key, dim_osds_student_population.financial_tier_key AS actual_f_tier_key, dim_osds_student_population.academic_tier_key
   FROM edw.dim_osds_student_population
UNION ALL 
 SELECT sp.student_detail_key, sp.population_key, -99 AS financial_tier_key, sp.financial_tier_key AS actual_f_tier_key, sp.academic_tier_key
   FROM edw.dim_osds_student_population sp
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
  WHERE ft.display_sequence < 7::double precision) sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN ( SELECT dim_osds_financial_tier.financial_tier_key, dim_osds_financial_tier.name, dim_osds_financial_tier.display_sequence
   FROM edw.dim_osds_financial_tier
UNION ALL 
 SELECT -99 AS financial_tier_key, 'Total Need Based'::character varying AS name, 100 AS display_sequence) ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_financial_tier ft2 ON ft2.financial_tier_key = sp.actual_f_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar
;

COMMIT
;

GRANT SELECT ON edw.melt_summary_view TO reportinguser
  ;
commit
;