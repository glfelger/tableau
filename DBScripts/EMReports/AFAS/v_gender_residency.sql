DROP VIEW IF EXISTS edw.v_gender_residency
;
COMMIT
;
CREATE OR REPLACE VIEW edw.v_gender_residency
(
  student_key,
  institution_key,
  client,
  sub_population,
  submission_date,
  student_detail_key,
  academic_year_key,
  student_id,
  gender,
  housing,
  first_name,
  last_name,
  state,
  szip_code,
  flag_athlete,
  sport1,
  counselor,
  i__g,
  in_g,
  ip_g,
  im_g,
  student_status,
  admitted_count,
  net_confirmed_count,
  gross_confirmed_count,
  pending_count,
  confirmed_cancelled_count,
  yield_denominator_count,
  melt_denominator_count,
  student_count
)
AS 
 SELECT fsd.student_key, fsd.institution_key, i.institution_name AS client, p.population_name AS sub_population, d.transmit_date AS submission_date, fsd.student_detail_key, fsd.academic_year_key, fsd.student_id, fsd.gender, fsd.housing, fsd.first_name, fsd.last_name, fsd.state, fsd.szip_code, fsd.flag_athlete, fsd.sport1, fsd.counselor, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, fsd.student_status, fsd.admitted_count, fsd.net_confirmed_count, fsd.gross_confirmed_count, fsd.pending_count, fsd.confirmed_cancelled_count, fsd.yield_denominator_count, fsd.melt_denominator_count, fsd.student_count
   FROM ((((( SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, sd.student_id, sd.gender, sd.housing, sd.first_name, sd.last_name, sd.state, sd.szip_code, sd.flag_athlete, sd.sport1, sd.counselor, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, 'Admitted'::character varying AS student_status, 1 AS admitted_count, 0 AS net_confirmed_count, 0 AS gross_confirmed_count, 0 AS pending_count, 0 AS confirmed_cancelled_count, 0 AS yield_denominator_count, 0 AS melt_denominator_count, 1 AS student_count
           FROM edw.fact_osds_student_detail fsd
      JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
     WHERE fsd.flag_admitted = 1
UNION ALL 
         SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, sd.student_id, sd.gender, sd.housing, sd.first_name, sd.last_name, sd.state, sd.szip_code, sd.flag_athlete, sd.sport1, sd.counselor, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, 'Net Confirmed'::character varying AS student_status, 0 AS admitted_count, 1 AS net_confirmed_count, 0 AS gross_confirmed_count, 0 AS pending_count, 0 AS confirmed_cancelled_count, 0 AS yield_denominator_count, 0 AS melt_denominator_count, 1 AS student_count
           FROM edw.fact_osds_student_detail fsd
      JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
     WHERE fsd.flag_net_confirmed = 1)
UNION ALL 
         SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, sd.student_id, sd.gender, sd.housing, sd.first_name, sd.last_name, sd.state, sd.szip_code, sd.flag_athlete, sd.sport1, sd.counselor, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, 'Net Confirmed'::character varying AS student_status, 0 AS admitted_count, 0 AS net_confirmed_count, 0 AS gross_confirmed_count, 0 AS pending_count, 0 AS confirmed_cancelled_count, 1 AS yield_denominator_count, 0 AS melt_denominator_count, 0 AS student_count
           FROM edw.fact_osds_student_detail fsd
      JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
     WHERE fsd.flag_admitted = 1)
UNION ALL 
         SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, sd.student_id, sd.gender, sd.housing, sd.first_name, sd.last_name, sd.state, sd.szip_code, sd.flag_athlete, sd.sport1, sd.counselor, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, 'Pending'::character varying AS student_status, 0 AS admitted_count, 0 AS net_confirmed_count, 0 AS gross_confirmed_count, 1 AS pending_count, 0 AS confirmed_cancelled_count, 0 AS yield_denominator_count, 0 AS melt_denominator_count, 1 AS student_count
           FROM edw.fact_osds_student_detail fsd
      JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
     WHERE fsd.flag_pending = 1)
UNION ALL 
         SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, sd.student_id, sd.gender, sd.housing, sd.first_name, sd.last_name, sd.state, sd.szip_code, sd.flag_athlete, sd.sport1, sd.counselor, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, 'Confirmed Cancelled'::character varying AS student_status, 0 AS admitted_count, 0 AS net_confirmed_count, 0 AS gross_confirmed_count, 0 AS pending_count, 1 AS confirmed_cancelled_count, 0 AS yield_denominator_count, 0 AS melt_denominator_count, 1 AS student_count
           FROM edw.fact_osds_student_detail fsd
      JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
     WHERE fsd.flag_confirmed_cancelled = 1)
UNION ALL 
         SELECT fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.student_detail_key, fsd.academic_year_key, sd.student_id, sd.gender, sd.housing, sd.first_name, sd.last_name, sd.state, sd.szip_code, sd.flag_athlete, sd.sport1, sd.counselor, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, 'Net Confirmed'::character varying AS student_status, 0 AS admitted_count, 0 AS net_confirmed_count, 0 AS gross_confirmed_count, 0 AS pending_count, 0 AS confirmed_cancelled_count, 0 AS yield_denominator_count, 1 AS melt_denominator_count, 0 AS student_count
           FROM edw.fact_osds_student_detail fsd
      JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
     WHERE fsd.flag_gross_confirmed = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset d ON fsd.dataset_key = d.dataset_key AND fsd.institution_key = d.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
  WHERE d.flag_active = '1'::bpchar AND p.flag_active::character varying::text = 1::character varying::text 
  AND d.process_type_id = 2 AND d.dataset_status_id = 4 
  AND "date_part"('year'::character varying::text, d.transmit_date) >= ("date_part"('year'::character varying::text, 'now'::character varying::date) - 2) 
   ;
commit
;
GRANT SELECT ON edw.v_gender_residency TO reportinguser;
commit;