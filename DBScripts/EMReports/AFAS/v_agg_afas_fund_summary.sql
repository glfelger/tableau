DROP VIEW IF EXISTS edw.v_agg_afas_fund_summary
;
commit
;
CREATE OR REPLACE VIEW edw.v_agg_afas_fund_summary
(
  client,
  institution_key,
  student_id,
  year,
  academic_start_year,
  submitted_date,
  sub_population,
  student_status,
  fafsa_status,
  name,
  a_tier_name,
  academictiersequence,
  financialtiersequence,
  fund_category,
  fund_description,
  amount,
  fund_rank,
  new_inst_rat,
  i_ug,
  i__g,
  tuition,
  fees,
  board,
  room,
  empb,
  hsgpa,
  hsgpa_student_count,
  c_gpa,
  c_gpa_student_count,
  sat_comp,
  sat_comp_student_count,
  act_comp,
  act_comp_student_count,
  standing,
  standing_student_count,
  inst_rat,
  calc_indx,
  calc_indx_student_count,
  need,
  need_student_count,
  fafsa_need_student_count,
  fafsa_student_count,
  fafsa_need,
  student_count,
  net_confirmed_count,
  admitted_count,
  last_name,
  first_name,
  state,
  scity,
  szip_code,
  housing,
  gap,
  in_g,
  ip_g,
  im_g,
  isource_cd,
  counselor,
  date_isir_rcvd,
  major1_admit,
  student_use
)
AS 
 SELECT i.institution_name AS client, i.institution_key, s.studentid AS student_id, a.academic_year_name AS "year", a.academic_start_year, dds.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.student_status, fsd.fafsa_status, ft.name, "at".name AS a_tier_name, "at".display_sequence AS academictiersequence, ft.display_sequence AS financialtiersequence, sf.fund_category, sf.fund_description, sf.amount, COALESCE(sf.fund_rank, 1::bigint) AS fund_rank, 
        CASE
            WHEN sp.new_inst_rat::text = 'Calculated Index'::character varying::text THEN fsd.calc_indx
            WHEN sp.new_inst_rat::text = 'HS GPA'::character varying::text THEN fsd.hsgpa
            WHEN sp.new_inst_rat::text = 'College GPA'::character varying::text THEN fsd.c_gpa
            WHEN sp.new_inst_rat::text = 'ACT Composite'::character varying::text THEN fsd.act_comp::double precision
            WHEN sp.new_inst_rat::text = 'Recalculated HS GPA'::character varying::text THEN fsd.hsgpa_recalc
            WHEN sp.new_inst_rat::text = 'SAT Composite'::character varying::text THEN fsd.sat_comp::double precision
            WHEN sp.new_inst_rat::text = 'Recalculated College GPA'::character varying::text THEN fsd.c_gpa_recalc
            WHEN sp.new_inst_rat::text = 'Predicted GPA'::character varying::text THEN fsd.p_gpa
            ELSE fsd.inst_rat
        END AS new_inst_rat, fsd.i_ug, fsd.i__g, fsd.tuition, fsd.fees, fsd.board, fsd.room, fsd.empb, fsd.hsgpa, 
        CASE
            WHEN COALESCE(fsd.hsgpa, 0::double precision) > 0::double precision THEN fsd.student_count
            ELSE 0
        END AS hsgpa_student_count, fsd.c_gpa, 
        CASE
            WHEN COALESCE(fsd.c_gpa, 0::double precision) > 0::double precision THEN fsd.student_count
            ELSE 0
        END AS c_gpa_student_count, fsd.sat_comp, 
        CASE
            WHEN COALESCE(fsd.sat_comp, 0) > 0 THEN fsd.student_count
            ELSE 0
        END AS sat_comp_student_count, fsd.act_comp, 
        CASE
            WHEN COALESCE(fsd.act_comp, 0) > 0 THEN fsd.student_count
            ELSE 0
        END AS act_comp_student_count, fsd.standing, 
        CASE
            WHEN COALESCE(fsd.standing, 0) > 0 THEN fsd.student_count
            ELSE 0
        END AS standing_student_count, fsd.inst_rat, fsd.calc_indx, 
        CASE
            WHEN COALESCE(fsd.calc_indx, 0::double precision) > 0::double precision THEN fsd.student_count
            ELSE 0
        END AS calc_indx_student_count, fsd.need, 
        CASE
            WHEN COALESCE(fsd.need::double precision, 0::double precision) > 0::double precision THEN fsd.student_count
            ELSE 0
        END AS need_student_count, 
        CASE
            WHEN COALESCE(fsd.fafsa_need::double precision, 0::double precision) > 0::double precision THEN fsd.fafsa_student_count
            ELSE 0
        END AS fafsa_need_student_count, fsd.fafsa_student_count, fsd.fafsa_need, fsd.student_count, fsd.net_confirmed_count, fsd.admitted_count, s.last_name, s.first_name, s.state, s.scity, s.szip_code, s.housing, fsd.gap, fsd.in_g, fsd.ip_g, fsd.im_g, s.isource_cd, s.counselor, fsd.date_isir_rcvd, s.major1_admit, fsd.student_use
   FROM (( SELECT fosd.institution_key, fosd.dataset_key, fosd.student_detail_key, fosd.academic_year_key, fosd.student_key, fosd.i_ug, fosd.i__g, fosd.tuition, fosd.fees, fosd.board, fosd.room, fosd.empb, fosd.hsgpa, fosd.c_gpa, fosd.p_gpa, fosd.sat_comp, fosd.act_comp, fosd.standing, fosd.hsgpa_recalc, fosd.c_gpa_recalc, fosd.inst_rat, fosd.calc_indx, 'Admitted'::character varying AS student_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 1
                    ELSE NULL::integer
                END AS fafsa_student_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.need
                    ELSE NULL::integer
                END AS fafsa_need, fosd.need, 1 AS student_count, 0 AS net_confirmed_count, 0 AS admitted_count, fosd.gap, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, 'Yes'::character varying AS student_use
           FROM edw.fact_osds_student_detail fosd
          WHERE fosd.flag_admitted = 1
UNION ALL 
         SELECT fosd.institution_key, fosd.dataset_key, fosd.student_detail_key, fosd.academic_year_key, fosd.student_key, fosd.i_ug, fosd.i__g, fosd.tuition, fosd.fees, fosd.board, fosd.room, fosd.empb, fosd.hsgpa, fosd.c_gpa, fosd.p_gpa, fosd.sat_comp, fosd.act_comp, fosd.standing, fosd.hsgpa_recalc, fosd.c_gpa_recalc, fosd.inst_rat, fosd.calc_indx, 
                CASE
                    WHEN fosd.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fosd.flag_confirmed_cancelled = 1 THEN 'Confirmed Cancelled'::character varying
                    WHEN fosd.flag_canc = 1 AND fosd.flag_con = 0 THEN 'Cancelled'::character varying
                    ELSE 'Pending'::character varying
                END AS student_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 1
                    ELSE NULL::integer
                END AS fafsa_student_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.need
                    ELSE NULL::integer
                END AS fafsa_need, fosd.need, 1 AS student_count, 0 AS net_confirmed_count, 0 AS admitted_count, fosd.gap, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, 'Yes'::character varying AS student_use
           FROM edw.fact_osds_student_detail fosd
          WHERE fosd.flag_net_confirmed = 1 OR fosd.flag_pending = 1 OR fosd.flag_confirmed_cancelled = 1 OR fosd.flag_canc = 1)
UNION ALL 
         SELECT fosd.institution_key, fosd.dataset_key, fosd.student_detail_key, fosd.academic_year_key, fosd.student_key, 0 AS i_ug, 0 AS i__g, 0 AS tuition, 0 AS fees, 0 AS board, 0 AS room, 0 AS empb, 0 AS hsgpa, 0 AS c_gpa, 0 AS p_gpa, 0 AS sat_comp, 0 AS act_comp, 0 AS standing, 0 AS hsgpa_recalc, fosd.c_gpa_recalc, 0 AS inst_rat, 0 AS calc_indx, 'Net Confirmed'::character varying AS student_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 0 AS fafsa_student_count, 0 AS fafsa_need, 0 AS need, 0 AS student_count, fosd.flag_net_confirmed AS net_confirmed_count, 1 AS admitted_count, 0 AS gap, 0 AS in_g, 0 AS ip_g, 0 AS im_g, fosd.date_isir_rcvd, 'No'::character varying AS student_use
           FROM edw.fact_osds_student_detail fosd
          WHERE fosd.flag_admitted = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   LEFT JOIN ( SELECT stf.student_key, stf.fund_category, stf.fund_description, stf.amount, pg_catalog.rank()
  OVER( 
  PARTITION BY stf.student_key
  ORDER BY stf.fund_description DESC) AS fund_rank
   FROM edw.dim_osds_student_fund stf 
   where amount > 0
UNION ALL 
 SELECT DISTINCT stf.student_key, NULL::character varying AS fund_category, NULL::character varying AS fund_description, sum(stf.amount) as amount, -99 AS fund_rank
   FROM edw.dim_osds_student_fund stf
   where amount > 0
   group by stf.student_key) sf ON s.student_key = sf.student_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar
  ;
commit
;

GRANT SELECT ON edw.v_agg_afas_fund_summary TO reportinguser;
GRANT SELECT ON edw.v_agg_afas_fund_summary TO group qauser; 
GRANT SELECT ON edw.v_agg_afas_fund_summary TO group biuser;
commit;
