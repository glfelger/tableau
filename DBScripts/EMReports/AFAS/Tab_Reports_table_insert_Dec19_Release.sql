update edw.tableau_reports set is_active = 'Y' where id = 93; 
commit
;
insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Department Distribution', 'https://reporting.ruffalonl.com/#/views/DepartmentsDistribution/DepartmentsDistribution', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;