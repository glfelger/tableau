DROP VIEW IF EXISTS edw.v_afas_fafsa_filer
;
commit
;
CREATE OR REPLACE VIEW edw.v_afas_fafsa_filer
(
  client,
  institution_key,
  academic_start_year,
  submitted_date,
  sub_population,
  student_status,
  a_tier_name,
  name,
  f_tier_display_sequence,
  actual_f_tier_display_sequence,
  a_tier_display_sequence,
  studentid,
  student_detail_key,
  last_name,
  first_name,
  state,
  szip_code,
  counselor,
  isource_cd,
  date_packaged,
  i__g,
  in_g,
  ip_g,
  im_g,
  date_isir_rcvd,
  flag_isir_valid,
  flag_isir_rcvd,
  dependency,
  student_count,
  fafsa_status,
  fafsa_filer_count,
  parent_income,
  parent_income_count,
  efc_9_month,
  efc_count,
  need,
  need_count,
  intent_count
)
AS 
 SELECT i.institution_name AS client, p.institution_key, a.academic_start_year, dds.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.student_status, "at".name AS a_tier_name, ft.name, ft.display_sequence AS f_tier_display_sequence, ft2.display_sequence AS actual_f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence, fsd.studentid, fsd.student_detail_key, fsd.last_name, fsd.first_name, fsd.state, fsd.szip_code, fsd.counselor, fsd.isource_cd, fsd.date_packaged, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, fsd.date_isir_rcvd, fsd.flag_isir_valid, fsd.flag_isir_rcvd, fsd.dependency, fsd.student_count, fsd.fafsa_status, fsd.fafsa_filer_count, fsd.parent_income, fsd.parent_income_count, fsd.efc_9_month, fsd.efc_count, fsd.need, fsd.need_count, fsd.intent_count
   FROM ((( SELECT fosd.institution_key, fosd.student_detail_key, fosd.dataset_key, fosd.flag_active, fosd.academic_year_key, fosd.student_key, 'Admitted'::character varying AS student_status, s.studentid, s.last_name, s.first_name, s.state, s.szip_code, s.counselor, s.isource_cd, s.date_packaged, s.flag_intent, fosd.i__g, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, fosd.flag_isir_valid, fosd.flag_isir_rcvd, lower(fosd.dependency::text)::character varying AS dependency, 1 AS student_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 1
                    ELSE 0
                END AS fafsa_filer_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN fosd.parent_income
                    ELSE 0
                END AS parent_income, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN 1
                    ELSE NULL::integer
                END AS parent_income_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.efc_9_month
                    ELSE 0
                END AS efc_9_month, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.efc_9_month >= 0 THEN 1
                    ELSE 0
                END AS efc_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.need
                    ELSE 0
                END AS need, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.need > 0 THEN 1
                    ELSE 0
                END AS need_count, 
                CASE
                    WHEN s.flag_intent = 1 AND fosd.flag_isir_valid = 0 THEN 1
                    ELSE 0
                END AS intent_count
           FROM edw.fact_osds_student_detail fosd
      JOIN edw.dim_osds_student s ON fosd.student_key = s.student_key
     WHERE fosd.flag_admitted = 1
UNION ALL 
         SELECT fosd.institution_key, fosd.student_detail_key, fosd.dataset_key, fosd.flag_active, fosd.academic_year_key, fosd.student_key, 'Net Confirmed'::character varying AS student_status, s.studentid, s.last_name, s.first_name, s.state, s.szip_code, s.counselor, s.isource_cd, s.date_packaged, s.flag_intent, fosd.i__g, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, fosd.flag_isir_valid, fosd.flag_isir_rcvd, lower(fosd.dependency::text)::character varying AS dependency, 1 AS student_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 1
                    ELSE 0
                END AS fafsa_filer_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN fosd.parent_income
                    ELSE 0
                END AS parent_income, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN 1
                    ELSE NULL::integer
                END AS parent_income_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.efc_9_month
                    ELSE 0
                END AS efc_9_month, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.efc_9_month >= 0 THEN 1
                    ELSE 0
                END AS efc_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.need
                    ELSE 0
                END AS need, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.need > 0 THEN 1
                    ELSE 0
                END AS need_count, 
                CASE
                    WHEN s.flag_intent = 1 AND fosd.flag_isir_valid = 0 THEN 1
                    ELSE 0
                END AS intent_count
           FROM edw.fact_osds_student_detail fosd
      JOIN edw.dim_osds_student s ON fosd.student_key = s.student_key
     WHERE fosd.flag_net_confirmed = 1)
UNION ALL 
         SELECT fosd.institution_key, fosd.student_detail_key, fosd.dataset_key, fosd.flag_active, fosd.academic_year_key, fosd.student_key, 'Confirmed Cancelled'::character varying AS student_status, s.studentid, s.last_name, s.first_name, s.state, s.szip_code, s.counselor, s.isource_cd, s.date_packaged, s.flag_intent, fosd.i__g, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, fosd.flag_isir_valid, fosd.flag_isir_rcvd, lower(fosd.dependency::text)::character varying AS dependency, 1 AS student_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 1
                    ELSE 0
                END AS fafsa_filer_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN fosd.parent_income
                    ELSE 0
                END AS parent_income, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN 1
                    ELSE NULL::integer
                END AS parent_income_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.efc_9_month
                    ELSE 0
                END AS efc_9_month, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.efc_9_month >= 0 THEN 1
                    ELSE 0
                END AS efc_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.need
                    ELSE 0
                END AS need, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.need > 0 THEN 1
                    ELSE 0
                END AS need_count, 
                CASE
                    WHEN s.flag_intent = 1 AND fosd.flag_isir_valid = 0 THEN 1
                    ELSE 0
                END AS intent_count
           FROM edw.fact_osds_student_detail fosd
      JOIN edw.dim_osds_student s ON fosd.student_key = s.student_key
     WHERE fosd.flag_confirmed_cancelled = 1)
UNION ALL 
         SELECT fosd.institution_key, fosd.student_detail_key, fosd.dataset_key, fosd.flag_active, fosd.academic_year_key, fosd.student_key, 'Pending'::character varying AS student_status, s.studentid, s.last_name, s.first_name, s.state, s.szip_code, s.counselor, s.isource_cd, s.date_packaged, s.flag_intent, fosd.i__g, fosd.in_g, fosd.ip_g, fosd.im_g, fosd.date_isir_rcvd, fosd.flag_isir_valid, fosd.flag_isir_rcvd, lower(fosd.dependency::text)::character varying AS dependency, 1 AS student_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN 1
                    ELSE 0
                END AS fafsa_filer_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN fosd.parent_income
                    ELSE 0
                END AS parent_income, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND lower(fosd.dependency::text) = 'd'::character varying::text AND fosd.parent_income > 0 THEN 1
                    ELSE NULL::integer
                END AS parent_income_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.efc_9_month
                    ELSE 0
                END AS efc_9_month, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.efc_9_month >= 0 THEN 1
                    ELSE 0
                END AS efc_count, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 THEN fosd.need
                    ELSE 0
                END AS need, 
                CASE
                    WHEN fosd.flag_isir_valid = 1 AND fosd.need > 0 THEN 1
                    ELSE 0
                END AS need_count, 
                CASE
                    WHEN s.flag_intent = 1 AND fosd.flag_isir_valid = 0 THEN 1
                    ELSE 0
                END AS intent_count
           FROM edw.fact_osds_student_detail fosd
      JOIN edw.dim_osds_student s ON fosd.student_key = s.student_key
     WHERE fosd.flag_pending = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN (( SELECT dim_osds_student_population.student_detail_key, dim_osds_student_population.population_key, dim_osds_student_population.financial_tier_key, dim_osds_student_population.financial_tier_key AS actual_f_tier_key, dim_osds_student_population.academic_tier_key
   FROM edw.dim_osds_student_population
UNION ALL 
 SELECT sp.student_detail_key, sp.population_key, -99 AS financial_tier_key, sp.financial_tier_key AS actual_f_tier_key, sp.academic_tier_key
   FROM edw.dim_osds_student_population sp
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
  WHERE ft.display_sequence < 7::double precision)
UNION ALL 
 SELECT sp.student_detail_key, sp.population_key, -100 AS financial_tier_key, sp.financial_tier_key AS actual_f_tier_key, sp.academic_tier_key
   FROM edw.dim_osds_student_population sp) sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN (( SELECT dim_osds_financial_tier.financial_tier_key, dim_osds_financial_tier.name, dim_osds_financial_tier.display_sequence
   FROM edw.dim_osds_financial_tier
UNION ALL 
 SELECT -99 AS financial_tier_key, 'Total Need Based'::character varying AS name, 100 AS display_sequence)
UNION ALL 
 SELECT -100 AS financial_tier_key, 'Grand Total'::character varying AS name, 200 AS display_sequence) ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_financial_tier ft2 ON ft2.financial_tier_key = sp.actual_f_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar 
  ;
commit
;
grant select on edw.v_afas_fafsa_filer to reportinguser;
GRANT SELECT ON edw.v_afas_fafsa_filer TO group qauser; 
GRANT SELECT ON edw.v_afas_fafsa_filer TO group biuser; 
;
commit
;
