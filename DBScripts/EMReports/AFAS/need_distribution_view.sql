DROP VIEW IF EXISTS edw.need_distribution_view
;
commit
;
CREATE OR REPLACE VIEW edw.need_distribution_view
AS
 SELECT i.institution_name AS client,  p.institution_key,
 a.academic_year_name AS "year", date(dds.transmit_date) AS submitted_date, 
 p.population_name AS sub_population, fsd.student_status,
 fsd.gift, fsd.need, fsd.i__g, fsd.i_ug, fsd.tuition, fsd.fees,  
 "at".name AS a_tier_name,  ft.name, ft2.name as actual_name, ft2.display_sequence as actual_f_display_sequence, 
 ft.display_sequence AS f_tier_display_sequence, "at".display_sequence AS a_tier_display_sequence
   FROM (((( SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Admitted'::character varying AS student_status, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.gift, fact_osds_student_detail.need
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Gross Confirmed'::character varying AS student_status, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.gift, fact_osds_student_detail.need
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_con = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Net Confirmed'::character varying AS student_status, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.gift, fact_osds_student_detail.need
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_con = 1 AND fact_osds_student_detail.flag_canc <> 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Confirmed Cancelled'::character varying AS student_status, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.gift, fact_osds_student_detail.need
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_con = 1 AND fact_osds_student_detail.flag_canc = 1)
UNION ALL 
         SELECT fact_osds_student_detail.institution_key, fact_osds_student_detail.student_detail_key, fact_osds_student_detail.dataset_key, fact_osds_student_detail.flag_active, fact_osds_student_detail.academic_year_key, fact_osds_student_detail.student_key, 'Pending'::character varying AS student_status, fact_osds_student_detail.i__g, fact_osds_student_detail.i_ug, fact_osds_student_detail.tuition, fact_osds_student_detail.fees, fact_osds_student_detail.board, fact_osds_student_detail.room, fact_osds_student_detail.gift, fact_osds_student_detail.need
           FROM edw.fact_osds_student_detail
          WHERE fact_osds_student_detail.flag_admitted = 1 AND fact_osds_student_detail.flag_con <> 1 AND fact_osds_student_detail.flag_canc <> 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset dds ON fsd.dataset_key = dds.dataset_key AND fsd.institution_key = dds.institution_key
   JOIN ( SELECT dim_osds_student_population.student_detail_key, dim_osds_student_population.population_key, dim_osds_student_population.financial_tier_key, dim_osds_student_population.financial_tier_key as actual_f_tier_key, dim_osds_student_population.academic_tier_key
          FROM edw.dim_osds_student_population
           UNION ALL 
          SELECT sp.student_detail_key, sp.population_key, -99 AS financial_tier_key, sp.financial_tier_key as actual_f_tier_key,sp.academic_tier_key
            FROM edw.dim_osds_student_population sp
          JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
        WHERE ft.display_sequence < 7::double precision
        ) sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON fsd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_student s ON fsd.student_key = s.student_key
   JOIN ( SELECT dim_osds_financial_tier.financial_tier_key, dim_osds_financial_tier.financial_tier_id, dim_osds_financial_tier.name, dim_osds_financial_tier.display_sequence
          FROM edw.dim_osds_financial_tier
          UNION ALL 
          SELECT -99 AS financial_tier_key, 13 AS financial_tier_id, 'Total Need Based'::character varying AS name, 13 AS display_sequence
         ) ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_financial_tier "ft2" ON "ft2".financial_tier_key = sp.actual_f_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE dds.flag_active = '1'::bpchar AND dds.process_type_id = 2 AND dds.dataset_status_id = 4 AND p.flag_active = '1'::bpchar 
  ;
commit;

GRANT SELECT ON edw.need_distribution_view TO reportinguser;commit;
