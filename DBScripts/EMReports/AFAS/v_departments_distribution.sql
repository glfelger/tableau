DROP VIEW IF EXISTS edw.v_departments_distribution
;
commit
;
CREATE OR REPLACE VIEW edw.v_departments_distribution
(
  student_detail_key,
  institution_key,
  client,
  dataset_key,
  academic_year_key,
  academic_start_year,
  year,
  submitted_date,
  sub_population,
  student_status,
  student_id,
  last_name,
  first_name,
  szip_code,
  gender,
  admit_cd,
  admit_type,
  app_type,
  counselor,
  country,
  denom_cd,
  state,
  ethnic_cd,
  hscode,
  housing, 
  housing_admissions,
  isource_cd,
  dept1_admit,
  major1_admit,
  market_segment,
  model_score_bin,
  sport1,
  subinst_cd,
  admit_cd_desc,
  admit_type_desc,
  app_type_desc,
  counselor_desc,
  country_cd_desc,
  denom_cd_desc,
  dept1_admit_desc,
  ethnic_cd_desc,
  hscode_desc,
  isource_cd_desc,
  major1_admit_desc,
  market_seg_desc,
  subinst_cd_desc,
  sport1_desc,
  housing_admissions_desc,
  model_score_bin_desc,
  i__g,
  in_g,
  ip_g,
  im_g,
  student_count,
  tuition,
  fees,
  room,
  board,
  empb,
  hsgpa,
  c_gpa,
  sat_comp,
  act_comp,
  new_inst_rat,
  standing,
  fafsa_status,
  need,
  net_confirmed_count,
  admitted_count
)
AS 
 SELECT fsd.student_detail_key, fsd.institution_key, i.institution_name AS client, fsd.dataset_key, fsd.academic_year_key, a.academic_start_year, a.academic_year_name AS "year", d.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.student_status, sd.studentid AS student_id, sd.last_name, sd.first_name, sd.szip_code, sd.gender, sd.admit_cd, sd.admit_type, sd.app_type, sd.counselor, sd.country, sd.denom_cd, sd.state, sd.ethnic_cd, sd.hscode, sd.housing, sd.housing_admissions, sd.isource_cd, sd.dept1_admit, sd.major1_admit, sd.market_segment, sd.model_score_bin, sd.sport1, sd.subinst_cd, sd.admit_cd_desc, sd.admit_type_desc, sd.app_type_desc, sd.counselor_desc, sd.country_cd_desc, sd.denom_cd_desc, sd.dept1_admit_desc, sd.ethnic_cd_desc, sd.hscode_desc, sd.isource_cd_desc, sd.major1_admit_desc, sd.market_seg_desc, sd.subinst_cd_desc, sd.sport1_desc, sd.housing_admissions_desc, sd.model_score_bin_desc, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, fsd.student_count, fsd.tuition, fsd.fees, fsd.room, fsd.board, fsd.empb, fsd.hsgpa, fsd.c_gpa, fsd.sat_comp, fsd.act_comp, 
        CASE
            WHEN sp.new_inst_rat::text = 'Calculated Index'::character varying::text THEN fsd.calc_indx
            WHEN sp.new_inst_rat::text = 'HS GPA'::character varying::text THEN fsd.hsgpa
            WHEN sp.new_inst_rat::text = 'College GPA'::character varying::text THEN fsd.c_gpa
            WHEN sp.new_inst_rat::text = 'ACT Composite'::character varying::text THEN fsd.act_comp::double precision
            WHEN sp.new_inst_rat::text = 'Recalculated HS GPA'::character varying::text THEN fsd.hsgpa_recalc
            WHEN sp.new_inst_rat::text = 'SAT Composite'::character varying::text THEN fsd.sat_comp::double precision
            WHEN sp.new_inst_rat::text = 'Recalculated College GPA'::character varying::text THEN fsd.c_gpa_recalc
            WHEN sp.new_inst_rat::text = 'Predicted GPA'::character varying::text THEN fsd.p_gpa
            ELSE fsd.inst_rat
        END AS new_inst_rat, fsd.standing, fsd.fafsa_status, fsd.need, fsd.net_confirmed_count, fsd.admitted_count
   FROM (( SELECT fsd.student_detail_key, fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.academic_year_key, 'Admitted'::character varying AS student_status, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.in_g, 0) AS in_g, COALESCE(fsd.ip_g, 0) AS ip_g, COALESCE(fsd.im_g, 0) AS im_g, 1 AS student_count, COALESCE(fsd.tuition, 0) AS tuition, COALESCE(fsd.fees, 0) AS fees, COALESCE(fsd.room, 0) AS room, COALESCE(fsd.board, 0) AS board, COALESCE(fsd.empb, 0) AS empb, COALESCE(fsd.hsgpa, 0::double precision) AS hsgpa, COALESCE(fsd.c_gpa, 0::double precision) AS c_gpa, COALESCE(fsd.p_gpa, 0::double precision) AS p_gpa, COALESCE(fsd.c_gpa_recalc, 0::double precision) AS c_gpa_recalc, COALESCE(fsd.sat_comp, 0) AS sat_comp, COALESCE(fsd.act_comp, 0) AS act_comp, COALESCE(fsd.calc_indx, 0::double precision) AS calc_indx, COALESCE(fsd.standing, 0) AS standing, COALESCE(fsd.inst_rat, 0::double precision) AS inst_rat, COALESCE(fsd.hsgpa_recalc, 0::double precision) AS hsgpa_recalc, 
                CASE
                    WHEN fsd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, COALESCE(fsd.need, 0) AS need, 0 AS net_confirmed_count, 0 AS admitted_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_admitted = 1
UNION ALL 
         SELECT fsd.student_detail_key, fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.academic_year_key, 
                CASE
                    WHEN fsd.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fsd.flag_confirmed_cancelled = 1 THEN 'Confirmed Cancelled'::character varying
                    WHEN fsd.flag_canc = 1 AND fsd.flag_con = 0 THEN 'Cancelled'::character varying
                    ELSE 'Pending'::character varying
                END AS student_status, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.in_g, 0) AS in_g, COALESCE(fsd.ip_g, 0) AS ip_g, COALESCE(fsd.im_g, 0) AS im_g, 1 AS student_count, COALESCE(fsd.tuition, 0) AS tuition, COALESCE(fsd.fees, 0) AS fees, COALESCE(fsd.room, 0) AS room, COALESCE(fsd.board, 0) AS board, COALESCE(fsd.empb, 0) AS empb, COALESCE(fsd.hsgpa, 0::double precision) AS hsgpa, COALESCE(fsd.c_gpa, 0::double precision) AS c_gpa, COALESCE(fsd.p_gpa, 0::double precision) AS p_gpa, COALESCE(fsd.c_gpa_recalc, 0::double precision) AS c_gpa_recalc, COALESCE(fsd.sat_comp, 0) AS sat_comp, COALESCE(fsd.act_comp, 0) AS act_comp, COALESCE(fsd.calc_indx, 0::double precision) AS calc_indx, COALESCE(fsd.standing, 0) AS standing, COALESCE(fsd.inst_rat, 0::double precision) AS inst_rat, COALESCE(fsd.hsgpa_recalc, 0::double precision) AS hsgpa_recalc, 
                CASE
                    WHEN fsd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, COALESCE(fsd.need, 0) AS need, 0 AS net_confirmed_count, 0 AS admitted_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_net_confirmed = 1 OR fsd.flag_confirmed_cancelled = 1 OR fsd.flag_pending = 1 OR fsd.flag_canc = 1)
UNION ALL 
         SELECT fsd.student_detail_key, fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.academic_year_key, 'Net Confirmed'::character varying AS student_status, 0 AS i__g, 0 AS in_g, 0 AS ip_g, 0 AS im_g, 0 AS student_count, 0 AS tuition, 0 AS fees, 0 AS room, 0 AS board, 0 AS empb, 0 AS hsgpa, 0 AS c_gpa, 0 AS p_gpa, 0 AS c_gpa_recalc, 0 AS sat_comp, 0 AS act_comp, 0 AS calc_indx, 0 AS standing, 0 AS inst_rat, 0 AS hsgpa_recalc, 
                CASE
                    WHEN fsd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, 0 AS need, fsd.flag_net_confirmed AS net_confirmed_count, fsd.flag_admitted AS admitted_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_admitted = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset d ON fsd.dataset_key = d.dataset_key AND fsd.institution_key = d.institution_key
   JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON a.academic_year_key = fsd.academic_year_key
  WHERE d.flag_active = '1'::bpchar AND p.flag_active::character varying::text = 1::character varying::text 
  AND d.process_type_id = 2 AND d.dataset_status_id = 4 
    ;
commit
;
GRANT SELECT ON edw.v_departments_distribution TO reportinguser;
GRANT SELECT ON edw.v_departments_distribution TO group qauser; 
GRANT SELECT ON edw.v_departments_distribution TO group biuser; 
commit;
