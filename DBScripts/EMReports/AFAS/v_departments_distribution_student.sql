DROP VIEW IF EXISTS edw.v_departments_distribution_student
;
commit
;
CREATE OR REPLACE VIEW edw.v_departments_distribution_student
(
  institution_key,
  client,
  submitted_date,
  sub_population,
  student_status,
  student_id,
  last_name,
  first_name,
  szip_code,
  gender,
  counselor,
  isource_cd,
  major1_admit,
  admit_cd_desc,
  admit_type_desc,
  app_type_desc,
  counselor_desc,
  country_cd_desc,
  denom_cd_desc,
  dept1_admit_desc,
  ethnic_cd_desc,
  hscode_desc,
  isource_cd_desc,
  major1_admit_desc,
  market_seg_desc,
  subinst_cd_desc,
  sport1_desc,
  housing_admissions_desc,
  model_score_bin_desc,
  state,
  i__g,
  in_g,
  ip_g,
  im_g,
  hsgpa,
  academictiersequence,
  financialtiersequence,
  fafsa_status
)
AS 
 SELECT fsd.institution_key, i.institution_name AS client, d.transmit_date AS submitted_date, p.population_name AS sub_population, fsd.student_status, sd.studentid AS student_id, sd.last_name, sd.first_name, sd.szip_code, sd.gender, sd.counselor,
  sd.isource_cd, 
  sd.major1_admit,
  nvl(case sd.admit_cd_desc when ' ' then sd.admit_cd 
  else sd.admit_cd_desc end,sd.admit_cd) as admit_cd_desc,
	nvl(case sd.admit_type_desc when ' ' then sd.admit_type 
  else sd.admit_type_desc end,sd.admit_type) as admit_type_desc,
  nvl(case sd.app_type_desc when ' ' then sd.app_type 
  else sd.app_type_desc end,sd.app_type) as app_type_desc,
nvl(case sd.counselor_desc when ' ' then sd.counselor 
  else sd.counselor_desc end,sd.counselor) as counselor_desc,
  
  sd.country, 
  
  nvl(case sd.denom_cd_desc when ' ' then sd.denom_cd 
  else sd.denom_cd_desc end,sd.denom_cd) as denom_cd_desc,
  nvl(case sd.dept1_admit_desc when ' ' then sd.dept1_admit 
  else sd.dept1_admit_desc end,sd.dept1_admit) as dept1_admit_desc,
  nvl(case sd.ethnic_cd_desc when ' ' then sd.ethnic_cd 
  else sd.ethnic_cd_desc end,sd.ethnic_cd) as ethnic_cd_desc,
 nvl(case sd.hscode_desc when ' ' then sd.hscode 
  else sd.hscode_desc end,sd.hscode) as hscode_desc,
  nvl(case sd.isource_cd_desc when ' ' then sd.isource_cd 
  else sd.isource_cd_desc end,sd.isource_cd) as isource_cd_desc,
nvl(case sd.major1_admit_desc when ' ' then sd.major1_admit 
  else sd.major1_admit_desc end,sd.major1_admit) as major1_admit_desc,
  nvl(case sd.market_seg_desc when ' ' then sd.market_segment 
  else sd.market_seg_desc end,sd.market_segment) as market_seg_desc,
 nvl(case sd.subinst_cd_desc when ' ' then sd.subinst_cd 
  else sd.subinst_cd_desc end,sd.subinst_cd) as subinst_cd_desc,
nvl(case sd.sport1_desc when ' ' then sd.sport1 
  else sd.sport1_desc end,sd.sport1) as sport1_desc,
  nvl(case sd.housing_admissions_desc when ' ' then sd.housing 
  else sd.housing_admissions_desc end,sd.housing) as housing_admissions_desc,
  nvl(case sd.model_score_bin_desc when ' ' then sd.model_score_bin 
  else sd.model_score_bin_desc end,sd.model_score_bin) as model_score_bin_desc,
 sd.state, fsd.i__g, fsd.in_g, fsd.ip_g, fsd.im_g, fsd.hsgpa, "at".display_sequence AS academictiersequence, ft.display_sequence AS financialtiersequence, fsd.fafsa_status
   FROM ( SELECT fsd.student_detail_key, fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.academic_year_key, 'Admitted'::character varying AS student_status, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.in_g, 0) AS in_g, COALESCE(fsd.ip_g, 0) AS ip_g, COALESCE(fsd.im_g, 0) AS im_g, 1 AS student_count, COALESCE(fsd.tuition, 0) AS tuition, COALESCE(fsd.fees, 0) AS fees, COALESCE(fsd.room, 0) AS room, COALESCE(fsd.board, 0) AS board, COALESCE(fsd.empb, 0) AS empb, COALESCE(fsd.hsgpa, 0::double precision) AS hsgpa, COALESCE(fsd.c_gpa, 0::double precision) AS c_gpa, COALESCE(fsd.p_gpa, 0::double precision) AS p_gpa, COALESCE(fsd.c_gpa_recalc, 0::double precision) AS c_gpa_recalc, COALESCE(fsd.sat_comp, 0) AS sat_comp, COALESCE(fsd.act_comp, 0) AS act_comp, COALESCE(fsd.calc_indx, 0::double precision) AS calc_indx, COALESCE(fsd.standing, 0) AS standing, COALESCE(fsd.inst_rat, 0::double precision) AS inst_rat, COALESCE(fsd.hsgpa_recalc, 0::double precision) AS hsgpa_recalc, 
                CASE
                    WHEN fsd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, COALESCE(fsd.need, 0) AS need, 0 AS net_confirmed_count, 0 AS admitted_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_admitted = 1
UNION ALL 
         SELECT fsd.student_detail_key, fsd.student_key, fsd.institution_key, fsd.dataset_key, fsd.academic_year_key, 
                CASE
                    WHEN fsd.flag_net_confirmed = 1 THEN 'Net Confirmed'::character varying
                    WHEN fsd.flag_confirmed_cancelled = 1 THEN 'Confirmed Cancelled'::character varying
                    WHEN fsd.flag_canc = 1 AND fsd.flag_con = 0 THEN 'Cancelled'::character varying
                    ELSE 'Pending'::character varying
                END AS student_status, COALESCE(fsd.i__g, 0) AS i__g, COALESCE(fsd.in_g, 0) AS in_g, COALESCE(fsd.ip_g, 0) AS ip_g, COALESCE(fsd.im_g, 0) AS im_g, 1 AS student_count, COALESCE(fsd.tuition, 0) AS tuition, COALESCE(fsd.fees, 0) AS fees, COALESCE(fsd.room, 0) AS room, COALESCE(fsd.board, 0) AS board, COALESCE(fsd.empb, 0) AS empb, COALESCE(fsd.hsgpa, 0::double precision) AS hsgpa, COALESCE(fsd.c_gpa, 0::double precision) AS c_gpa, COALESCE(fsd.p_gpa, 0::double precision) AS p_gpa, COALESCE(fsd.c_gpa_recalc, 0::double precision) AS c_gpa_recalc, COALESCE(fsd.sat_comp, 0) AS sat_comp, COALESCE(fsd.act_comp, 0) AS act_comp, COALESCE(fsd.calc_indx, 0::double precision) AS calc_indx, COALESCE(fsd.standing, 0) AS standing, COALESCE(fsd.inst_rat, 0::double precision) AS inst_rat, COALESCE(fsd.hsgpa_recalc, 0::double precision) AS hsgpa_recalc, 
                CASE
                    WHEN fsd.flag_isir_valid = 1 THEN 'FAFSA Filer'::character varying
                    ELSE 'Non-FAFSA Filer'::character varying
                END AS fafsa_status, COALESCE(fsd.need, 0) AS need, 0 AS net_confirmed_count, 0 AS admitted_count
           FROM edw.fact_osds_student_detail fsd
          WHERE fsd.flag_net_confirmed = 1 OR fsd.flag_confirmed_cancelled = 1 OR fsd.flag_pending = 1 OR fsd.flag_canc = 1) fsd
   JOIN edw.dim_osds_institution i ON fsd.institution_key = i.institution_key
   JOIN edw.dim_osds_dataset d ON fsd.dataset_key = d.dataset_key AND fsd.institution_key = d.institution_key
   JOIN edw.dim_osds_student sd ON fsd.student_key = sd.student_key
   JOIN edw.dim_osds_student_population sp ON fsd.student_detail_key = sp.student_detail_key
   JOIN edw.dim_osds_population p ON sp.population_key = p.population_key AND fsd.institution_key = p.institution_key
   JOIN edw.dim_osds_academic_year a ON a.academic_year_key = fsd.academic_year_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = sp.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = sp.academic_tier_key
  WHERE d.flag_active = '1'::bpchar AND p.flag_active::character varying::text = 1::character varying::text 
  AND d.process_type_id = 2 AND d.dataset_status_id = 4 
     ;
commit
;
GRANT SELECT ON edw.v_departments_distribution TO reportinguser;
GRANT SELECT ON edw.v_departments_distribution TO group qauser; 
GRANT SELECT ON edw.v_departments_distribution TO group biuser; 
commit;
