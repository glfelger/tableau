DROP VIEW edw.agg_afas_rev_analysis
;
commit
;
DROP VIEW edw.agg_afas_rev_analysis_goals1;
commit;
CREATE OR REPLACE VIEW edw.agg_afas_rev_analysis_goals1
(
  client,
  year,
  fiscal_year,
  sub_population,
  fin_tier_name,
  fin_tier_display_sequence,
  a_tier_display_sequence,
  student_status,
  student_count,
  sum_tuitionrevenue,
  sum_requiredfeerevenue,
  sum_roomboardrevenue,
  sum_totalrevenue,
  sum_unfundedinstitutionalgiftaid,
  sum_fundedinstitutionalgiftaid,
  sum_tuitionexchangewaivers,
  sum_premieracademic,
  sum_roomboard,
  sum_campusbasedwork,
  sum_nettuitionfeerevenue,
  sum_overallincludingfees,
  sum_insneed,
  sum_insmerit,
  sum_talentgift,
  sum_otheraid,
  sum_totalaid,
  sum_empb,
  tuitionrevenue,
  requiredfeerevenue,
  roomboardrevenue,
  totalrevenue,
  nettuitionfeerevenue,
  netrevenue,
  overallincludingfees,
  unfundedinstitutionalgiftaid,
  roomboard,
  fundedinstitutionalgiftaid,
  institutionalgiftaid
)
AS 
SELECT derived_table1.client,
       derived_table1."year",
       derived_table1.fiscal_year,
       derived_table1.sub_population,
       derived_table1.fin_tier_name,
       derived_table1.fin_tier_display_sequence,
       derived_table1.a_tier_display_sequence,
       derived_table1.student_status,
       derived_table1.student_count,
       derived_table1.sum_tuitionrevenue,
       derived_table1.sum_requiredfeerevenue,
       derived_table1.sum_roomboardrevenue,
       derived_table1.sum_totalrevenue,
       derived_table1.sum_unfundedinstitutionalgiftaid,
       derived_table1.sum_fundedinstitutionalgiftaid,
       derived_table1.sum_tuitionexchangewaivers,
       derived_table1.sum_premieracademic,
       derived_table1.sum_roomboard,
       derived_table1.sum_campusbasedwork,
       derived_table1.sum_nettuitionfeerevenue,
       derived_table1.sum_overallincludingfees,
       derived_table1.sum_insneed,
       derived_table1.sum_insmerit,
       derived_table1.sum_talentgift,
       derived_table1.sum_otheraid,
       derived_table1.sum_totalaid,
       derived_table1.sum_empb,
       derived_table1.tuitionrevenue,
       derived_table1.requiredfeerevenue,
       derived_table1.roomboardrevenue,
       derived_table1.totalrevenue,
       derived_table1.nettuitionfeerevenue,
       derived_table1.netrevenue,
       derived_table1.overallincludingfees,
       derived_table1.unfundedinstitutionalgiftaid,
       derived_table1.roomboard,
       derived_table1.fundedinstitutionalgiftaid,
       derived_table1.institutionalgiftaid
FROM (SELECT i.institution_name AS client,
             a.academic_year_name AS "year",
             a.academic_start_year AS fiscal_year,
             p.population_name AS sub_population,
             ft.name AS fin_tier_name,
             CASE
               WHEN fgd.cellnumber >= 2001 AND fgd.cellnumber <= 2015 THEN fgd.cellnumber
               ELSE 99
             END AS fin_tier_display_sequence,
             CASE
               WHEN fgd.cellnumber >= 1001 AND fgd.cellnumber <= 1007 THEN fgd.cellnumber
               ELSE 99
             END AS a_tier_display_sequence,
             'Net Confirmed'::CHARACTER VARYING AS student_status,
             fgd.enrolledcount AS student_count,
             0 AS sum_tuitionrevenue,
             0 AS sum_requiredfeerevenue,
             0 AS sum_roomboardrevenue,
             0 AS sum_totalrevenue,
             0 AS sum_unfundedinstitutionalgiftaid,
             0 AS sum_fundedinstitutionalgiftaid,
             0 AS sum_tuitionexchangewaivers,
             0 AS sum_premieracademic,
             0 AS sum_roomboard,
             0 AS sum_campusbasedwork,
             0 AS sum_nettuitionfeerevenue,
             0 AS sum_overallincludingfees,
             0 AS sum_insneed,
             0 AS sum_insmerit,
             0 AS sum_talentgift,
             0 AS sum_otheraid,
             0 AS sum_totalaid,
             0 AS sum_empb,
             fgd.tuitionrevenue,
             fgd.feerevenue AS requiredfeerevenue,
             fgd.roomboardrevenue,
             fgd.totalrevenue,
             fgd.netrevenue AS nettuitionfeerevenue,
             fgd.nettuitionfeerevenue AS netrevenue,
             fgd.overallincludingfees,
             fgd.unfundedinstitutionalgiftaid,
             fgd.roomboard,
             fgd.fundedinstitutionalgiftaid,
             fgd.institutionalgiftaid
      FROM edw.fact_osds_goal_detail_revenue fgd
        JOIN edw.dim_osds_institution i ON fgd.institution_key = i.institution_key
        LEFT JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
        JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
        LEFT JOIN edw.dim_osds_financial_tier ft ON fgd.financial_tier_key = ft.financial_tier_key
      WHERE p.flag_active::CHARACTER VARYING::TEXT = 1::CHARACTER VARYING::TEXT
      AND   fgd.source_type::TEXT = 'GoalDetail'::CHARACTER VARYING::TEXT
      AND   fgd.cellnumber >= 1000
      AND   fgd.cellnumber <= 2050
      UNION ALL
      SELECT DISTINCT i.institution_name AS client,
             a.academic_year_name AS "year",
             a.academic_start_year AS fiscal_year,
             p.population_name AS sub_population,
             ''::CHARACTER VARYING AS fin_tier_name,
             99 AS fin_tier_display_sequence,
             99 AS a_tier_display_sequence,
             'Net Confirmed'::CHARACTER VARYING AS student_status,
             0 AS student_count,
             fgd.tuitionrevenue AS sum_tuitionrevenue,
             fgd.requiredfeerevenue AS sum_requiredfeerevenue,
             fgd.roomboardrevenue AS sum_roomboardrevenue,
             fgd.totalrevenue AS sum_totalrevenue,
             fgd.unfundedinstitutionalgiftaid AS sum_unfundedinstitutionalgiftaid,
             fgd.fundedinstitutionalgiftaid AS sum_fundedinstitutionalgiftaid,
             fgd.tuitionexchangewaivers AS sum_tuitionexchangewaivers,
             fgd.premieracademic AS sum_premieracademic,
             fgd.roomboard AS sum_roomboard,
             fgd.campusbasedwork AS sum_campusbasedwork,
             fgd.nettuitionfeerevenue AS sum_nettuitionfeerevenue,
             fgd.overallincludingfees AS sum_overallincludingfees,
             fgd.ins_need AS sum_insneed,
             fgd.ins_merit AS sum_insmerit,
             fgd.talent_gift AS sum_talentgift,
             fgd.other_aid AS sum_otheraid,
             fgd.total_aid AS sum_totalaid,
             fgd.employeedependentwaivers AS sum_empb,
             0 AS tuitionrevenue,
             0 AS requiredfeerevenue,
             0 AS roomboardrevenue,
             0 AS totalrevenue,
             0 AS nettuitionfeerevenue,
             0 AS netrevenue,
             0 AS overallincludingfees,
             0 AS unfundedinstitutionalgiftaid,
             0 AS roomboard,
             0 AS fundedinstitutionalgiftaid,
             0 AS institutionalgiftaid
      FROM edw.fact_osds_goal_detail_revenue fgd
        JOIN edw.dim_osds_institution i ON fgd.institution_key = i.institution_key
        LEFT JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
        JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
      WHERE p.flag_active::CHARACTER VARYING::TEXT = 1::CHARACTER VARYING::TEXT
      AND   fgd.source_type::TEXT = 'GoalSummary'::CHARACTER VARYING::TEXT) derived_table1;

commit;

GRANT SELECT ON edw.agg_afas_rev_analysis_goals1 to reportinguser;
GRANT SELECT ON edw.agg_afas_rev_analysis_goals1 TO group qauser; 
GRANT SELECT ON edw.agg_afas_rev_analysis_goals1 TO group biuser;

