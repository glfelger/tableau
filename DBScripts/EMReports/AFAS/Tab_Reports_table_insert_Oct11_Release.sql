insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Department Distribution', 'https://reporting.ruffalonl.com/#/views/DepartmentsDistribution/DepartmentsDistribution', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;
insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Fund Summary', 'https://reporting.ruffalonl.com/#/views/FundSummary/FundSummary', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;
insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Melt Summary', 'https://reporting.ruffalonl.com/#/views/MeltSummary/MeltSummary', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;
insert into edw.tableau_reports (module, reportname, generic_url, is_active, report_type, vision_project_type, report_group)
values ('Enrollment Management', 'Gender and Residency', 'https://reporting.ruffalonl.com/#/views/GenderandResidenceDistribution/GenderResidencyHall', 'Y', 'Dashboard', 'AFAS Reporting', 'Class Optimizer')
;
commit
;
