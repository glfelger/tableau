update edw.tableau_reports set vision_project_type = 'AFAS Reporting'
where id in (31,34)
;
commit
;
update edw.tableau_reports 
set generic_url = 'https://reporting.ruffalonl.com/#/views/AcademicCredentialsbyAI_CS/AcademicClassStanding' 
where  id = 31
;
commit
;
update edw.tableau_reports 
set generic_url = 'https://reporting.ruffalonl.com/#/views/AcademicDistributionbyAI_CS/AcademicClassStanding_1' 
where  id = 34
;
commit
;