DROP VIEW IF EXISTS edw.v_agg_population_goal
;
commit
;
CREATE OR REPLACE VIEW edw.v_agg_population_goal
(
  client,
  year,
  sub_population,
  goal,
  ngoal,
  cellnumber,
  student_status
)
AS 
 SELECT i.institution_name AS client, a.academic_year_name AS "year", p.population_name AS sub_population, fgd.admittedcount AS goal, fgd.enrolledcount AS ngoal, fgd.cellnumber, fgd.student_status
   FROM ( SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, 'ADMITTED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail
UNION ALL 
         SELECT fact_osds_goal_detail.admittedcount, fact_osds_goal_detail.enrolledcount, fact_osds_goal_detail.institution_key, fact_osds_goal_detail.academic_year_key, fact_osds_goal_detail.financial_tier_key, fact_osds_goal_detail.population_key, fact_osds_goal_detail.academic_tier_key, fact_osds_goal_detail.cellnumber, 'NET CONFIRMED'::character varying AS student_status
           FROM edw.fact_osds_goal_detail) fgd
   JOIN edw.dim_osds_institution i ON fgd.institution_key = i.institution_key
   JOIN edw.dim_osds_academic_year a ON fgd.academic_year_key = a.academic_year_key
   JOIN edw.dim_osds_population p ON fgd.population_key = p.population_key
   JOIN edw.dim_osds_financial_tier ft ON ft.financial_tier_key = fgd.financial_tier_key
   JOIN edw.dim_osds_academic_tier "at" ON "at".academic_tier_key = fgd.academic_tier_key
  WHERE fgd.cellnumber >= 0 AND fgd.cellnumber < 56 AND p.flag_active = 1;
commit
;

GRANT SELECT ON edw.v_agg_population_goal TO reportinguser
;
commit;